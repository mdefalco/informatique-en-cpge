# Cours d'informatique en CPGE

Ce repo contient les sources ainsi que les outils permettant de générer les
ressources visibles sur https://marcdefalco.github.io/

## Dépendances

* LuaLaTeX
* Pandoc
* Python 3.*
* make

## Compilation

* Taper make
* Tous les documents produits se trouvent dans le répertoire html, les pdf sont
  dans html/pdf
