import os
import subprocess



LANG_CAP = {
        'python' : 'Python',
        'c' : 'C',
        'ocaml' : 'OCaml'
        }

class PreprocessSyntaxError(Exception):
    pass

class Environment:
    def __init__(self):
        self.vals = {}
        self.html = False
        self.lang = "ocaml"
        self.slides = False

    def isBound(self, v):
        return v in self.vals and self.vals[v] is not []

    def bind(self,v,x):
        if v not in self.vals:
            self.vals[v] = []
        self.vals[v].append(x)

    def unbind(self,v):
        self.vals[v].pop()

    def set(self,v,x):
        if v not in self.vals:
            self.vals[v] = [x]
        self.vals[v][-1] = x

    def get(self,v,default=None):
        if v not in self.vals or self.vals[v] == []:
            return default
        return self.vals[v][-1]

    def __repr__(self):
        return repr(self.vals)

class Macro:
    '''Macro can have nth arguments, pth optional arguments and a last
    mandatory argument'''
    def __init__(self,nArgs=1,nOpts=0,func=None,text=None,protected=[]):
        self.nArgs = nArgs
        self.nOpts = nOpts
        self.text = text
        self.func = func
        self.protected = protected

    def apply(self, env, args, opts):
        env.set('callid', env.get('callid', default=0)+1)
        if self.func is not None:
            try:
                return self.func(env, args, opts=opts)
            except Exception as e:
                print('Syntax Error applying macro', self.func.__name__, 
                        'args : ', repr(args))
                print(e)
                raise PreprocessSyntaxError
        for i, arg in enumerate(args):
            env.bind(str(i+1), arg)
        for i in range(self.nOpts):
            arg = opts[i] if i < len(opts) else ''
            env.bind('O'+str(i+1), arg)
        out = process(self.text,env)
        for i, arg in enumerate(args):
            env.unbind(str(i+1))
        for i in range(self.nOpts):
            env.unbind('O'+str(i+1))
        return out

def macro_undefine(env, args, opts=[]):
    name = args[0]
    env.unbind(name)
    return ''

def macro_define(env, args, opts=[]):
    name = args[0]
    nArgs = int(opts[0]) if len(opts) > 0 and opts[0] is not None else 0
    nOpts = int(opts[1]) if len(opts) > 1 and opts[1] is not None else 0
    text = args[1]
    if nArgs == 0:
        env.bind(name, text)
    else:
        env.bind(name, Macro(nArgs, nOpts, text=text))
    return ''

def macro_ref(env, args, opts=[]):
    ref = args[0]
    desc = ref
    if len(opts) > 0:
        desc = opts[0]
    if env.html:
        for fn in os.listdir('html'):
            if fn[-4:] == 'html' and '"#'+ref+'"' in open('html/'+fn).read():
                return f'[{desc}]({fn}#{ref})'
        return 'REFNOTFOUND'
    else:
        return f'[{desc}](#{ref})'

def macro_ifndef(env, args, opts=[]):
    if not env.isBound(args[0]):
        return process(args[1], env)
    return ''

def macro_ifdef(env, args, opts=[]):
    if env.isBound(args[0]):
        return process(args[1], env)
    return ''

def macro_lang(env, args, opts=[]):
    return LANG_CAP[env.lang]

def macro_langcap(env, args, opts=[]):
    lang = args[0].split()[0]
    if lang in LANG_CAP:
        return LANG_CAP[lang]
    return lang

def macro_iflangeq(env, args, opts=[]):
    if env.lang == args[0]:
        return process(args[1], env)
    return ''

def macro_iflangneq(env, args, opts=[]):
    if env.lang != args[0]:
        return process(args[1], env)
    return ''

def macro_debug(env, args, opts=[]):
    print(process(args[0], env))
    return ''

def macro_iftex(env, args, opts=[]):
    if not env.html:
        return process(args[0], env)
    return ''

def macro_ifhtml(env, args, opts=[]):
    if env.html:
        return process(args[0], env)
    return ''

def macro_rawinclude(env, args, opts=[]):
    fn = args[0]
    d = env.get('current_working_directory')
    tag = None
    if len(opts) == 1 and opts[0] != '':
        tag = opts[0]
    data = []
    if tag:
        found = False
        if fn[0] == '/':
            d = os.path.dirname(__file__)
            fn = fn[1:]
        fn_complet = os.path.join(d, fn)

        if not os.path.exists(fn_complet):
            return f'ERROR: {fn_complet} does not exist'

        for l in open(fn_complet):
            if 'BEGIN '+tag in l:
                found = True
                continue
            if 'END '+tag in l:
                break
            if found:
                data.append(l)
    else:
        fn_complet = os.path.join(d, fn)
        if not os.path.exists(fn_complet):
            return f'ERROR: {fn_complet} does not exist'

        for l in open(fn_complet):
            if 'IGNORE BELOW' in l: break
            if 'IGNORE ABOVE' in l:
                data = []
            else:
                data.append(l)
    return ''.join(data)

def macro_include(env, args, opts=[]):
    fn = args[0]
    d = env.get('current_working_directory')
    return process_file(os.path.join(d, fn), env)

def macro_inlinetikz(env, args, opts=[]):
    picture_count = 1 + env.get('picture_count', default=0)
    env.set('picture_count', picture_count)
    return macro_tikz(env,[ 'inline_%03d' % picture_count ] +
            args,opts=[ 'anchor=base', 'inline' ]+opts)

def macro_retikz(env, args, opts=[]):
    picture_name = args[0].split(' ')[0]
    attrs = ' '.join(args[0].split(' ')[1:])
    if env.html:
        s = '![](assets/pics/' + picture_name + '.png)' + attrs
    else:
        s = '![](../pics/' + picture_name + '.pdf)' + attrs
    return s

def macro_tikz(env, args, opts=[]):
    extra = ''
    if len(opts) > 0:
        extra = process(opts[0],env)
    else:
        extra = 'anchor=base'
    te = env.get('tikzextra', None)
    if te is not None:
        extra += te + ',' + extra

    args[1] = '\\begin{tikzpicture}[%s]' % extra + process(args[1], env)
    args[1] += '\\end{tikzpicture}'
    return macro_latex(env, args, opts=opts)


def macro_latex(env, args, opts=[]):
    tikz_template = open('tikz.template','r').read()
    picture_name = args[0].split(' ')[0]
    attrs = ' '.join(args[0].split(' ')[1:])
    picture_code = args[1]
    latex_code = tikz_template % picture_code
    cached = False
    try:
        saved_code = open('pics/' + picture_name + '.tex', 'r').read()
        cached = latex_code == saved_code \
            and os.path.exists('pics/' + picture_name + '.pdf')
    except IOError:
        pass

    if not cached:
        open('pics/' + picture_name + '.tex', 'w').write(latex_code)

        args = [ 'lualatex', '-interaction=nonstopmode', picture_name ]
        compile_process = subprocess.run(args, cwd='pics', capture_output=True)

        print(compile_process.stdout.decode('utf-8'))
        if compile_process.returncode != 0:
            print(compile_process.stdout.decode('utf-8'))
            import sys
            sys.exit(1)

        subprocess.run(['convert','-density','1200',picture_name + '.pdf',
            '-resize', '10%', '-alpha','on','../html/assets/pics/'+picture_name + '.png'], cwd='pics')

    if env.html:
        s = '![](assets/pics/' + picture_name + '.png)' + attrs
    else:
        s = '![](../pics/' + picture_name + '.pdf)' + attrs

    if 'inline' not in opts:
        s = '!center\n'+'`'*42+'\n'+s+'\n'+'`'*42+'\n'
        return process(s, env)

    return s

def macro_set_counter(env, args, opts=[]):
    counter_name = args[0]
    val = args[1]
    env.bind(counter_name, val)
    return ''
    
def macro_counter(env, args, opts=[]):
    counter_name = args[0]

    if env.isBound(counter_name):
        val = str(int(env.get(counter_name)) + 1)
    else:
        val = '1'
        if len(opts) > 0:
            val = opts[0]
    env.bind(counter_name, val)

    return val

def macro_new_counter(env, args, opts=[]):
    counter_name = args[0]
    val = 1
    if len(opts) > 0:
        val = int(opts[0])
    env.bind(counter_name, str(val))
    return ''

def macro_incr_counter(env, args, opts=[]):
    counter_name = args[0]
    counter = env.get(counter_name)
    increment = 1
    if len(opts) > 0:
        increment = int(opts[0])
    counter = str(int(counter) + increment)
    env.bind(counter_name, counter)
    return ''

def macro_save(env, args, opts=[]):
    reg_name = '[reg]:'+args[0]
    val = args[1]
    if env.isBound(reg_name):
        val = env.get(reg_name) + val
    env.bind(reg_name, val)
    return ''

def macro_restore(env, args, opts=[]):
    reg_name = '[reg]:'+args[0]
    val = env.get(reg_name)
    env.bind(reg_name, '')
    return process(val, env)

def macro_eval(env, args, opts=[]):
    e = args[0]
    return str(eval(e))

def macro_numeral(env, args, opts=[]):
    n = int(args[0])
    numerals = [ 'zero', 'one', 'two', 'three', 'four', 'five',
            'six', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve' ]
    return numerals[n]

def macro_if(env, args, opts=[]):
    if args[0] != '':
        return process(args[1],env)
    else:
        return process(args[2],env)

def macro_comment(env, args, opts=[]):
    return ''

def macro_strreplace(env, args, opts=[]):
    return process(args[2],env).replace(args[0], args[1])

def loadStandardLibrary(env):
    env.bind('include', Macro(1, 0, func=macro_include))
    env.bind('rawinclude', Macro(1, 1, func=macro_rawinclude))
    env.bind('define', Macro(2, 2, func=macro_define, protected=['!1','!2','!O1','!O2']))
    env.bind('undefine', Macro(1, 0, func=macro_undefine))
    env.bind('tikz', Macro(2, 1, func=macro_tikz, protected=['!2']))
    env.bind('latex', Macro(2, 0, func=macro_latex, protected=['!2']))
    env.bind('retikz', Macro(1, 0, func=macro_retikz))
    env.bind('strreplace', Macro(3, 0, func=macro_strreplace))
    env.bind('inlinetikz', Macro(1, 1, func=macro_inlinetikz, protected=['!1']))
    env.bind('language', Macro(0, 0, func=macro_lang))
    env.bind('capitalize', Macro(1, 0, func=macro_langcap))
    env.bind('iflangeq', Macro(2, 0, func=macro_iflangeq))
    env.bind('iflangneq', Macro(2, 0, func=macro_iflangneq))
    env.bind('debug', Macro(1, 0, func=macro_debug,protected=['!1']))
    env.bind('iftex', Macro(1, 0, func=macro_iftex,protected=['!1']))
    env.bind('ifhtml', Macro(1, 0, func=macro_ifhtml,protected=['!1']))
    env.bind('ifdef', Macro(2, 0, func=macro_ifdef))
    env.bind('ifndef', Macro(2, 0, func=macro_ifndef))
    env.bind('if', Macro(3, 0, func=macro_if,protected=['!2','!3']))
    env.bind('ref', Macro(1, 1, func=macro_ref))
    env.bind('newcounter', Macro(1, 1, func=macro_new_counter))
    env.bind('incrcounter', Macro(1, 1, func=macro_incr_counter))
    env.bind('setcounter', Macro(2, 0, func=macro_set_counter))
    env.bind('save', Macro(2, 0, func=macro_save))
    env.bind('restore', Macro(1, 0, func=macro_restore))
    env.bind('counter', Macro(1, 1, func=macro_counter))
    env.bind('numeral', Macro(1, 0, func=macro_numeral))
    env.bind('comment', Macro(1, 0, func=macro_comment, protected=['!1']))

class Flow:
    def __init__(self, value):
        self.value = value
        self.pos = 0
        self.length = len(self.value)

    def peek(self,n=1):
        return self.value[self.pos:self.pos+n]

    def unget(self,n):
        self.pos -= n

    def get(self,n):
        s = self.value[self.pos:self.pos+n]
        self.pos += n
        return s

    def get_val(self):
        val = ''
        while not self.finished():
            c = self.peek()
            if c.isalnum():
                self.get(1)
                val += c
            else:
                break
        return val

    def get_delim(self,vin,vout):
        val = ''
        c = self.peek()
        if c != vin:
            return None
        self.get(1)
        level = 0
        while not self.finished():
            c = self.get(1)
            if c == vout:
                if level == 0:
                    break
                else:
                    level -= 1
            elif c == vin:
                level += 1
            val += c
        return val

    def get_opt(self):
        return self.get_delim('[',']')

    def get_arg(self):
        return self.get_delim('(',')')

    def get_block(self):
        leadin = self.get_line()
        val = []
        while not self.finished():
            l = self.get_line()
            if l == leadin:
                self.unget(1)
                if self.peek(1) != '\n':
                    self.get(1)
                break
            val.append(l)
        return '\n'.join(val)

    def get_line(self):
        val = ''
        while not self.finished():
            c = self.get(1)
            if c == '\n':
                break
            val += c
        return val

    def peek_line(self):
        i = self.pos
        while i < len(self.length) and self.vals[i] != '\n':
            i += 1
        return self.vals[self.pos:i]

    def get_arg_or_block(self):
        c = self.peek()
        if c == '\n':
            self.get(1)
            return self.get_block()
        else:
            return self.get_arg()

    def finished(self):
        return self.pos >= self.length

    def __repr__(self):
        if self.finished():
            return 'finished'
        return repr(self.value[self.pos-10:self.pos]) \
                + '[' + repr(self.value[self.pos]) + ']' \
                + repr(self.value[self.pos+1:self.pos+10])

def process_file(fn, env):
    fin = open(fn, "r")
    dirname = os.path.dirname(fn)
    env.bind('current_working_directory', dirname)
    env.bind('currentfile', fn)
    print(fn)
    s = process(fin.read(),env)
    env.unbind('currentfile')
    env.unbind('current_working_directory')
    return s

def process(s,env,source=None):
    out = ''

    s = Flow(s)
    while not s.finished():
        c = s.get(1)
        if c == '!': # possibly a macro
            if s.peek(1) == '!':
                s.get(1)
                out += c
            elif s.peek(1) == '[':
                out += c
            else:
                val = s.get_val()
                if val == '':
                    out += c
                elif not env.isBound(val):
                    out += '!' + val
                else:
                    elem = env.get(val)
                    if isinstance(elem,Macro):
                        opts, args = [], []
                        while True:
                            opt = s.get_opt()
                            if opt is None:
                                break
                            opts.append(opt)
                        for i in range(elem.nArgs):
                            args.append(s.get_arg_or_block())

                        pargs = []
                        for i, arg in enumerate(args):
                            if '!'+str(i+1) not in elem.protected:
                                try:
                                    pargs.append(process(arg,env))
                                except Exception as e:
                                    print('SyntaxError processing argument to',
                                            val, arg)
                                    raise PreprocessSyntaxError
                            else:
                                pargs.append(arg)

                        popts = []
                        for i, opt in enumerate(opts):
                            if '!O'+str(i+1) not in elem.protected:
                                popts.append(process(opt,env))
                            else:
                                popts.append(opt)
                        d = process(elem.apply(env,pargs,popts), env)
                        out += d
                        #if args != [] and '\n' in args[-1]:
                        #    out += '\n'

                    elif elem is not None:
                        out += process(str(elem), env)
                        # eat next character if space
                        if s.peek(1) == ' ':
                            s.get(1)
        else:
            out += c

    return out


if __name__ == '__main__':
    import sys
    import argparse

    parser = argparse.ArgumentParser(description='Preprocess markdown files with macros.')
    parser.add_argument('input', type=str, help='Input file name')
    parser.add_argument('output', type=str, help='Output file name')
    parser.add_argument('--macros', type=str, help='macros file to load',
            default='')
    parser.add_argument('--lang', type=str, help='Langage by default',
            default='ocaml')
    parser.add_argument('--format', type=str, help='Target output format (tex/html)', default='html')
    parser.add_argument('--slides', help='Slides output', action="store_true")
    parser.add_argument('--exportref', help='Export references', action="store_true")

    args = parser.parse_args()

    env = Environment()
    loadStandardLibrary(env)


    env.html = args.format == 'html'
    env.lang = args.lang
    env.slides = args.slides

    env.bind('site', 'https://marcdefalco.github.io/')

    try:
        if args.macros:
            process_file(args.macros, env)
        val = process_file(args.input, env)

        if args.exportref:
            val = '' # TODO

        open(args.output, 'w').write(val)
    except PreprocessSyntaxError:
        print('ERROR  preprocessing aborted')
        import sys
        sys.exit(1)
