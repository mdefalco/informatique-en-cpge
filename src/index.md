---
title: Informatique - Introduction
subtitle: MPI
author: Marc de Falco
---

!iftex
~~~
!include(macros.md)
~~~

!ifhtml
~~~
!include(macros_html.md)
~~~

!include(logo.md)

!include(introduction.md)

### Plan du cours

* [Introduction à l'informatique](chap_introduction_introduction_informatique.html) <div class="ui small button">[PDF](chap_introduction_introduction_informatique.pdf)</div> 
  **À reprendre**
* Programmation
    * [Introduction à la programmation (impérative)](chap_programmation_imperatif.html) <div class="ui small button">[PDF](chap_programmation_imperatif.pdf)</div> **En cours d'écriture**
    * [Récursivité](chap_programmation_recursivite.html) <div class="ui small button">[PDF](chap_programmation_recursivite.pdf)</div> **En cours d'écriture**
    * [Options en OCaml](chap_programmation_options_ocaml.html) <div class="ui small button">[PDF](chap_programmation_options_ocaml.pdf)</div>
    * [Exceptions en OCaml](chap_programmation_exceptions_ocaml.html) <div class="ui small button">[PDF](chap_programmation_exceptions_ocaml.pdf)</div>
* Structures de données
    * [Structures de données abstraites et implémentations](chap_structuresdonnees_structurededonnees.html) <div class="ui small button">[PDF](chap_structuresdonnees_structurededonnees.pdf)</div>
    * [Séquences : structure abstraite et implémentations (tableaux, listes chaînées)](chap_structuresdonnees_sequences.html) <div class="ui small button">[PDF](chap_structuresdonnees_sequences.pdf)</div>
    * [Piles et files : structures et implémentations](chap_structuresdonnees_pilesfiles.html) <div class="ui small button">[PDF](chap_structuresdonnees_pilesfiles.pdf)</div>
    * [Arbres et structures hiérarchiques](chap_structuresdonnees_arbres.html) <div class="ui small button">[PDF](chap_structuresdonnees_arbres.pdf)</div>
    * [Dictionnaire : ABR, Tables de hachage](chap_structuresdonnees_dictionnaire.html) <div class="ui small button">[PDF](chap_structuresdonnees_dictionnaire.pdf)</div>
    * [Graphes et structures relationnelles](chap_structuresdonnees_graphes.html) <div class="ui small button">[PDF](chap_structuresdonnees_graphes.pdf)</div>
        * [TP Graphes](chap_tps_graphes.html) <div class="ui small button">[PDF](chap_tps_graphes.pdf)</div>
    * [Union find](chap_structuresdonnees_unionfind.html) <div class="ui small button">[PDF](chap_structuresdonnees_unionfind.pdf)</div>
* Algorithmique
    * [Introduction à l'analyse des algorithmes](chap_algorithmique_analyse_algorithme.html) <div class="ui small button">[PDF](chap_algorithmique_analyse_algorithme.pdf)</div>
    * [Complexité amortie](chap_algorithmique_complexite_amortie.html) <div class="ui small button">[PDF](chap_algorithmique_complexite_amortie.pdf)</div>
    * Algorithmique exacte **En réorganisation**
        * [Recherche par force brute](chap_algorithmique_force_brute.html) <div class="ui small button">[PDF](chap_algorithmique_force_brute.pdf)</div> 
        * [Algorithmes gloutons](chap_algorithmique_gloutons.html) <div class="ui small button">[PDF](chap_algorithmique_gloutons.pdf)</div> 
        * [Diviser pour régner](chap_algorithmique_diviser_pour_regner.html) <div class="ui small button">[PDF](chap_algorithmique_diviser_pour_regner.pdf)</div>
        * [Programmation dynamique](chap_algorithmique_programmation_dynamique.html) <div class="ui small button">[PDF](chap_algorithmique_programmation_dynamique.pdf)</div>
    * [Algorithmique du texte](chap_algorithmique_algorithmique_du_texte.html) <div class="ui small button">[PDF](chap_algorithmique_algorithmique_du_texte.pdf)</div>
    * Algorithmique approchée
    * [Algorithmique des graphes avancée](chap_algorithmique_graphes_avances.html) <div class="ui small button">[PDF](chap_algorithmique_graphes_avances.pdf)</div>
    * Intelligence artificielle
* Systèmes
    * [Gestion de la mémoire d'un programme](chap_systeme_memoire.html) <div class="ui small button">[PDF](chap_systeme_memoire.pdf)</div>
    * Fichiers et entrées/sorties
    * Concurrence et synchronisation
* Logique
    * [Calcul propositionnel](chap_logique_calculprop.html) <div class="ui small button">[PDF](chap_logique_calculprop.pdf)</div>
* [Bases de données](chap_bdd_bdd.html) <div class="ui small button">[PDF](chap_bdd_bdd.pdf)</div>
* Langages
    * [Langage réguliers et Automates finis](chap_langages_langages_reguliers_automates.html) <div class="ui small button">[PDF](chap_langages_langages_reguliers_automates.pdf)</div>
        * [Fiche](chap_langages_langages_reguliers_automates_fiche.pdf)
        * [Exercices](chap_langages_langages_reguliers_automates_exos.pdf)

* Problèmes
    * [Dungeons&Diagrams](chap_problemes_d_and_diagrams.html) <div class="ui small button">[PDF](chap_problemes_d_and_diagrams.pdf)</div> Un problème
      explorant le backtracking d'un casse-tête en C en exploitant au maximum
      les représentations binaires des entiers.
