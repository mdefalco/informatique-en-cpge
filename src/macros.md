!define[1](widefigure)
~~~
`\begin{figure*}[tbp]`{=tex}

!1

`\end{figure*}`{=tex}
~~~

!define[1](center)
~~~
`\begin{center}`{=tex}!1`\end{center}`{=tex}
~~~

!define[1](sidenote)
~~~
`\sidenote{\footnotesize{`{=tex}!1`}}`{=tex}
~~~

!define[1](marginnote)
~~~
`\marginnote{\footnotesize{`{=tex}!1`}}`{=tex}
~~~

!define[1](part)
~~~
`\part{!1}`{=tex}
~~~
!define[1](chapter)
~~~
`\chapter{!1}`{=tex}
~~~
!define[1](section)
~~~

# !1

~~~
!define[1](subsection)
~~~

## !1

~~~
!define[1](subsubsection)
~~~

### !1

~~~
!define[1](paragraph)
~~~

#### !1

~~~


!define[1](image)
~~~
![](../html/assets/pics/!1)
~~~


!define[2](chapterimage)
~~~
`\chapterimage{../html/assets/pics/!1}`{=tex}
`\chapter{!2}`{=tex}
~~~

!define[1](definition)
~~~
`\begin{definition}`{=tex}

!1

`\end{definition}`{=tex}
~~~
!define[1](lemme)
~~~
`\begin{lemme}`{=tex}

!1

`\end{lemme}`{=tex}
~~~
!define[1](corollaire)
~~~
`\begin{corollaire}`{=tex}

!1

`\end{corollaire}`{=tex}
~~~


!define[1](question)
~~~
`\begin{question}`{=tex}

!1

`\end{question}`{=tex}
~~~

!define[1](theoreme)
~~~
`\begin{theoreme}`{=tex}

!1

`\end{theoreme}`{=tex}
~~~
!define[1](reponse)
~~~
`\begin{proof}
~`{=tex}

!1

`\end{proof}`{=tex}
~~~


!define[1](preuve)
~~~
`\begin{proof}
~`{=tex}

!1

`\end{proof}`{=tex}
~~~

!define[1](exercice)
~~~
`\begin{exercice}`{=tex}

!1

`\end{exercice}`{=tex}
~~~

!define[1](complement)
~~~
`\begin{remarque}`{=tex}

!1

`\end{remarque}`{=tex}
~~~


!define[1](remarque)
~~~
`\begin{remarque}`{=tex}

!1

`\end{remarque}`{=tex}
~~~

!define[1](exemple)
~~~
`\begin{exemple}`{=tex}

!1

`\end{exemple}`{=tex}
~~~

!define[8](listing4)
```

~~~~~ {.!1}
!5
~~~~~

```

!define[6](langspec3)
```
`\begin{langspec}{`{=tex}!language`}`{=tex}
!iflangeq(!1)
~~~~~~~~~~~~
!4
~~~~~~~~~~~~
!iflangeq(!2)
~~~~~~~~~~~~
!5
~~~~~~~~~~~~
!iflangeq(!3)
~~~~~~~~~~~~
!6
~~~~~~~~~~~~
`\end{langspec}`{=tex}
```

!define[6](listing3)
```
!iflangeq(!1)
~~~~~~~~~~~~
!listing(!1)
~~~~
!4
~~~~
~~~~~~~~~~~~
!iflangeq(!2)
~~~~~~~~~~~~
!listing(!2)
~~~~
!5
~~~~
~~~~~~~~~~~~
!iflangeq(!3)
~~~~~~~~~~~~
!listing(!3)
~~~~
!6
~~~~
~~~~~~~~~~~~
```

!define[3](listinglit)
```
!listing(!1)
~~~~~
!rawinclude[!2](!3)
~~~~~
```

!define[2](listing3l)
```
!listing3(ocaml)(c)(python)
~~~~
!rawinclude[!1](!2.ml)
~~~~
~~~~
!rawinclude[!1](!2.c)
~~~~
~~~~
!rawinclude[!1](!2.py)
~~~~
```

!define[2](listing)
```
`\begin{langspec}{`{=tex}!capitalize(!1)`}`{=tex}

~~~~~ {.!1}
!2
~~~~~

`\end{langspec}`{=tex}
```

!define[1](note)
~~~
`\begin{note}`{=tex}

!1

`\end{note}`{=tex}
~~~

!define[2][1](twocolumns)
~~~
!if(!O1)
```
`\begin{tabular}{m{!eval(14 / 16 * !O1)cm}|m{!eval(14 - 14 / 16 * !O1)cm}}`{=tex}

!1

`&`{=tex}

!2

`\\`{=tex}
`\end{tabular}`{=tex}
```
```
`\begin{tabular}{m{7cm}|m{7cm}}`{=tex}

!1

`&`{=tex}

!2

`\\`{=tex}
`\end{tabular}`{=tex}
```
~~~

!define[3](algorithme)
~~~
`\begin{mdframed}\begin{flushleft}\textbf{Algorithme} - \textsc{!1}`{=tex}
`\begin{itemize}
\item`{=tex} Entrées :

!2 

`\item`{=tex}

!3

`\end{itemize}\end{flushleft}\end{mdframed}`{=tex}
~~~



!define[3](probleme)
~~~
`\begin{mdframed}\begin{flushleft}\textbf{Problème} - \textsc{!1}`{=tex}
`\begin{itemize}
\item`{=tex} Entrées :

!2 

`\item`{=tex} Sortie : 

!3

`\end{itemize}\end{flushleft}\end{mdframed}`{=tex}
~~~

!define(clearpage)
~~~
`\clearpage`{=tex}
~~~

!define[2](classe)
~~~
`\fbox{!1 - !2}`{=tex}
~~~

!define(OISup)
~~~
!classe(OI)(Sup)
~~~

!define(OISpe)
~~~
!classe(OI)(Spé)
~~~

!define(ITCSpe)
~~~
!classe(ITC)(Spé)
~~~

!define(ITCSup)
~~~
!classe(ITC)(Sup)
~~~

!define(MP2IS1)
~~~
!classe(MP2I)(S1)
~~~

!define(MP2IS2)
~~~
!classe(MP2I)(S2)
~~~

!define(MPI)
~~~
`\fbox{MPI}`{=tex}
~~~

!define(and)(\&)
!define[1](tikzre)
~~~
![](../pics/!1.pdf)
~~~

!define(noheaders)
~~~
`\pagestyle{empty}`{=tex}
~~~

!define(headers)
~~~
`\pagestyle{fancy}`{=tex}
~~~

!define[1](jupyter)
~~~
~~~
