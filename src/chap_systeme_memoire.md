---
title: Informatique MPI
subtitle: Gestion de la mémoire dans un programme compilé
author: Marc de Falco
autoSectionLabels: true
---

!iftex
```
!debug(Printing for tex)
!include(macros_chap.md)
```

!ifhtml
```
!debug(Html output)
!include(macros_html.md)
```

!include(systeme/memoire.md)