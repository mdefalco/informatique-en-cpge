!chapterimage(chap_algo_exacte.png)(Algorithmique exacte)

*Source image : https://www.flickr.com/photos/x6e38/3440634940/*

!remarque(Dans ce chapitre, on étudie des problèmes pour lesquels on va
exprimer des algorithmes permettant d'obtenir des solutions exactes. C'est à
contraster avec le chapitre sur l'algorithmique approchée.)

Une grande partie des stratégies de résolution est basée sur la résolution de
sous-problèmes. On verra ainsi trois types de stratégies résumées dans le
schéma suivant :

!center
~~~~~~~
!tikz(strategies_sousproblemes)
```
    \node at (5,3) {Stratégies de résolution basées sur des sous-problèmes};

\begin{scope}[xshift=0cm]
        \draw[orange,fill=orange!60] (1,1.5) ellipse (4.2em and 3em);
        \draw[orange,fill=orange!40] (.85,1.75) ellipse (3em and 2em);
        \draw[orange,fill=orange!20] (0.5,1.5) ellipse (1em and 1em);
        \fill[red] (0.5,1.5) circle (0.1em);
        \fill[red!70] (1.5,2) circle (0.1em);
        \fill[red!50] (2,1) circle (0.1em);
        \node[scale=0.8] at (1,-0.25) {Algorithme Glouton};
\node[scale=0.4] at (1,-.5) {Optimal = Choix Glouton + Sous-problème optimal};
\end{scope}

\begin{scope}[xshift=4cm]
    \begin{scope}[xscale=0.5,xshift=0cm,yshift=1.4cm,yscale=0.35]
        \draw[orange,fill=orange!20] (0,0.5) .. controls (-1,.75) and (-1,2.25) ..
        (0,2.5) -- (0,0.5);
        %\draw[orange,fill=orange!20] (2,0.5) .. controls (3,.75) and (3,2.25) ..
        %(2,2.5) -- (2,0.5);
        \draw[orange,fill=orange!20] (.25,0.5) rectangle (1.75,2.5);

        \draw[orange,dashed,thick] (0.125,0.5) -- (0.125,2.5)
            %(1.875,0.5) -- (1.875,2.5)
        ;
    \end{scope}

    \begin{scope}[xscale=0.5,xshift=2cm,yshift=1.4cm,yscale=0.35]
        %\draw[orange,fill=orange!20] (0,0.5) .. controls (-1,.75) and (-1,2.25) ..
        %(0,2.5) -- (0,0.5);
        \draw[orange,fill=orange!20] (2,0.5) .. controls (3,.75) and (3,2.25) ..
        (2,2.5) -- (2,0.5);
        \draw[orange,fill=orange!20] (.25,0.5) rectangle (1.75,2.5);

        \draw[orange,dashed,thick] % (0.125,0.5) -- (0.125,2.5)
            (1.875,0.5) -- (1.875,2.5)
        ;
    \end{scope}

    \begin{scope}[xscale=0.5,xshift=0cm,yshift=1cm,yscale=0.35]
        \draw[orange,fill=orange!40] (0,0.5) .. controls (-1,.75) and (-1,2.25) ..
        (0,2.5) -- (1.75,2.5) -- (1.75,0.5) -- (0,0.5);
        
        \draw[orange,fill=orange!40] (4,0.5) .. controls (5,.75) and (5,2.25) ..
        (4,2.5) -- (2.25,2.5) -- (2.25,0.5) -- (4,0.5);

        \draw[orange,dashed,thick] (2,0.5) -- (2,2.5)
        ;
    \end{scope}

    \begin{scope}[xscale=0.5,xshift=0cm,yshift=0.5cm,yscale=0.35]
        \draw[orange,fill=orange!60] (0,0.5) .. controls (-1,.75) and (-1,2.25) ..
        (0,2.5) -- (1.75,2.5) -- (2.25,2.5) --
        (4,2.5) .. controls (5,2.25) and (5,0.75) ..
        (4,0.5) -- (0,0.5);
    \end{scope}

\iffalse
    \draw[<-,red] 
        (-0.15,2.35) -- (0.25,1.5);
    \draw[<-,red] 
        (0.5,2.35) -- (0.25,1.5);

    \draw[<-,red] 
        (2.15,2.35) -- (1.75,1.5);
    \draw[<-,red] 
        (1.5,2.35) -- (1.75,1.5);

    \draw[<-,red] 
        (0.25,1.25) -- (1,0.25);
    \draw[<-,red] 
        (1.75,1.25) -- (1,0.25);
\fi


        \node[scale=0.8] at (1,-0.25) {Diviser pour Régner};
\node[scale=0.4] at (1,-.5) {Optimal = Sous-problèmes disjoints + Fusion};
\end{scope}

\begin{scope}[xshift=8cm]
    \draw[orange,fill=orange!10] (0,1) circle (1em);
    \draw[orange,fill=orange!20] (0.5,2.2) circle (1em);
        \draw[orange,fill=orange!30] (1,1) circle (1em);
        \draw[orange,fill=orange!40] (1.5,2.5) circle (1em);
        \draw[orange,fill=orange!50] (2,1) circle (1em);
        \draw[orange,fill=orange!60] (0.5,1.5) circle (1.5em);
        \draw[orange,fill=orange!70] (1.5,1.5) circle (2em);

        \draw[red,->] (0.5,1.5) -- (0,1);
        \draw[red,->] (0.5,1.5) -- (0.85,0.85);
        \draw[red,->] (0.5,1.5) -- (0.5,2.2);
        \draw[red,->] (1.5,1.5) -- (0.6,1.5);
        \draw[red,->] (1.5,1.5) -- (0.9,0.9);
        \draw[red,->] (1.5,1.5) -- (1.5,2.5);
        \draw[red,->] (1.5,1.5) -- (2.2,0.85);

        \node[scale=0.8] at (1,-0.25) {Programmation dynamique};
\node[scale=0.4] at (1,-.5) {Optimal = Sous-problèmes superposés + Stratégie
de calcul};
\end{scope}
```
~~~~~~~

!section(Recherche par force brute)

!subsection(Principe)

Considérons un problème du type trouver un $x \in V$ vérifiant une propriété
$P(x)$. Par exemple, $V$ est l'ensemble des chaînes de caractère et $P$ vérifie
si une chaîne est un mot de passe qu'on cherche. Dans certains problèmes, un
tel $x$ n'est pas unique et on cherche à tous les énumérer.

Une recherche par force brute, ou recherche exhaustive, consiste à parcourir
l'ensemble $V$ jusqu'à obtenir une solution. Pour la recherche du mot de passe,
on pourrait commencer par énumérer les chaînes de longueur 1, puis de longueur
2, et ainsi de suite. 

Le plus souvent, l'ensemble $V$ est fini (pour les mots
de passe, cela peut consister à limiter la longueur maximale du mot de passe).
Ainsi, une recherche par force brute effectue $O(|V|)$ itérations.

Considérons le problème *PlusProchePaire* qui, étant donné un ensemble de
$n$ points ($n \ge 2$), détermine la paire constituée des deux points les plus proches.

!center
~~~~
!tikz(plusprochepaire_a)
```
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n7) at (0.39,1.62) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n5) at (0.50,0.61) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n2) at (0.81,1.68) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n0) at (1.37,0.55) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n1) at (1.68,1.59) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n8) at (2.22,2.40) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n3) at (2.41,1.62) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n6) at (2.56,2.62) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n4) at (2.62,0.60) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n9) at (2.67,1.32) {};
\draw [thick, red] (n3) edge (n9);
```
~~~~

Une implémentation naïve de la recherche par force brute consiste à énumérer
les $\frac{n(n-1)}{2}$ paires et donc à effectuer $O(n^2)$ itérations.

!listing(ocaml)
```
let plus_proche_paire points =
    let n = Array.length points in
    let min_paire = ref (distance points.(0) points.(1), (0, 1)) in
    for i = 0 to n - 1 do
        for j = i+1 to n - 1 do
            let d = distance points.(i) points.(j) in
            if d < fst !min_paire
            then min_paire := (d, (i, j))
        done
    done;
    snd !min_paire
```
!subsection(Raffinement : droite de balayage)

Il est parfois possible d'accélérer la recherche par force brute en ordonnant
le parcours des candidats pour pouvoir éviter de tester certains d'entres eux.

En géométrie algorithmique, une approche classique consiste à ordonner les
objets selon leur abscisse et à parcourir les objets par abscisse croissante. 
On parle alors de **droite de balayage** (en anglais, *sweep line*) car cela
revient à balayer le plan par une droite verticale en ne traitant que les
objets avant cette ligne.

Reprenons le problème précédent, on considère que les points sont triés par
abscisse croissante : $(x_0,y_0), \dots, (x_{n-1}, y_{n-1})$. On va parcourir
les points dans cet ordre en maintenant un ensemble de points à gauche du
point courant, appelés *points actifs*, et en ne calculant que les intersections 
avec les points actifs.

Si on a parcouru les $N$ premiers points et qu'on a obtenu que la
plus petite distance était $d$, lorsqu'on considère le point $(x_N,y_N)$, il
est inutile de tester les points qui sont forcément à distance $> d$ de
celui-ci. C'est-à-dire qu'on peut éliminer les points qui ne sont pas dans le
rectangle $[x_N-d,x_N]\times [y_N-d,y_N+d]$ du test. Les points dont l'abscisse
est $< x_N -d$ peuvent être éliminés définitivement vu que l'on raisonne par
abscisse croissante, par contre, les points d'ordonnées invalides doivent être
conservés pour les points ultérieurs.

Ce rectangle est représenté sur le schéma suivant ainsi qu'une ligne imaginaire
qui correspond à l'abscisse du point courant et qu'on peut imaginer parcourant
le plan de gauche à droite pour traiter les points au fur et à mesure.

!center
~~~~
!tikz(plusprochepaire_b)
```
\fill [red!20] (0,0) rectangle (1,2);
\draw [red,dotted] (1,-0.5) -- (1,3);

\draw[thick,red] (0,1) -- node[above right] {$d$} (0.5,0.33);

\draw [thin] (0,-0.2) edge[<->] node[below] {$d$} (1,-0.2);
\draw [thin] (-0.2,0) edge[<->] node[left] {$d$} (-0.2,1);
\draw [thin] (-0.2,1) edge[<->] node[left] {$d$} (-0.2,2);

\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n0) at (0,1) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n1) at (0.75,2.5) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n2) at (0.5,0.33) {};
\node [inner sep=0.05cm, draw=red, fill=red!80, circle] (n2) at (1,1) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n1) at (1.25,1.5) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] (n1) at (1.5,0.5) {};
```
~~~~

Afin de déterminer la complexité de cet algorithme, il est nécessaire de
connaitre le nombre maximal de points dans le rectangle. Comme ces points ont
été pris en compte précédemment, ils sont forcément à distance au moins $d$ les
uns des autres. Il s'agit donc de déterminer le nombre maximum de points qu'on
peut placer dans ce rectangle à distance au moins $d$. On remarque tout d'abord
qu'on peut placer six points ainsi :

!center
~~~~
!tikz(plusprochepaire_activesize_a)
```
\fill[red!20] (0,0) rectangle (1,2);
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (0,0) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (0,1) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (0,2) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (1,0) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (1,1) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (1,2) {};
```
~~~~

Si jamais on avait au moins sept points, on peut voir qu'il y a forcément un
des six sous-rectangles suivants qui contiendrait au moins deux points :

!center
~~~~
!tikz(plusprochepaire_activesize_b)
```
\fill[blue!20,draw=blue] (0,0) rectangle (0.5,0.66);
\fill[yellow!20,draw=yellow] (0,0.66) rectangle (0.5,1.33);
\fill[orange!20,draw=orange] (0,1.33) rectangle (0.5,2);
\fill[green!20,draw=green] (0.5,0) rectangle (1,0.66);
\fill[red!20,draw=red] (0.5,0.66) rectangle (1,1.33);
\fill[purple!20,draw=purple] (0.5,1.33) rectangle (1,2);
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (.2,.4) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (.3,1) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (.1,1.5) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (.7,.2) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (.8,0.9) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (.6,1.2) {};
\node [inner sep=0.05cm, draw=black, fill=gray!80, circle] at (.55,1.7) {};
```
~~~~

Or, ces sous-rectangles sont de longueur $\frac{1}{2}d$ et de hauteur
$\frac{2}{3}d$, donc la distance maximale entre deux de leurs points correspond
à la longueur des diagonales : $\sqrt{\frac{1}{4} + \frac{4}{9}}d = \frac{5}{6}d
< d$.

Comme un de ces six points est le point courant, il y a toujours au plus 5
points dans l'ensemble des points actifs.

Voici le principe de l'algorithme que l'on va implémenter :

* On trie le tableau `points` par ordre croissant. __Complexité :__ $O(n \log n)$
* On initialise la plus petite distance `d` courante à la distance entre les
  deux premiers points
* On crée un ensemble `actifs`, ordonné par les ordonnées, de points contenant
  initialement les deux premiers points 
* Pour chaque point $(x,y)$ en partant du deuxième :

    * On supprime les points $(x',y')$ tels que $x' < x - d$ de `actifs`.
      __Complexité :__ sur l'ensemble des itérations on ne pourra jamais
      supprimer deux fois un point, donc on effectue au maximum $n$
      suppressions chacune en $O(\log n)$ donc $O(n \log n)$.
    * On parcourt les points de `actifs` dont les ordonnées sont comprises
    entre $y-d$ et $y+d$. __Complexité :__ pour récupérer le premier point de
    l'ensemble, il faut $O(\log n)$ en pire cas (tous les points actifs) et
    ensuite on effectue au plus 5 itérations comme on vient de le prouver.

!ifhtml
```
L'animation suivante présente le déroulement de cet algorihtme. La bande active
est indiquée en gris et le rectangle autour du point courant en gris foncé :

`<img class="ui image fluid" src="assets/pics/closest_pair.gif"/>`{=html}
```

On remarque ainsi que la complexité en temps et en pire cas de cet algorithme
est de $O(n \log n)$. Ici, le fait d'avoir la structure `actifs` ordonnée par 
les ordonnées est crucial pour garantir la complexité. Pour la réalisation
d'une structure d'ensemble ordonnée ayant ces complexité, voir le chapitre FIXME.

Ici, on utilise le module `Set` d'OCaml pour réaliser la structure d'ensemble,
pour cela on commence par créer le module `PointSet` pour les ensembles de
points :

!listing(ocaml)
```
module Point = struct
    type t = float * float
    let compare (x1,y1) (x2,y2) = Stdlib.compare y1 y2
end

module PointSet = Set.Make(Point)
```

Puis on définit une fonction permettant de parcourir les points entre 
deux ordonnées :

!listing(ocaml)
```
let set_iter_entre f set bas haut =
    try
        let e = PointSet.find_first (fun p -> snd p >= bas) set in
        let seq = PointSet.to_seq_from e set in
        let rec aux seq =
            match seq () with
            | Seq.Nil -> ()
            | Seq.Cons (p, seq_suite) -> 
                    if snd p <= haut
                    then begin
                        f p;
                        aux seq_suite
                    end
        in aux seq
    with Not_found -> ()
```

On implémente alors assez directement l'algorithme décrit précédemment :

!listing(ocaml)
```
let plus_proche_paire_balayage points =
    let compare (x1,y1) (x2,y2) =
        if x1 = x2
        then if y1 < y2 then -1 else 1
        else if x1 < x2 then -1 else 1
    in
    Array.sort compare points;
    let n = Array.length points in
    let d = ref (distance points.(0) points.(1)) in
    let couple = ref (points.(0), points.(1)) in
    let actifs = ref (PointSet.empty 
            |> PointSet.add points.(0) |> PointSet.add points.(1)) in

    let gauche = ref 0 in

    for i = 2 to n-1 do
        let xi, yi = points.(i) in
        
        while fst points.(!gauche) < xi -. !d do
            actifs := PointSet.remove points.(!gauche) !actifs;
            incr gauche
        done;

        set_iter_entre (fun pj -> 
            let dip = distance points.(i) pj in
            if dip < !d
            then begin
                couple := (points.(i), pj);
                d := dip
            end) !actifs (yi -. !d) (yi +. !d);

        actifs := PointSet.add points.(i) !actifs
    done;
    !d
```

!subsubsection(Problème : test d'intersection pour un ensemble de segments)

Considérons le problème suivant *IntersectionEnsemble* : étant donné $n$
segments dans le plan, il s'agit de déterminer si au moins deux des segments
s'intersectent.


!remarque
`````
On peut considérer ici que l'on dispose d'une fonction 

!listing(ocaml)
~~~
intersecte : (float * float) * (float * float) 
    -> (float * float) * (float * float) -> bool 
~~~

qui teste l'intersection entre deux segments.

Cependant, il est possible d'écrire une telle fonction avec un peu de géométrie
élémentaire.

Si on considère que les deux segments sont $[A_1B_1]$ et $[A_2B_2]$, avec $A_1
\neq B_1$ et $A_2 \neq B_2$, alors
chaque point du segment $[A_1B_1]$ est de la forme $A_1 + t \overrightarrow{A_1B_1}$ 
où $t \in [0,1]$. De même les points du segments $[A_2B_2]$ sont de la forme
$A_2 + u \overrightarrow{A_2B_2}$ où $u \in [0,1]$.

S'il y a une intersection, c'est qu'il existe $(t,u) \in [0,1]^2$ tel que

$$
A_1 + t \overrightarrow{A_1B_1} = A_2 + u \overrightarrow{A_2B_2}
\iff \overrightarrow{A_2 A_1} + t \overrightarrow{A_1B_1} = u
\overrightarrow{A_2 B_2}
$$

L'idée est alors d'utiliser une opération appelée **produit vectoriel** sur les
vecteurs. Comme ici, tout est plan, le produit vectoriel est uniquement
déterminé par sa troisième coordonnée, celle qui sort du plan, et on peut se
contenter de calculer celle-ci. On note ainsi $(x,y) \times (x',y') = x y' - y
x'$ cette coordonnée. On a donc $u \times u = 0$.

On peut alors composer l'égalité par $\times \overrightarrow{A_2B_2}$ :

$$
\overrightarrow{A_2 A_1} \times \overrightarrow{A_2 B_2} + t 
\left( \overrightarrow{A_1 B_1} \times \overrightarrow{A_2 B_2} \right) = 0
$$

Notons $\Delta = \overrightarrow{A_1 B_1} \times \overrightarrow{A_2 B_2}$, si
$\Delta \neq 0$, alors 

$$
t = - \frac{\overrightarrow{A_2 A_1} \times \overrightarrow{A_2 B_2}}{\Delta}
= \frac{\overrightarrow{A_1 A_2} \times \overrightarrow{A_2 B_2}}{\Delta}
$$

On procède de même avec $\times \overrightarrow{A_1 B_1}$ pour obtenir une
expression de $u$ : $\overrightarrow{A_2 A_1} \times \overrightarrow{A_1 B_1}
= u \left ( \overrightarrow{A_2 B_2} \times \overrightarrow{A_1 B_1} \right)
= - u \Delta$ et donc

$$
u = - \frac{\overrightarrow{A_2 A_1} \times \overrightarrow{A_1 B_1}}{\Delta}
= \frac{\overrightarrow{A_1 A_2} \times \overrightarrow{A_1 B_1}}{\Delta}
$$

Si $\Delta \neq 0$, on peut donc alors exprimer $u$ et $t$ et vérifier qu'ils
sont dans $[0,1]$.

Si $\Delta = 0$ c'est que les deux segments sont de directions parallèles ou
confondues. 

* Si $\overrightarrow{A_1 A_2} \times \overrightarrow{A_1 B_1} \neq 0$
alors $\overrightarrow{A_1 A_2}$ et $\overrightarrow{A_1 B_1}$ sont non
colinéaires donc les deux segments sont sur des droites parallèles distinctes
et ne peuvent s'intersecter. 
* Sinon, les segments reposent sur une même droite
et il s'agit de vérifier leurs positions sur la droite. Pour cela, on exprime
$A_2 = A_1 + t_A \overrightarrow{A_1B_1}$ de
même pour $B_2 = A_1 + t_B \overrightarrow{A_1 B_1}$. 
Plus précisement, on calcule $\overrightarrow{A_1 A_2} \cdot 
\overrightarrow{A_1 B_1} = t_A ||\overrightarrow{A_1 B_1}||^2$ à l'aide du
produit scalaire et on a $t_A = \frac{\overrightarrow{A_1 A_2} \cdot
\overrightarrow{A_1 B_1}}{||\overrightarrow{A_1 B_1}||^2}$. De même, $t_B =
\frac{\overrightarrow{A_1 B_2} \cdot \overrightarrow{A_1
B_1}}{||\overrightarrow{A_1 B_1}||^2}$. On doit alors vérifier si l'intervalle
$[t_A,t_B]$ (ou $[t_B,t_A]$ selon leur position) intersecte $[0,1]$.

Voici une fonction *OCaml* qui correspond à ce raisonnement
!listing(ocaml)
```
let intersecte (a1,b1) (a2,b2) =
    let vec (x1,y1) (x2,y2) = (x2-.x1,y2-.y1) in
    let cross (x1,y1) (x2,y2) = x1 *. y2 -. y1 *. x2 in
    let dot (x1,y1) (x2,y2) = x1 *. x2 +. y1 *. y2 in
    let proche0 x = let eps = 1e-20 in 
        if x < 0. then -.x < eps else x < eps in
    let a1b1 = vec a1 b1 in let a2b2 = vec a2 b2 in
    let a1a2 = vec a1 a2 in let a1b2 = vec a1 b2 in

    let delta = cross a1b1 a2b2 in

    if proche0 delta
    then
         if proche0 (cross a1a2 a1b1)
         then let na1b1 = dot a1b1 a1b1 in (* colinéaires *)
              let tA = (dot a1a2 a1b1) /. na1b1 in
              let tB = (dot a1b2 a1b1) /. na1b1 in
              if tA < tB
              then not (tB < 0. || tA > 1.)
              else not (tA < 0. || tB > 1.)
         else false (* parallèles *)
    else let t = (cross a1a2 a2b2) /. delta in (* se croisent *)
         let u = (cross a1a2 a1b1) /. delta in
         t >= 0. && t <= 1. && u >= 0. && u <= 1.
```
 
`````

!note(réécrire cela avec le déterminant de deux vecteurs du plan qui est au
programme de mathématiques de seconde.)

La recherche par force brute va alors énumérer l'ensemble des paires de
segments distincts et tester deux à deux les intersections. On peut ainsi
écrire le programme suivant qui est assez simple et effectuera effectivement
$O(|v|^2)$ itérations dans le pire cas, i.e. lorsqu'il n'y a pas
d'intersections.

!listing(ocaml)
```
exception Trouve

let intersection_ensemble (v: ((float * float) * (float * float)) array) : bool =
    let n = Array.length v in
    try
        for i = 0 to n - 1 do
            for j = i+1 to n-1 do
                if intersecte v.(i) v.(j)
                then raise Trouve
            done
        done;
        false
    with Trouve -> true
```

TODO approche par droite de balayage : algorithme de Shamos et Hoey (1976)

!subsection(Recherche par retour sur trace (**backtracking**))

Dans des problèmes admettant des solutions partielles, on peut construire une
solution par essai de toutes les possibilités en complétant tant qu'on a bien
une solution partielle. La recherche par retour sur trace repose sur ce constat
pour énumérer l'ensemble des solutions en utilisant la récursivité (d'où la
notion de *retour sur trace*) pour les essais.

L'exemble classique de ce problème est celui des huit reines : étant donné un
échiquier, peut-on placer huit reines de sorte qu'aucune reine ne puisse
prendre une autre reine ? Plus précisément : sur un plateau de 8x8 cases, peut-on
placer huit pions tels que deux pions quelconques ne soient jamais sur la même
ligne ou la même diagonale ?

Exemple de solution :

!latex(huit_reines_ex)
```
\setchessboard{smallboard,showmover=false}
\chessboard[setpieces={qa2,qf1,qe3,qb4,qh5,qc6,qg7,qd8}]
```

Ce problème admet effectivement des solutions partielles en ne considérant que
$k$ reines à placer. Pour énumérer les solutions, on peut même se contenter de
solutions partielles où les $k$ reines sont placées sur les $k$ premières
rangées.

Voici ainsi un algorithme pour énumérer les solutions :

* Supposons que $k$ reines aient été placées et qu'on dispose d'une solution
  partielle.

    * Si $k = 8$ alors toutes les reines sont placées et la solution est
      complète, on la comptabilise
    * Sinon, on continue la recherche pour chaque position de la $k+1$ reine sur
      la $k+1$ rangée qui préserve le fait d'être une solution partielle.

Ici, quand on dit qu'on continue la recherche, ce qu'on signifie c'est qu'on
effectue un appel récursif.

Pour programmer cette méthode, on va définir une fonction récursive de
signature :

!listing(ocaml)
```
val resout_reines : (int * int) list -> (int * int) list list
```

Un appel à `resout_reines part` va ainsi renvoyer la liste des solutions
complètes construites à partir de la solution partielle `part`. Les solutions
sont représentées par des listes de couples de coordonnées sur l'échiquier,
donc dans $[|0;7|]^2$

Voici une implémentation où on explore les solutions à l'aide d'une boucle
impérative dans l'appel récursif. La fonction `valide` permet de tester si le
placement d'une reine est possible avant d'effectuer un appel.

!listing(ocaml)
```
let rec valide (x1,y1) l =
    match l with
    | [] -> true
    | (x2,y2)::q ->
        x1 <> x2 && abs (x2-x1) <> abs(y2-y1) && valide (x1,y1) q

let rec resout_reines part =
    let k = List.length part in
    if k = 8 
    then [ part ]
    else begin
        let resultats = ref [] in
        for x = 0 to 7 do
            let essai = (x,k) :: part in
            if valide (x,k) part
            then begin
                resultats := (resout_reines essai) @ !resultats;
            end
        done;
        !resultats
    end
```

et, ici, une autre implémentation purement récursive à l'aide d'une fonction
récursive.

!listing(ocaml)
```
let rec resout_reines part =
    let k = List.length part in
    if k = 8 
    then [ part ]
    else 
        let rec aux x acc =
            if x < 0
            then acc
            else let essai = (x,k) :: part in
                 let nacc = if valide (x,k) part
                           then (resout_reines essai) @ acc
                           else acc in
                    aux (x-1) nacc
        in
        aux 7 [] 
```

Une partie de l'arbre de recherche est présenté sur l'image suivante :

![Arbre de recherche pour les huit reines](../assets/pics/reines_arbre.png){width=1000}

L'arbre complet comporte 2057 noeuds dont 92 feuilles correspondant aux
solutions du problème. A titre de comparaison, l'arbre exhaustif correspondant
à faire tous les choix de placement à raison d'une reine par ligne compterait
$8^8 = 16777216$ noeuds. On voit bien que le backtracking est plus économe en
exploration.

!subsubsection(Problème : résolution de Sudoku)
La recherche par retour sur trace se prête très bien à la résolution de
problèmes comme le Sudoku. On va ici tout simplement tenter de remplir chaque
case du haut vers le bas tant qu'on satisfait les contraintes du Sudoku.
Le programme sera ainsi très proche de la résolution des huit reines.

Commençons par rappeler le principe du Sudoku :

* On part d'une grille de 81 cases réparties en une grille de 3x3 sous-grilles
  de 3x3 cases et comportant des chiffres de 1 à 9 dans certaines cases.
!tikz(sudoku_exemple)
```
\matrix (m) [matrix of math nodes,
    row sep=1mm,column sep=1mm, align=center,
    every node/.append style={ anchor=base,text height=2ex,text width=1em}]
   {{1}&{}&{} & {}&{}&{} & {}&{}&{6} \\
   {}&{}&{6} & {}&{2}&{} & {7}&{}&{} \\
   {7}&{8}&{9} & {4}&{5}&{} & {1}&{}&{3} \\
   {}&{}&{} & {8}&{}&{7} & {}&{}&{4} \\
   {}&{}&{} & {}&{3}&{} & {}&{}&{} \\
   {}&{9}&{} & {}&{}&{4} & {2}&{}&{1} \\
   {3}&{1}&{2} & {9}&{7}&{} & {}&{4}&{} \\
   {}&{4}&{} & {}&{1}&{2} & {}&{7}&{8} \\
   {9}&{}&{8} & {}&{}&{} & {}&{}&{} \\
   };

\draw[thick] 
(m-1-1.north west) -- (m-1-9.north east)
(m-4-1.north west) -- (m-4-9.north east)
(m-7-1.north west) -- (m-7-9.north east)
(m-9-1.south west) -- (m-9-9.south east)
(m-1-1.north west) -- (m-9-1.south west)
(m-1-4.north west) -- (m-9-4.south west)
(m-1-7.north west) -- (m-9-7.south west)
(m-1-9.north east) -- (m-9-9.south east)
;
\draw[thin]
(m-2-1.north west) -- (m-2-9.north east)
(m-3-1.north west) -- (m-3-9.north east)
(m-5-1.north west) -- (m-5-9.north east)
(m-6-1.north west) -- (m-6-9.north east)
(m-8-1.north west) -- (m-8-9.north east)
(m-9-1.north west) -- (m-9-9.north east)
(m-1-2.north west) -- (m-9-2.south west)
(m-1-3.north west) -- (m-9-3.south west)
(m-1-5.north west) -- (m-9-5.south west)
(m-1-6.north west) -- (m-9-6.south west)
(m-1-8.north west) -- (m-9-8.south west)
(m-1-9.north west) -- (m-9-9.south west)
;
```

* L'objectif est de remplir chaque case avec un chiffre de 1 à 9 de sorte que
  chaque ligne, chaque colonne et chaque sous-grille 3x3 comporte une et une
  seule fois chaque chiffre.
* Un sudoku admet une unique solution.

Pour représenter une grille de Sudoku en `OCaml` on utilise un 
`(int option) array array`, la valeur `None` signifiant que la case est vide et 
la valeur `Some x` qu'elle est remplie avec la valeur $x$.

!listing(ocaml)
```
type grille = (int option) array array
```

On fait le choix de représenté la grille par un tableau de lignes, ce qui
signiie que pour accèder à la case de coordonnée $(x,y)$ dans `g` il faut
écrire `g.(y).(x)`.

Le problème donnée précédemment est alors représenté par la valeur suivante :
!listing(ocaml)
```
let probleme = [|
    [| Some 1; None; None;    None; None; None;   None; None; Some 6 |];
    [| None; None; Some 6;    None; Some 2; None;   Some 7; None; None |];
    [| Some 7; Some 8; Some 9;    Some 4; Some 5; None;   Some 1; None; Some 3 |];

    [| None; None; None;    Some 8; None; Some 7;   None; None; Some 4 |];
    [| None; None; None;    None; Some 3; None;   None; None; None |];
    [| None; Some 9; None;    None; None; Some 4;   Some 2; None; Some 1 |];

    [| Some 3; Some 1; Some 2;    Some 9; Some 7; None;   None; Some 4; None |];
    [| None; Some 4; None;    None; Some 1; Some 2;   None; Some 7; Some 8 |];
    [| Some 9; None; Some 8;    None; None; None;   None; None; None |];
    |]
```

Afin de définir la fonction de résolution, on définit une première fonction
`suivant` de signature :

!listing(ocaml)
```
val suivant : grille -> (int * int) -> (int * int) option
```

telle que l'appel à `suivant g (x,y)` renvoie `Some (xi,yi)` quand $(x_i,y_i)$
sont les coordonnées de la prochaine case libre, dans l'ordre gauche à droite puis haut
vers bas, après $(x,y)$ ou `None` quand
il n'existe pas de telle case libre. Cela signifie alors que la grille est
entièrement remplie.

!listing(ocaml)
```
let rec suivant g (x,y) =
    if y > 8
    then None
    else if g.(y).(x) = None
    then Some (x,y)
    else if x < 8 then suivant g (x+1, y)
    else suivant g (0, y+1)
```

On définit également une fonction `valide` de signature

!listing(ocaml)
```
val valide : grille -> int -> int -> bool
```

telle que l'appel à `valide g x y` renvoie `true` si et seulement si la valeur
placée en coordonnée $(x,y)$ n'invalide pas la grille. Ne pas prendre cette
valeur en paramètre permettant d'écrire un peu plus simplement cette fonction.
La fonction est assez directe, étant donné $(x,y)$ on va parcourir sa ligne, sa
colonne et sa sous-grille pour vérifier qu'un nombre n'a pas été placé deux
fois à l'aide d'un tableau de drapeaux :

!listing(ocaml)
```
let valide g x y =
    let v = ref true in
    let vus_colonne = Array.make 9 false in
    for y0 = 0 to 8 do
        match g.(y0).(x) with
        | None -> ()
        | Some k -> 
                if vus_colonne.(k-1)
                then v := false;
                vus_colonne.(k-1) <- true
    done;
    let vus_ligne = Array.make 9 false in
    for x0 = 0 to 8 do
        match g.(y).(x0) with
        | None -> ()
        | Some k -> 
                if vus_ligne.(k-1)
                then v := false;
                vus_ligne.(k-1) <- true
    done;
    let vus_grille = Array.make 9 false in
    let xb = (x / 3) * 3 in
    let yb = (y / 3) * 3 in
    for xd = 0 to 2 do
        for yd = 0 to 2 do
            match g.(yb+yd).(xb+xd) with
            | None -> ()
            | Some k -> 
                    if vus_grille.(k-1)
                    then v := false;
                    vus_grille.(k-1) <- true
        done
    done;
    !v
```

On peut alors définir la fonction `resout` qui va résoudre le Sudoku en
effectuant tous les remplissages tant qu'on a une grille valide. Dès qu'une
solution est trouvé, on s'arrête. Pour cela, on utilise le mécanisme des
exceptions pour permettre une sortie prématurée. On a fait le choix de
travailler en place dans la grille, ainsi à la fin de l'exécution de la
fonction, la grille correspond à la solution.

!listing(ocaml)
```
exception Solution

let resout g =
    let rec aux xi yi = match suivant g (xi, yi) with
        | None -> raise Solution
        | Some (x,y) ->
            for i = 1 to 9 do
                g.(y).(x) <- Some i;
                if valide g x y
                then begin
                    aux x y
                end
            done;
            g.(y).(x) <- None
    in 
    try 
        aux 0 0
    with Solution -> ()
```

!ifhtml
```
La résolution du Sudoku donnée précédemment par ce programme est présenté dans
la vidéo suivante :

`<center><video width="400" height="380" controls="controls"><source src="assets/pics/sudoku.mp4" type="video/mp4" /></video></center>`{=html}
```

!comment
~~~~~~~~

!subsection(Stratégies d'énumération)
!subsubsection(Combinatoire élémentaire)


produits, combinaisons, permutations

!subsubsection(Enumération d'arbres)

Imaginons que l'on souhaite énumérer des arbres binaires non étiquettés pour trouver le
premier arbre binaire à $n$ noeuds vérifiant une certaine propriété. On suppos ainsi défini
un type

!listing(ocaml)
```
type arbre = Nil | Noeud of arbre * arbre
```

et une fonction pour tester le prédicat :

!listing(ocaml)
```
val : valide : arbre -> bool
```

Une première possibilité est d'effectuer un simple parcours récursif :

!listing(ocaml)
```
let rec recherche n =
    if n = 0
    then 
```


normale, par passage de continuation

~~~~~~~~


!section(Algorithmes gloutons)

!subsection(Principe)
On considère ici un problème d'énumération comme dans la section précédente
muni d'une fonction d'objectifs qui attribue une valeur numérique aux solutions
et aux solutions partielles.

Soit $f : P \rightarrow \R$ une telle fonction, où $S \cup P$ est l'ensemble
des solutions du problème d'énumération et $P$ l'ensemble des solutions
partielles, on se pose maintenant le problème de l'optimalité vis-à-vis de $f$
: déterminer $x \in S$ tel que $f(x) = \max_{y \in S} f(y)$ on note souvent
$x = \text{argmax}_{y\in S} f(y)$. On parle alors de problème d'optimisation
combinatoire.

!remarque
```


* En considérant $g : y \mapsto -f(y)$, on transforme un problème de maximisation
  en un problème de minimisation.
* Il y a une ambiguïté sur $\text{argmax}_{y \in S} f(y)$ quand plusieurs
  éléments de $S$ réalisent ce maximum. Dans la plupart des algorithmes
  gloutons qu'on va considérer, on commence par donner un ordre sur $S$ et on
  considère le plus petit $y$ pour cet ordre réalisant le maximum. L'ordre
  choisi est alors crucial dans la preuve de correction. C'est aussi une des
  raisons pour lesquelles les algorithmes gloutons sont souvent de complexité
  temporelle $O(n \log_2 n)$.

```

Une première stratégie très élémentaire consiste alors à énumérer $S$, de
manière exhaustive ou avec une stratégie plus fine comme le retour sur trace,
puis à déterminer un élément maximal de manière directe.

Cela revient donc à déterminer l'arbre des solutions puis à trouver une feuille
maximisant l'objectif :

!tikz(glouton_exhaustif)
```
\tikzstyle{level 1}=[sibling distance=10em] 
\tikzstyle{level 2}=[sibling distance=5em] 
\node 
    {$\bullet$}
    child { node {$\bullet$} 
        child { node {$\bullet$} }
        child { node {$\bullet$} 
            child { node[red] (s1) {$\bullet$}  }
        }
    }
    child { node {$\bullet$} 
        child { node {$\bullet$} 
            child { node[red] (s2) {$\bullet$}  }
            child { node[red] (s3) {$\bullet$}  }
        }
        child { node {$\bullet$}  }
    }
;
    \draw[rounded corners=0.5em,red] (s1.north west) rectangle  (s3.south east);
    \node[left=1em,red] at (s1) {$S$};
    \node[below=2em] (x) at (s2) {$x$};
    \draw (x) edge[->] (s2);
```

Un algorithme glouton va suivre une approche beaucoup plus efficace : à chaque
étape de construction de la solution, on choisit la branche qui maximise
la fonction d'objectif. C'est-à-dire que si en partant d'une solution partielle
$x \in P$ il est possible de l'étendre en d'autres solutions partielles $p_x =
\{ y_1, ..., y_n \}$, on va choisir $y = \text{argmax}_{t \in p_x}  f(t)$ la solution
qui maximise localement $f$.

Sur l'arbre précédent, cela reviendrait à n'emprunter qu'une seule branche :

!tikz(glouton_glouton1)
```
\tikzstyle{level 1}=[sibling distance=10em] 
\tikzstyle{level 2}=[sibling distance=5em] 
\node[red]
    {$\bullet$}
    child[black] { node {$\bullet$} 
        child { node {$\bullet$} }
        child { node {$\bullet$} 
            child { node (s1) {$\bullet$}  }
        }
    }
    child[red] { node {$\bullet$} 
        child { node {$\bullet$} 
            child { node[red] (s2) {$\bullet$}  }
            child[black] { node (s3) {$\bullet$}  }
        }
        child[black] { node {$\bullet$}  }
    }
;
    \draw[rounded corners=0.5em,red] (s1.north west) rectangle  (s3.south east);
    \node[left=1em,red] at (s1) {$S$};
    \node[below=2em] (x) at (s2) {$x$};
    \draw (x) edge[->] (s2);
```

Cela a l'air très efficace mais il y a un problème majeur : il n'y a aucune
garantie qu'on aboutisse ainsi à une solution, encore moins à une solution
optimale. En effet, on aurait très bien pu faire les choix suivants :

!tikz(glouton_glouton2)
```
\tikzstyle{level 1}=[sibling distance=10em] 
\tikzstyle{level 2}=[sibling distance=5em] 
\node[red] 
    {$\bullet$}
    child[red] { node {$\bullet$} 
        child { node {$\bullet$} }
        child[black] { node {$\bullet$} 
            child { node (s1) {$\bullet$}  }
        }
    }
    child[black] { node {$\bullet$} 
        child { node {$\bullet$} 
            child { node (s2) {$\bullet$}  }
            child { node (s3) {$\bullet$}  }
        }
        child { node {$\bullet$}  }
    }
;
    \draw[rounded corners=0.5em,red] (s1.north west) rectangle  (s3.south east);
    \node[left=1em,red] at (s1) {$S$};
    \node[below=2em] (x) at (s2) {$x$};
    \draw (x) edge[->] (s2);
```

et ne pas aboutir à une solution.

Considérons par exemple le problème du **rendu de monnaie** :
étant donné, une liste de valeurs faciales de pièces $P = (v_1,\dots,v_p) \in
(\N^*)^p$ avec $1 = v_1 < \dots < v_p$ et une somme $n \in \N^*$, on cherche la
manière d'exprimer cette somme avec le plus petit nombre de pièces possible.

Plus précisément, l'ensemble des solutions $S = \{ (k_1,\dots,k_p) \in N^p
~|~ k_1 v_1 + \dots + k_p v_p = n \}$ et la fonction d'objectif est $f :
(k_1,\dots,k_p) \mapsto k_1 + \dots + k_p$. Les solutions partielles ici sont
les réalisations de valeur $< n$. On cherche alors $x = argmin_{y \in S} f(y)$.

Comme $1 = v_1$, $S \neq \emptyset$ car $(n,0,\dots,0) \in S$ et ainsi $f(x)
\le n$.

L'algorithme glouton va utiliser la plus grande pièce possible à chaque étape
puis on applique l'algorithme glouton sur la somme restante sauf si elle est
nulle, ce qui constitue la condition d'arrêt.

**Exemple 1**

* $P = (1, 2, 5, 10)$
* $n = 14$
* On utilise la plus grande pièce possible $10 \le 14$ puis on exprime $4 = 14
  - 10$
* Ici, la plus grande pièce est $2$ et on continue avec $2 = 4 - 2$
* La plus grande pièce est encore $2$ et on s'arrête car $0 = 2 - 2$.
* En conclusion, on a obtenu $x = (0,2,0,1)$.
* Une exploration exhuastive permet de s'assurer qu'on a effectivement obtenu une
  décomposition minimale. En effet, ici l'ensemble des décompositions est : {
  (14,0,0,0), (12,1,0,0), (8,3,0,0), (6,4,0,0),
  (4,5,0,0),  (2,6,0,0),  (0,7,0,0), (9,0,1,0),
  (7,1,1,0),  (5,2,1,0),  (3,3,1,0), (1,4,1,0),
  (4,0,2,0),  (2,1,2,0),  (0,2,2,0), (4,0,0,1),
  (2,1,0,1),  (0,2,0,1) }.

**Exemple 2**

* $P = (1, 2, 7, 10)$
* $n = 14$
* L'algorithme glouton va ici procéder comme dans l'exemple 1 et on va obtenir
  $x = (0,2,0,1)$.
* Mais on remarque que ce n'est pas un minimum car $x' = (0,0,2,0)$ convient
  avec $f(x') = 2 < 3 = f(x)$.


*Conclusion* l'algorithme glouton n'a effectivement pas de raisons d'être
optimal.


On peut se poser la question des algorithmes pour lesquels l'algorithme glouton
aboutit nécessairement à une solution optimale. 

!note(TODO - Ajouter un paragraphe simple sur les matroïdes qui puisse se
décliner sous la forme d'un problème.)

!subsection(Construction de l'arbre de Huffman { #huffman-arbre })

!remarque
````
Ce paragraphe décrit l'étape cruciale du principe de compression de Huffman.
Celui-ci sera présenté complétement dans le chapitre !ref[Algorithmique des textes](sec:huffman).
````

On va étudier ici un principe de compression parfaite (sans perte d'information
à la décompression) de données appelé l'algorithme de Huffman et qui repose sur
ce principe simple : coder sur moins de bits les caractères les plus fréquents.

Par exemple si on considère le mot `abaabc`, en le codant avec un nombre de
bits fixes, par exemple 2 avec le code `a=00,b=01,c=10`, on aurait besoin de
12 bits pour représenter le mot. Mais si on choisit le code suivant :
`a=0,b=10,c=11`, il suffit de 9 bits. On a donc gagné 3 bits soit un facteur de
compression de 75%.

On remarque que pour pouvoir décompreser, il n'aurait pas été possible de faire
commencer le code de `b` ou `c` par un `0`, sinon on aurait eu une ambiguité
avec la lecture d'un `a`. On parle alors de code préfixe :

!definition
```
Soit $X \subset \{0,1\}^*$, on dit que $X$ est un code préfixe lorsque 
pour tous $x, y \in X$, $x$ n'est pas un préfixe de $y$ et 
$y$ n'est pas un préfixe de $x$.
```


On se pose alors la question du code préfixe optimal pour un texte donné.

Plus précisément, étant donné un alphabet fini $\Sigma$ et une application
$f : \Sigma \rightarrow [0,1]$ associant à chaque lettre son nombre
d'occurences dans le texte considéré. Ainsi $\sum_{x \in \Sigma} f(x)$ est la longueur du texte.
On cherche un code préfixe $X$ et une application $c : \Sigma \rightarrow X$ telle que 
$\sum_{x \in \Sigma} f(x) |c(x)|$  soit minimale car cela correspond au nombre
de bits après codage.

!remarque
~~~~
On utilise aussi la notion de fréquence du lettre qui est son nombre
d'occurence rapporté à la longueur du texte. Un des avantages de la notion de
fréquence est qu'il est possible de considèrer une table de fréquence déjà
construite comme celle de la langue française.
~~~~

L'application de codage $c$ peut être représenté par un arbre binaire où les arêtes gauches
correspondent à 0, les arêtes droites à 1 et les feuilles aux éléments de
$\Sigma$ dont les étiquettes des chemins y menant depuis la racine de l'arbre
correspondent à leur image par $c$.

Par exemple, pour le code $a=0,b=10,c=11$ on aurait l'arbre :

!latex(arbre_huffman_ex1)
```
\begin{forest}
[.
    [a,edge label={node[midway,left]{0}}]
    [.,edge label={node[midway,right]{1}}
        [b,edge label={node[midway,left]{0}}]
        [c,edge label={node[midway,right]{1}}]
    ]
]
\end{forest}
```

Avec un tel arbre, il est très simple de décoder le texte codé car il suffit de
suivre un chemin dans l'arbre jusqu'à tomber sur une feuille, produire la
lettre correspondante, puis repartir de la racine de l'arbre. La longueur du
code associé à une lettre est alors égale à la profondeur de la feuille
correspondante. L'optimalité du codage préfixe est ainsi équivalente à
la minimalité de l'arbre vis-à-vis de la fonction d'objectif $\varphi(t) = \sum_{x
\in \Sigma} f(x) p(t,x)$ où $p(t,x)$ est la profondeur de la feuille
d'étiquette $x$ dans l'arbre $t$ ou $0$ si $x$ n'est pas une des étiquettes,
cet extension permettant d'étendre la fonction d'objectif aux solutions
partielles.

L'algorithme d'Huffman va construire un arbre correspondant à un codage optimal
à l'aide d'une file de priorité d'arbres. On étend pour cela l'application $f$
à de tels arbres en définissant que si $t$ est un arbre de feuilles
$x_1,\dots,x_n$ alors $f(t) = f(x_1) + \dots + f(x_n)$.

* Au départ, on place dans la file des arbres réduits à une feuille pour chaque
  élément $x \in \Sigma$ et dont la priorité est $f(x)$.
* Tant que la file contient au moins deux éléments

    * on retire les deux plus petits éléments $x$ et $y$ de la file de priorité
      $f(x)$ et $f(y)$
    * on ajoute un arbre $z = Noeud(x,y)$ de priorité $f(z) = f(x) + f(y)$.
* On renvoie l'unique élément restant dans la file.

L'implémentation de cet algorithme est alors assez directe avec une file de
priorité. On réutilise ici la structure de tas implementée en FIXME. Comme il
s'agit d'un tas max, on insère avec $-f(x)$ comme valeur.

!listing3l(ARBRE)(../../snippets/algorithmique/huffman)

L'algorithme de Huffman est un algorithme glouton car si on considère 
pour solution partielle la fôret présente dans la file et pour objectif 
la fonction $\varphi$ étendue aux fôrets en sommant la valeur de $\varphi$ sur
chaque arbre, alors fusionner dans la fôret $F$ deux arbres $x$ et $y$ en la
transformant en une fôret $F'$ va avoir l'impact suivant sur la fonction
d'objectif :

$$
\varphi(F') = \varphi(F) + f(x) + f(y)
$$

car, en effet, on va rajouter 1 à la profondeur de chaque feuille et donc on
passe pour la contribution de $x$ de $\varphi(x) = \sum_{c \in \Sigma} f(c)
p(x,c)$ à $\sum_{c \in \Sigma} f(c) (p(x,c)+1) = \varphi(x) + \sum_{c \in
\Sigma} f(c) = \varphi(x) + f(x)$.

On remarque ainsi que la fusion qui minimise localement $\varphi$ est celle qui
fusionne les deux arbres de plus petite valeur pour $f$.

Pour montrer que l'algorithme glouton produit ici un codage minimal, on va
utiliser une technique classique qui consiste à montrer qu'étant donné une
solution optimale, on peut toujours la transformer sans augmenter sa valeur 
pour obtenir, de proche en proche, la solution renvoyée par le glouton.

!theoreme
```
Supposons que les lettres les moins fréquentes soient $a$ et $b$,
il existe un arbre optimal dont les deux feuilles étiquettées par $a$ et 
$b$ descendent du même noeud et sont de profondeur maximale.
```

!preuve
```
Considérons un arbre optimal $t$ et soient $c$ l'étiquette d'une feuille de
profondeur maximale. On remarque qu'elle a forcément une feuille sœur car
sinon, on pourrait omettre le noeud et l'arbre obtenu serait de plus petite
valeur par $\varphi$.

FIXME: dessin

Soit $d$ l'étiquette de cette feuille sœur. Sans perte de généralités, on
suppose que $f(c) \le f(d)$ et $f(a) \le f(b)$. Comme $a$ a le plus petit
nombre d'occurences, a $f(a) \le f(c)$ et comme $b$ est la deuxième, on a $f(b) \le
f(d)$. De plus, $p(t,a) \ge p(t,c)$ et $p(t,b) \ge p(t,d)$.

Si on échange les étiquettes $a$ et $c$, seule les termes associées à ces
lettres changent dans l'évaluation de $\varphi$. Si on note $t'$ le nouvel
arbre obtenu après cet échange, on a 
$$
\varphi(t') = \varphi(t) - f(a) p(t,a) - f(c) p(t,c) + f(a) p(t,c) + f(c)
p(t,a)
$$
Or, $f(c) \ge f(a)$ et $p(t,a) \ge p(t,c)$ donc
$$\varphi(t') = \varphi(t) + (f(c) - f(a))(p(t,a)-p(t,c))
\le \varphi(t)$$
L'échange préserve le caractère optimal. En fait, ici, on a nécessairement une
égalité pour ne pas aboutir à une contradiction, donc soit les feuilles étaient
à même profondeur, soit les lettres avaient le même nombre d'occurrences.

Comme on a les mêmes relations entre $b$ et $d$, on peut effectuer le même
argument et échanger les étiquettes en préservant le caractère optimal.
```

Le théorème suivant permet de raisonner par récurrence en diminuant le nombre
de lettres.

!theoreme
```
Soit $t$ un arbre ayant
$x$ et $y$ comme feuilles soeurs et $t'$ l'arbre
obtenu en remplaçant le noeud liant $x$ et $y$ par une feuille étiquettée par
$z$ où $z$ est une nouvelle lettre telle que $f(z) = f(x) + f(y)$.

!center
===========
!tikz(huffman_fusion)
~~~~
\draw (2,2) -- (1,0.5) -- (3,0.5) -- (2,2)
    (1.5,0.5) -- (1.5,0)
    (1.5,0) -- (1,-0.5)
    (1.5,0) -- (2,-0.5)
    ;
\fill (1.5,0) circle (2pt);
\node[below] at (1,-0.5) {$x$};
\node[below] at (2,-0.5) {$y$};

\node at (3.5,1) {$\rightarrow$};

\begin{scope}[xshift=3cm]
\draw (2,2) -- (1,0.5) -- (3,0.5) -- (2,2)
    (1.5,0.5) -- (1.5,0)
    ;
\node[below] at (1.5,0) {$z$};
\end{scope}
~~~~
===========

On a alors $\varphi(t) = \varphi(t') + f(z)$.
```

!preuve
```
Seule les termes portant sur $x, y$ et $z$ sont influencés par le changement et
on a :
$$
\begin{array}{rl}
\varphi(t) & = \varphi(t') + f(x) p(t,x) + f(y) p(t,y) - f(z) p(t',z) \\
& = \varphi(t') + f(z) (p(t',z) + 1) - f(z) p(t',z) \\
& = \varphi(t') + f(z)
\end{array}
$$
```

!theoreme
```
L'algorithme de Huffman renvoie un arbre optimal.
```

!preuve
```
Par récurrence sur $|\Sigma|$.

*Initialisation* : si $\Sigma$ ne contient qu'une lettre, il n'y a qu'un arbre qui est
nécessairement optimal.

*Hérédité* : si la propriété est vraie pour un alphabet de $n-1 \ge 1$ lettres,
alors soit $\Sigma$ contenant $n$ lettres et $x$ et $y$ les deux lettres les moins fréquentes.

On pose $\Sigma'$ obtenue en remplaçant $x$ et $y$ par une nouvelle lettre $z$
et on suppose que $f(z) = f(x) + f(y)$. L'hypothèse de récurrence assure qu'on
obtient un arbre optimal $t'$ en appliquant l'algorithme d'Huffman sur $\Sigma'$.
Comme la première étape d'Huffman va fusionner les feuilles $x$ et $y$, on sait
que l'arbre $t$ obtenu en partant de $\Sigma$ se déduit de $t'$  en remplaçant
$z$ par $Noeud(x,y)$. Le théorème précédent assure alors que $\varphi(t) =
\varphi(t') + f(z)$.

Soit $t_o$ un arbre optimal pour $\Sigma$ dans lequel $x$ et $y$ sont soeurs,
possible en vertu du premier théorème, et soit $t_o'$ l'arbre obtenue en
remplaçant dans $t_o$ le noeud liant $x$ et $y$ par une feuille étiquettée par
$z$. On a ici encore $\varphi(t_o) = \varphi(t_o') + f(z) \ge \varphi(t') +
f(z) \ge \varphi(t)$ car $t'$ est optimal.

Ainsi, on a bien l'égalité $\varphi(t_o) = \varphi(t)$ et $t$ est optimal.
```

!subsection(Preuve d'optimalité)

Dans le paragraphe précédent, on retrouve un schéma de preuve classique pour
les preuves d'optimalité des algorithmes gloutons :

* Montrer qu'à partir d'une solution optimale, il est possible de déterminer
  une solution optimale ayant fait le même choix que l'algorithme glouton. Pour
  Huffman c'était le fait d'avoir un arbre optimal ayant les deux lettres les
  moins fréquentes comme sœurs à profondeur maximale.
* Montrer qu'une solution optimale se comportant comme le résultat de
  l'algorithme glouton à une étape
  ne peut être meilleure que le résultat de l'algorithme glouton.

!subsection(Sélection d'activités)

!subsubsection(Description)
Étant donné un ensemble d'activités données par leur temps de début et leur
temps de fin (on considère les temps comme des entiers pour simplifier), on se
pose la question du nombre maximal d'activité que l'on puisse sélectionner sans
que deux activités soient en conflits. Cela correspond par exemple à
l'organisation du planning d'un employé.

On dit que deux activités $(d_1,f_1)$ et $(d_2,f_2)$ sont en conflits quand
$[d_1,f_1[ \cap [d_2,f_2[ \neq \emptyset$.

!twocolumns
~~~~~~
!tikz(activites_conflits)
```
\draw (1,2) --node[above] {$t_1$} (3,2)
    (3,3) --node[above] {$t_2$} (5,3)
    (2,4) --node[above] {$t_3$} (6,4)
    ;
\draw[thin,dotted] (1,2) -- (1,1) (3,3) -- (3,1) (2,4) -- ( 2,1) (5,3) -- (5,1) (6,4) --
(6,1);

\draw[->] (0,1) -- (7,1);
\node[below] at (1,1) {$1$};
\node[below] at (2,1) {$2$};
\node[below] at (3,1) {$3$};
\node[below] at (4,1) {$4$};
\node[below] at (5,1) {$5$};
\node[below] at (6,1) {$6$};
```
~~~~~~
~~~~~~
Ici, $t_1$ et $t_2$ sont en conflits avec $t_3$. Mais $t_1$ et $t_2$ ne sont
pas en conflit. On considère que deux activités peuvent se succéder
directement : $f_1 = d_2$.
~~~~~~

On considère donc en entrée de ce problème une suite finie $( (d_1,f_1), \dots,
(d_n,f_n) )$ et on cherche un sous-ensemble $I \subset \range{1}{n}$ de plus
grand cardinal tel que pour tous $i, j \in I$, si $i \neq j$ alors $(d_i,f_i)$
et $(d_j,f_j)$ ne sont pas en conflits. On dit que $I$ est un 
**ensemble indépendant**.

!subsubsection(Algorithme glouton et implémentation)
Pour résoudre ce problème, on considère l'algorithme glouton associé à la
fonction d'objectif cardinal et **en triant les activités** ordre croissant de
temps de fin.

Cet algorithme est implémenté dans le programme suivant :

!listing(c)
```
!rawinclude(../../snippets/algorithmique/tasksel.c)
```

Ce programme produit alors la sortie :

!listing(c)
```
Activité 0 (1,3) : 1
Activité 1 (3,4) : 1
Activité 2 (2,5) : 0
Activité 6 (0,7) : 0
Activité 3 (5,9) : 1
Activité 5 (8,10) : 0
Activité 4 (11,12) : 1
```

!remarque(Comme l'algorithme commence par effectuer un tri, on a rajouté dans
la structure `activite` un champ permettant d'identifier une activité autrement
que par son indice.)

!subsubsection(Preuve d'optimalité)
On va prouver que l'algorithme glouton renvoie un ensemble indépendant optimal.
Le fait que l'ensemble soit indépendant étant direct, on se concentre sur la
preuve d'optimalité en présentant un schéma de preuve qui correspond à celui
identifié dans le paragraphe précédent.

!comment(Preuve adaptée de https://web.cs.ucdavis.edu/~bai/ECS122A/Notes/ActivitySelect.pdf)

!theoreme(Si $a_1,\dots,a_n$ sont des activités énumérées dans l'ordre
croissant de leur temps de fin, alors il existe un ensemble indépendant optimal
contenant $a_1$.)

!remarque(Cela signifie qu'il fait le même choix que l'algorithme glouton à la
première étape.)

!preuve
```
Soit $I$ un ensemble indépendant optimal ne contenant pas $a_1 = (d_1,f_1)$ (sinon c'est
direct).  Si $a_k = (d_k,f_k)$ est l'activité de plus petit indice dans $I$, alors
$f_k \ge f_1$ donc pour tout $a_i = (d_i,f_i)$ dans $I' = I \backslash \{ a_k \}$
on a $d_i \ge f_k \ge f_1$ et ainsi $a_1$ et $a_i$ ne sont pas en conflit.
Ainsi $I' \cup \{ a_1\}$ est un ensemble indépendant contenant $a_1$ 
de même cardinal que $I$ donc optimal.
```

!theoreme
```
Soit  $A = \{ a_1,\dots,a_n \}$ des activités ordonnées par ordre croissant de temps de
fin et 
$I$ un ensemble indépendant optimal contenant $a_1 = (d_1,f_1)$ (ce qui est possible
selon le théorème précédent).

$I' = I \backslash \{a_1\}$ est optimal pour $A' = \enscomp{ (d,f) \in A}{d \ge f_1}$.
```
!preuve
```
Si, par l'absurde, $I'$ est pas optimal pour $A'$ alors $J \subset A'$ est
un ensemble indépendant de cardinal strictement plus grand que celui de $I'$.
Or, $A' \cup \{ a_1 \}$ est indépendant pour l'ensemble des activités et est de
cardinal strictement plus grand que $I$. Contradiction.
```

!theoreme(L'algorithme glouton renvoie un ensemble indépendant optimal.)

!preuve
```
Par récurrence forte sur le nombre d'activités.

* Initialisation : Pour une activité $a_1$, le glouton renvoie $\{ a_1 \}$ qui est
  directement optimal.
* Hérédité : Si la propriété est vérifiée pour $k \le n$ activités, soit $A =
  \{ a_1,
  \dots, a_{n}\}$ des activités ordonnées par temps de fin. Soit $I$ un
  ensemble indépendant optimal  contenant $a_1$ et $I' = I \cap \{ a_1 \}$. Le
  théorème précédent assure que $I'$ est optimal sur $A' = \enscomp{ (d,f) \in  }{d \ge f_1}$.

  Par hypothèse de récurrence, l'algorithme glouton sur $A'$ produit un
  ensemble indépendant optimal $G'$, donc tel que $|G'| = |I'|$. Par
  construction l'algorithme glouton sur $A$ renvoie $G = G' \cup \{ a_1\}$ de
  même cardinal que $I$, donc optimal.
```


!subsection(Ordonnancement de tâches)

!subsubsection(Description)

On considère ici un problème voisin du problème précédent. On considère $n$
tâches $T = \{ t_1, \dots, t_n \}$ prenant une unité de temps pour être traitées sur une unité de calcul.

Chaque tâche $t$ dispose d'une date limite $f(t) \in \range{1}{n}$ (**deadline**) à laquelle elle 
doit être traitée sans quoi on écope d'une pénalité $p(t) \in \N$.

On appelle stratégie d'ordonnancement une application $d : T \rightarrow \range{0}{n-1}$ qui associe à chaque 
tache un unique temps de début $d(t)$. Selon cette stratégie, on déduit une
séparation de $T$ en deux ensembles disjoints :

* $T^+(d)$ l'ensemble des tâches traitées dans les délais : $t \in T^+(d) \iff
  d(t) < f(t)$.
* $T^-(d)$ l'ensemble des tâches traitées en retard : $t \in T^-(d) \iff d(t) \ge f(t)$.

On note alors $P(d) = \sum_{t \in T^-(d)} p(t)$
la somme des pénalités des tâches en retard.

!exemple
```
On considère l'ensemble de tâches :

  $t_i$   | 1 | 2 | 3 | 4 | 5 | 6 | 7 
----------|---|---|---|---|---|---|---
 $f(t_i)$ | 1 | 2 | 3 | 4 | 4 | 4 | 6 
 $p(t_i)$ | 3 | 6 | 4 | 2 | 5 | 7 | 1 

Une stratégie d'ordonnancement (les tâches en retard sont en gras)
est donnée dans le tableau suivant :

$t_i$        | 1     | 2 | 3 | 4     | 5 | 6 | 7
-------------+-------+---+---+-------+---+---+---
$d(t_i)$     | **6** | 0 | 1 | **4** | 3 | 2 | 5

On a alors $P(d) = 5$.
```

On cherche à obtenir une stratégie d'ordonnancement de valeur $P(d)$ minimale.

On remarque que l'ordonnancement des tâches en retard n'a aucune importance, 
et on peut donc se contenter de déterminer une stratégie
d'ordonnancement pour les tâches traitées dans les délais et la compléter par
n'importe quel ordonnancement des autres tâches. On peut ainsi reformuler le
problème : déterminer un sous-ensemble $T^+ \subset T$ de tâches **pouvant**
être traitées dans les délais tel que $\sum_{t \in T^+} p(t)$ soit
**maximale**.

!subsubsection(Algorithme glouton et implémentation)

On résout maintenant ce problème de maximisation des pénalités $T^+$ par un 
algorithme glouton :

* On commence avec $T^+ = \emptyset$ et tous les temps de $\range{0}{n-1}$ sont
  marqués comme étant disponibles.
* On parcourt les tâches dans l'ordre décroissant des pénalités.
    * Quand on considère la tâche $t$ s'il existe un temps $i$ disponible tel que 
      $i < d(t)$ alors on marque comme indisponible le temps $i_0 = \max
      \enscomp{i \in \range{0}{n-1}}{i < d(t) \text{ et } i \text {
      disponible} }$ et on rajoute alors
      $t$ à $T^+$ en commençant $t$ au temps $i_0$.
* On place les tâches restantes aux temps disponibles.

Pour les structures de données, on utilise une représentation en tableaux de
booléens (des `unsigned char` à `0` ou `1` en C) pour la
disponibilités des temps. L'ensemble $T^+$ est alors implicite car il
correspond aux tâches ordonnancé dans la première étape.
Utiliser un tableau implique qu'une recherche linéaire soit faite
pour chercher un plus grand temps disponible, et donc, la complexité temporelle
globale sera en $O(n^2)$.

!remarque(Il est possible d'améliorer cela pour passer en $O(n \log_2 n)$ (*exercice*).)

Le programme C suivant implémente cet algorithme.

!listing(c)
```
!include(../../snippets/algorithmique/scheduler.c)
```

Il produit la sortie :
!listing(c)
```
T6 (f:4,p:7) @ 3
T2 (f:2,p:6) @ 1
T5 (f:4,p:5) @ 2
T3 (f:3,p:4) @ 0
T1 (f:1,p:3) @ 4
T4 (f:4,p:2) @ 6
T7 (f:6,p:1) @ 5
```

Ce qui correspond à l'ordonnancement $t_3, t_2, t_5, t_6, t_1, t_7, t_4$. Les
tâches $t_1$ et $t_4$ sont en retard, donc la pénalité totale est de 5.

!subsubsection(Preuve d'optimalité)

On va montrer que cet algorithme glouton renvoie un ensemble $T^+$ optimal.
Pour cela, on procède comme précédemment. Tout d'abord, on montre qu'il existe
une solution optimale qui effectue le premier choix de l'algorithme glouton.

!theoreme(Soit $T$ un ensemble de tâches et $t \in T$ une tâche de 
pénalité maximale. Il existe un ensemble $T^+$ de tâches pouvant être traitées
dans les délais, maximal pour les pénalités et tel que $t \in T^+$.)

!preuve
```
Soit $T^+ \subset T$ un ensemble maximal. S'il contient $t$, il convient
directement. Sinon, il existe une tâche $t'$ de $T^+$ qui est traitée à un
moment où on pourrait traiter $t$ à temps (sinon $T^+ \cup \{t\}$ conviendrait
et $T^+$ ne pourrait être maximal). On a $p(t') \le p(t)$ par maximalité de
$t$. L'ensemble $T'$ déduit de $T^+$ en remplaçant $t'$ par $t$ convient car on
a forcément $P(T') = P(T^+)$ (en fait $\ge$ mais $=$ par optimalité de $T^+$)
et par construction toutes ses tâches peuvent être traitées à temps.
```

On montre maintenant qu'en enlevant le choix glouton, on obtient une solution
optimale du sous-problème.

!theoreme
```
Soit $T^+ \subset T$ ensemble de tâches pouvant être traitées, maximal pour les pénalités et 
contenant une tâche $t$ de plus grande pénalité. Soit $i$ l'instant auquel la
tâche $t$ commence dans un ordonnancement de $T^+$.

On pose $T' = T \backslash \{ t \}$ avec des dates limites modifiées : 
$$\forall t' \in T', d_{T'}(t') = 
\begin{cases}
d_T(t') & \text{si } d_T(t') \le i \\
d_T(t')-1 & \text{sinon} 
\end{cases}$$

$T^+ \backslash \{t\}$ est alors maximal pour $T'$.
```

!preuve
```
Dans $T'$, on a à la fois enlevé $t$ et supprimé l'instant $i$. Tout
ordonnancement de $T'$ peut alors être *relevé* en un ordonnancement de $T$ en
décalant d'un instant les tâches commençant à partir de l'instant $i$ et en
ordonnançant là la tâche $t$. Réciproquement d'un ordonnancement dans $T$, on
déduit directement un ordonnancement de $T'$

Ainsi, s'il existait $T'^+$ maximal pour $T'$ tel que $P(T'^+) > P(T^+
\backslash \{ t\}) = P(T^+) - p(t)$ alors $T'^+ \cup \{ t \}$ serait de somme
de pénalités strictement plus grande que celle de $T^+$ supposé maximal. 

Donc, $T^+ \backslash \{ t\}$ est maximal.
```

On conclut alors directement par récurrence sur le nombre de tâches comme on
l'a fait précédemment pour la sélection d'activités :

!theoreme
```
L'algorithme glouton renvoie un ordonnancement optimal.
```

!section(Diviser pour régner)
!subsection(Principe)

Le principe des algorithmes dits *Diviser pour régner* est de décomposer un
problème en plusieurs sous-problèmes disjoints et de déduire des solutions de
ces sous-problème une solution au problème de départ.

Le point clé pour ce principe est de pouvoir **fusionner** les solutions de
sous-problèmes pour en faire une solution, et de pouvoir le faire dans un
temps/espace raisonnable. On procède alors par récursivité en appliquant ce
principe pour résoudre les sous-problèmes eux-mêmes jusqu'à tomber sur des
sous-problèmes très simples.

!subsection(Tri fusion)

L'algorithme du tri fusion est un des exemples les plus important d'algorithmes
*Diviser pour régner* : 

* Étant donnée une liste $l$ de taille $n \ge 2$, on va
  considérer les sous-listes $l_p$ des valeurs d'indice pair et $l_i$ des valeurs d'indice
  impair.
* On trie ensuite $l_1$ et $l_2$ pour obtenir $l'_1$ et $l'_2$.
* On fusionne ces deux listes pour obtenir $l' = \text{fusion}(l'_1,l'_2)$
  liste triée déduite de $l$.

Comme expliqué dans le paragraphe précédent, les tris de $l_1$ et $l_2$
s'effectuent eux-aussi à l'aide d'un tri fusion.

!note(TODO : dessin)

Voici une implémentation en `OCaml` de cet algorithme :

!listing(ocaml)
```
!include(../../snippets/algorithmique/trifusion.ml)
```

La correction et la terminaison de cet algorithme ne posant aucune difficulté, on
va se concentrer sur le calcul de la complexité temporelle :

* `separe_en_deux` consiste en un parcours linéaire de la liste `l` donc
  $O(|l|)$.
* `fusionne` supprime un élément d'une des deux listes à chaque appel récursif,
  donc une complexité en $O(|l_1|+|l_2|)$.
* Pour `tri_fusion` la situation est plus complexe en raison du double appel
  récursif. On va d'abord traiter le cas des listes contenant $2^k$ éléments.

Notons $t_n$ la complexité temporelle pour $|l|=n$. 


!lemme($t_{2^n} = O(2^n \log_2 2^n)$)

!preuve
```
Par l'analyse de complexité des deux fonctions auxiliaires, on a pour $n \in \N$
$$t_{2^{n+1}} = 2 t_{2^n} + O(2^n) \le 2 t_{2^n} + M 2^n$$
où on peut supposer que $M \ge 1$.

On va montrer par récurence sur $n \in \N^*$ que $t_{2^n} \le 2 M n 2^n$.

* Initialisation : $t_{2^1} = 2 t_1 + M 2 = 2 M + 2 \le 4 M = 2 \times 1 \times 2^1 M$.
* Hérédité : si $n \in \N^*$ et l'hypothèse est vérifiée pour $t_{2^n}$,
  alors $t_{2^{n+1}} \le 4 n 2^n M + M 2^n = (4n +1) M 2^n \le 4 (n+1) M 2^n 
  \le 2 M (n+1) 2^{n+1}$.

Ainsi $t_{2^n} = O(n 2^n)$.
```

!theoreme($t_n = O(n \log_2 n)$)

!preuve
```
Le lemme assure qu'il existe $M'$ tel que $\forall p \in \N^*, t_{2^p} \le M' p
2^p$.

Soit $n\in \N^*$ et $p$ minimum tel que $n \le 2^p$. On a $\log_2 n \le p$ par croissance de
$\log_2$ et ainsi $t_n \le t_{2^p} \le M' p 2^p = M' n \log_2 n$.

Ainsi, $t_n = O(n \log_2 n)$.
```

!remarque(On a utilisé implicitement la croissance de $t_n$ ici : plus la liste
est longue, plus on effectue d'opérations.)

Le programme suivant présente une implémentation du tri fusion reposant sur
des tableaux. Les sous-tableaux sont manipulés à l'aide de leurs indices de
début et de fin comme pour la recherche dichotomique.

!listing(ocaml)
```
!include(../../snippets/algorithmique/trifusion.ml)
```

!note(TODO : exercice tri avec un tableau et tri **en place**)

!subsection(Nombre d'inversions)

!definition
```
Soit $t$ une structure séquentielle (tableau, liste, ....) 
contenant des valeurs comparables $a_0, ..., a_{n-1}$ et énumérées dans
cet ordre au sein de $t$.

Une paire $(i,j) \in \range{0}{n-1}^2$ où $i < j$ est appelée une *inversion*
de $t$ lorsque $a_i > a_j$.

On note $I(t)$ le nombre d'inversion de $t$.
```

!remarque
```
* Le nombre d'inversions permet de mesurer à quel point $t$ est non triée dans
  l'ordre croissante.
* Ce concept d'inversion est exactement celui utilisé pour les
  permutations en mathématiques : si $\sigma \in \mathfrak{S}_n$, il suffit de
  considérer $(\sigma(1),\dots, \sigma(n))$.
```

On cherche dans ce paragraphe à calculer $I(t)$ efficacement. Remarquons tout
d'abord qu'un algorithme naïf est en $O(n^2)$ où $|t|=n$ en explorant toutes
les paires :

!listing(c)
```
size_t inversions(int *t, size_t taille)
{
    size_t inv = 0;
    for (size_t i = 0; i < taille; i++)
    {
        for (size_t j = i+1; j < taille; j++)
        {
            if (t[i] > t[j]) inv++;
        }
    }

    return inv;
}
```

On va maintenant donner un algorithme type *Diviser pour régner* :

* On sépare $t$ en deux moitiés $t_1$ et $t_2$.
* On calcule $I(t_1)$ et $I(t_2)$ par des appels récursifs.
* On compte les inversions entre des éléments de $t_1$ et des éléments de
  $t_2$
  * Cela ne dépend pas de leur position dans $t_1$ ou dans $t_2$.
  * On peut donc trier $t_1$ en $t'_1$ et $t_2$ en $t'_2$.
  * On compte $N(t_1,t_2) = N(t'_1,t'_2)$ le nombre d'inversions entre $t'_1$
    et $t'_2$.
* On en déduit que $I(t) = I(t_1) + I(t_2) + N(t_1,t_2)$.

Pour calculer le nombre d'inversions entre deux tableaux triés $t'_1$ et $t'_2$
on peut utiliser l'algorithme en $O(|t'_1| + |t'_2|)$ suivant : pour $j$ parcourant les indices de $t'_2$, on cherche 
le plus petit $i$ tel que $t'_1[i]$.

!subsection(Points les plus proches)
!subsection(Sous-ensemble de somme donnée)
!subsection(Recherche dichotomique)
!subsection(Couverture par des segments égaux)

!section(Programmation dynamique)
!subsection(Principe)
!subsection(Somme de sous-ensembles)
!subsection(Ordonnancement de tâches)
!subsection(Plus longue sous-suite commune)
!subsection(Distance d'édition)

