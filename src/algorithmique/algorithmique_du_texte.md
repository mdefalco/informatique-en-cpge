!chapterimage(chap_algo_texte.jpg)(Algorithmique des textes)

*Source image : justgrims, https://www.flickr.com/photos/notbrucelee/8016192302 *

!define[0](textmatrix)
~~~
\matrix[matrix of nodes,nodes in empty cells,nodes={anchor=center,align=center,text width=.5cm,minimum height=.5cm},row 1/.style={nodes={text=white,fill=black}},row 2/.style={nodes={fill=gray!20}}]
~~~

Sources

* *Algorithms* Robert Sedgewick, Kevin Wayne
* *Éléments d'algorithmique* D. Beauquier, J. Berstel, Ph. Chrétienne
* *125 Problems in Text Algorithms with Solutions* Maxime Crochemore, Thierry
  Lecroq, Wojciech Rytter

!section(Recherche dans un texte)
!subsection(Principe de la recherche)
On s'intéresse ici au problème suivant :

!probleme(RechercheTexte)
```
* une chaîne de caractère $s$ sur l'alphabet $\Sigma$
* un autre chaîne de caractère $m$ sur ce même alphabet appelé *motif* et de
  longueur plus petite que $s$
```
```
un résultat partiel correspondant à l'indice de la première occurrence du 
motif dans la chaîne s'il est présent.
```

La différence fondamentale entre ce problème et celui de la recherche d'un
sous-tableau dans un tableau est le fait qu'on considère un alphabet fini et
dont le nombre d'éléments est le plus souvent négligeable par rapport à la
taille des chaînes de caractères. Cela permet d'effectuer des optimisations qui
ne sont pas sans rappeler les tris linéaires comme le tri par comptage.

On parle alors d'algorithmique du texte pour désigner des algorithmes tirant
partie de cette contrainte sur les données. La plupart des algorithmes que l'on
présente peuvent ainsi s'adapter aisément au cas de tableaux dont les éléments
sont pris dans un ensemble fini de petit cardinal.

Avant d'entamer ce chapitre, remarquons qu'il existe, outre l'alphabet usuel,
trois alphabets très importants :

* celui des caractères ASCII usuels
* celui contenant les deux éléments 0 et 1, ce qui permet de travailler sur des
  recherche en binaire.
* et enfin, très important pour la biologie, l'alphabet à quatre lettres A, T, G
  et C correspondant aux bases d'un brin d'ADN et qui ouvre la porte à
  beaucoup d'applications en bio-informatique.

!note(Il y aura sûrement des applications bio-info dans la partie programmation
dynamique, faire le lien ici.)

!subsection(Algorithme naïf en force brute)

Une solution naïve consiste à parcourir chaque position de $s$ afin de tester
si le motif est présent à partir de cette position.

!tikz(texte_rech_naive)
```
!textmatrix
(m) {
    0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
    t & o & t & a & t & o & t & o & u \\
    |[green!50!black]| t &|[green!50!black]|  o & |[green!50!black]| t & |[red]| o \\
    & |[red]| t &  o & t &  o \\
    & & |[green!50!black]| t &  |[red]| o & t &  o \\
    & & & |[red]| t &  o & t &  o \\
    & & & & |[green!50!black]| t & |[green!50!black]| o & |[green!50!black]| t & |[green!50!black]|  o \\
};

\draw (m-2-1.north west) rectangle (m-2-9.south east);
\node[left] at (m-1-1.west) {\footnotesize indices};
\node[left] at (m-2-1.west) {$s$};
\node[left] at (m-3-1.west) {\footnotesize recherche à l'indice 0};
\node[left] at (m-4-1.west) {\footnotesize à l'indice 1};
\node[left] at (m-5-1.west) {\footnotesize à l'indice 2};
\node[left] at (m-6-1.west) {\footnotesize à l'indice 3};
\node[left] at (m-7-1.west) {\footnotesize à l'indice 4};
\draw[green,rounded corners] (m-7-5.north west) rectangle node[below=.1cm,black] {\footnotesize motif trouvé à l'indice 4} (m-7-8.south east);
```

Cela donne l'implémentation assez directe suivante :

!listing3(ocaml)(c)(python)
```
!rawinclude(../../snippets/algorithmique/texte_rech_naive.ml)
```
```
!rawinclude(../../snippets/algorithmique/texte_rech_naive.c)
```
```
!rawinclude(../../snippets/algorithmique/texte_rech_naive.py)
```

La complexité temporelle en pire cas de cet algorithme correspond au maximum de
comparaisons. On peut naturellement en déduire par majoration une borne en
$O(np)$ mais on peut remarquer qu'il est assez difficile d'obtenir un exemple
concret, ce qui fait penser que ce pire cas est *rare*.

!remarque
```
Considérons la chaîne $s = a a ... a = a^n$ qui contient $n$ fois la lettre $a$ et le
motif $m = a^{p-1} b$ qui contient $p-1$ a et finit par un $b$. Dans
l'algorithme, on va donc à chaque étape de la première boucle effectuer $p$
itérations dans la seconde avant de se rendre compte que le motif n'est pas
présent en comparant $b$ et $a$. On a donc exactement $(n-p+1)p = \Theta(np)$ 
comparaisons et on retombe ainsi sur la complexité $O(np)$ pour ces
exemples.
```

Ce qui va se passer dans une application usuelle de cet algorithme, c'est qu'au
bout d'une ou deux comparaisons, on pourra invalider la position et passer à la
suivante. On va alors avoir une complexité en $O(n+p)$ en considérant en plus
la validation du motif dans le cas où il est présent. Ici $p \le n$ donc
$O(n+p) = O(n)$ mais c'est important de garder en tête cette complexité en
$O(n+p)$ qu'on retrouvera car elle s'appliquera à des algorithmes où on
effectue un prétraitement sur le motif pour l'appliquer ensuite sur plusieurs
chaînes.

!comment
~~~~~~~~
!remarque
```
Pour aller plus loin par rapport à la remarque précédente, on peut considérer
une distribution probabiliste uniforme sur les chaînes de caractères de
longueur $n$ sur un alphabet $\Sigma$ de taille $N$. Cela correspond à
considérer une chaîne aléatoire comme la concaténée $X_0 ... X_{n-1}$ des $n$ variables
aléatoires indépendantes $X_i$ de loi uniforme sur $\Sigma$. On considère
également les variables aléatoires $T_i$ qui indiquent le nombre d'itérations
pour la recherche de $m$ à partir de l'indice $i$. L'hypothèse d'indépendance
assure que toutes ces variables ont même loi.

Si on considère $m = a_0 ... a_{p-1}$ on a pour $0 \le i \le n - p$ et 
$1 \le j \le p-1$ :
$P(T_0 = j)
= P(X_0 = a_0, \dots,  X_{0+j-2} = a_{j-2}, X_{0+j-1} \neq a_{j-1})$
$= P(X_0 = a_0) \dots P(X_{0+j-2} = a_{j-2}) P(X_{0+j-1} \neq a_{j-1}) =  \frac{N-1}{N^{j}}$

Dans le cas où $j = p$, on peut ignorer la dernière comparaison, on obtient
donc 
$P(T_0 = p) = \frac{1}{N^{p-1}}$

Le nombre moyen d'itérations à l'indice $0$ est donc
$$
E(T_0) = \sum_{j=1}^{p-1} j \frac{N-1}{N^j} + \frac{1}{N^{p-1}}
$$
```
~~~~~~~~

!subsection(Algorithme de Boyer-Moore)

Dans un premier temps, on va présenter la variante usuelle de cet algorithme
appelée algorithme de Boyer-Moore-Horspool. On présentera ensuite l'algorithme
de Boyer-Moore en tant que tel.

!subsubsection(Principe de Boyer-Moore-Horspool)
Le principe de l'algorithme de Boyer-Moore-Horspool est d'effectuer une recherche du
motif comme précédemment mais en partant de la fin. On va alors tenter de
trouver des suffixes de plus en plus grand du motif. Si on trouve ainsi le
motif, on renvoie la position. Sinon, c'est qu'on a lu dans $s$ un mot de la
forme $x m'$ où $m'$ est un suffixe strict de $m$ mais $x m'$ n'en est pas un.
Si $x$ n'est pas présent dans $m$, alors on peut relancer la recherche juste
après $x$ dans $s$. Si $x$ est présent dans $m$, on peut relancer la
recherche en alignant ce caractère avec sa position la plus à droite dans $m$.

!remarque(Il faut tenir compte différemment du dernier caractère du motif, car
il n'est pas utile de le réaligner. On considère alors, quand elle existe,
l'occurrence précédente de ce caractère.)

On obtient ainsi une stratégie de saut qui en cas d'échec relance la recherche plus loin.

Voici un premier exemple où on effectue une recherche de $abaa$ dans le mot
$aabababbaa$. Cette stratégie a permis d'éviter une recherche inutile à partir
de l'indice 1.

!tikz(texte_bm_exemple)
```
!textmatrix
(m) {
    0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10\\
    a & a & a & b & a & b & a & b & b & a & a \\
    a & b & a & |[red]| a  \\
    & & a & b & a & |[red]| a  \\
    & & & & a & b & a & |[red]| a  \\
    & & & & & &  a & b & |[red]| a & |[green!50!black]| a  \\
    & & & & & & & |[red]| a & |[green!50!black]| b & |[green!50!black]| a & |[green!50!black]| a  \\
};

\draw[rounded corners] (m-3-2.north west) rectangle (m-3-2.south east);
\draw[rounded corners] 
    (m-4-4.north west) rectangle (m-4-4.south east)
    (m-5-6.north west) rectangle (m-5-6.south east)
    (m-6-8.north west) rectangle (m-6-8.south east)
    (m-7-9.north west) rectangle (m-7-9.south east)
    ;
\node[left] at (m-1-1.west) {\footnotesize indices};
\node[left] at (m-2-1.west) {$s$};
\draw[dashed,thin] 
    (m-3-2.south east) edge[->] (m-4-4.north west)
    (m-4-4.south east) edge[->] (m-5-6.north west)
    (m-5-6.south east) edge[->] (m-6-8.north west)
    (m-6-8.south east) edge[->] (m-7-9.north west)
    ;
```

!subsubsection(Implémentation par table de saut)

Pour réaliser ces sauts, on construit une table `droite` indexée par $\Sigma$
et telle que `droite[c]` indique l'indice de l'occurrence la plus à droite dans
le motif $m$ du caractère $c$, en ignorant le dernier caractère du motif.

Ainsi, dans l'exemple précédent du motif `abaa`, on obtient la table suivante :

`c`        |'a'|'b'|'c'         | ...
-----------+---+---+------------+------
`droite[c]`| 2 | 1 | $\emptyset$| ...

On a indiqué ici $\emptyset$ quand un caractère de $\Sigma$ n'est pas présent
dans le motif, car il peut être présent dans $s$.

Cette table contient donc de l'ordre de $\Sigma$ éléments. On peut la réaliser
par un tableau direct de taille $|\Sigma|$ étant donné un ordre d'énumération.
On peut aussi la réaliser par un dictionnaire, ce qui est plus économe en
espace si le motif contient peu de lettres différentes. On a choisit ici, pour
des raisons pédagogiques, de considérer la numérotation ASCII naturelle associées 
au caractère de cette table.

!listing3l(DROITE)(../../snippets/algorithmique/texte_boyer_moore)

Afin d'implémenter l'algorithme lui-même, il est nécessaire de faire des
calculs élémentaires mais précis pour déterminer le saut à effectuer. Si à la
position $i+j$ on a un échec après avoir lu le caractère `c` où `droite[c]`
contient la valeur $k$.

* Si $k = \emptyset$, c'est que le motif ne pourra jamais être trouvé tant que
  ce caractère `c` sera présent. 
  On relance donc la recherche juste après à l'indice $i+j+1$.

!tikz(texte_bm_saut_cas_vide)
```
!textmatrix
(m) {
    0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
    a & b & b & a & a & d & a & c & a\\
    d & a & |[red]| c \\
    & & & d & a & |[red]| c \\
};
```
* Si $k \ge j$, cela signifie que `c` est présent plus à droite dans le motif,
  donc aligner cette occurrence ne permettrait pas d'avancer la recherche. Rien
  ne nous permet de savoir si `c` est présent ou non ailleurs dans le motif, on
  relance alors prudemment la recherche en $i+1$.
!tikz(texte_bm_saut_cas_pres)
```
!textmatrix
(m) {
    0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
    a & c & a & c & a & d & a & c & a\\
    c & |[red]| a & |[green!50!black]| c \\
    &|[green!50!black]|  c & |[green!50!black]| a & |[green!50!black]| c \\
};
```
* Sinon, on veut aligner ce `c` avec le caractère correspondant du motif,
  si on relance à l'indice $i'$, on souhaite ainsi avoir $i' + k = i + j$
  donc $i ' = i + j - k$.

!listing3l(HORSPOOL)(../../snippets/algorithmique/texte_boyer_moore)

!paragraph(Correction)

Tout d'abord, remarquons que la terminaison ne pose pas de questions dans la
mesure où on le nouvel indice auquel on relance la recherche est toujours
strictement plus grand que le précédent.

Au sujet de la correction, il suffit de s'assurer que les indices écartés
correspondent nécessairement à des recherches infructueuses. Sans perte de
généralité, on peut supposer que la recherche s'effectue depuis le premier
indice de $s$. Comme seul les sauts d'au moins deux indices sont ceux pour
lesquels il est nécessaire de faire une preuve, cela correspond au
cas où $m = m_1 c m_2 d m_3 x$ et $s = s_1 c m_3 s'$ avec $c,d$ et $x$ des
caractères, $d \neq c$ et $c$ non présent dans $m_2 d m_3$.

Ainsi, toute recherche démarrant à des indices inférieurs échouera
systématiquement, au plus tard, en comparant le caractère $c$ de $c m_3$ avec
un caractère du motif dans $m_2 d m_3$ donc différent de $c$.

!paragraph(Complexité)

Tout d'abord, on remarque que la table de saut se construit en
$O(\max(|m|,|\Sigma|))$ pour un motif $m$ sur un alphabet $\Sigma$.

Sans chercher à rentrer dans les détails, on peut raisonnablement penser **si
l'alphabet contient assez de caractères** que
les motifs auront peu de répétitions et qu'ainsi, les sauts seront presque
toujours maximaux, ce qui permet d'obtenir de l'ordre de $\frac{n}{p}$
comparaisons où $n$ est la longueur de la chaîne et $p$ la longueur du motif.


Cependant, en pire cas, cet algorithme n'est pas meilleur que le précédent.
Pour s'en convaincre, on va considérer un exemple proche de celui introduit
poru l'algorithme naïf. Si on cherche $b a^{p-1}$ dans $a^n$ à l'indice $i$, 
il est nécessaire d'attendre de comparer au caractère $b$ pour constater un échec 
et devoir relancer l'algorithme à l'indice $i+1$. On va donc faire ici aussi
$(n-p+1)p = \Theta(np)$ comparaisons.

La complexite temporelle en pire cas de Boyer-Moore-Horspool est donc de
$O(np)$, même si, en pratique, elle est sous-linéaire.

!remarque(Si l'alphabet contient peu de caractères, ce qui est le cas en
particulier du binaire, il y a de grandes chances qu'on soit dans ce cas pire
cas. Ainsi, Boyer-Moore-Horspool n'est pas adapté pour ce type de texte.)

!subsubsection(Principe de Boyer-Moore)

Considérons le cas suivant de l'algorithme précédent : on cherche abbcabc
dans cbacbbcabc.

!tikz(texte_bm_complet_a)
```
!textmatrix
(m) {
    0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\
    c & b & a & c & b & b & c & a & b & c \\
    a & b & b & c & |[red]| a & |[green!50!black]| b & |[green!50!black]| c \\
    & a & b & b & c & |[]| a & |[]| b & |[red]| c \\
    & & & |[red]| a & |[green!50!black]| b & |[green!50!black]| b & |[green!50!black]| c & |[green!50!black]| a & |[green!50!black]| b & |[green!50!black]| c \\
};
```

On remarque qu'en raison du fonctionnement de cet algorithme, on est forcé de
faire de tous petits sauts et on est ramené à l'algorithme naïf. Cependant,
après la première étape, on sait qu'on a lu un suffixe du motif `bc` qui est
précédé d'un caractère `a` en sorte que `bbc` ne soit pas un suffixe du motif.

Il y a un autre endroit dans le motif où on peut trouver `*bc` avec `*` un
autre caractère que `a`. On pourrait donc relancer la recherche en alignant
cette occurrence de `bc` avec celle qu'on vient de lire. Cela revient à sauter
directement à la dernière étape dans cet exemple :

!tikz(texte_bm_complet_b)
```
!textmatrix
(m) {
    0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\
    c & b & a & c & b & b & c & a & b & c \\
    a & b & b & c & |[red]| a & |[green!50!black]| b & |[green!50!black]| c \\
    & & & |[red]| a & |[green!50!black]| b & |[green!50!black]| b & |[green!50!black]| c & |[green!50!black]| a & |[green!50!black]| b & |[green!50!black]| c \\
};
```

Pour pouvoir réaliser ce décalage, il est nécessaire de calculer une nouvelle
table en parcourant le motif pour identifier de telles apparitions de suffixes.

On peut aller plus loin en considérant également le plus long préfixe du motif qui
soit un suffixe du suffixe considéré. Par exemple, pour le motif `bcabc` on
remarque que `bc` étant un préfixe, on peut effectuer un saut comme dans
l'exemple suivant :

!tikz(texte_bm_complet_c)
```
!textmatrix
(m) {
    0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
    c & a & a & b & c & c & b & b & c \\
     b & |[red]| c & |[green!50!black]| a & |[green!50!black]| b & |[green!50!black]| c \\
     & & & b & c & a & b & c \\
};
```
!paragraph(Table des bons suffixes)

!note(Tout cela sera redéfini proprement plus tard dans le chapitre sur les
langages. Je laisse cette partie en attendant pour que la présentation soit
complète.)

Il est nécessaire d'introduire des définitions précises pour formaliser la
stratégie qu'on vient de présenter. Dans le contexte des langages, on parle
plus souvent de mot que de chaîne de caractères, qui sont un type de données
permettant de les représenter. Un mot sur l'alphabet $\Sigma$ est donc une 
suite finie $a_1 \dots a_n$ de lettres dans l'alphabet. On note $\mu$ l'unique
mot vide, c'est-à-dire ne contenant aucune lettre. L'ensemble des mots sur
$\Sigma$ est noté $\Sigma^*$.
Si $u$ et $v$ sont des mots,
$uv$ est le mot obtenu par concaténation.

!remarque
~~~
$\Sigma^*$ muni de cette loi de composition a une structure proche de l'ensemble des
entiers naturels $\N$ muni de l'addition :

* on a : $\forall u,v,w \in \Sigma^*, u(vw) = (uv)w = uvw$, on dit que la loi
  est associative ;
* elle possède un élément neutre $\mu$ : $\forall u \in \Sigma^*, \mu u = u \mu = u$.

On dit alors que $\Sigma^*$ est un **monoïde**. Cette structure très simple est
cruciale en informatique.
~~~

!definition
~~~
Soit $u, v \in \Sigma^*$, on dit que $v$ est :

* un **suffixe** de $u$ s'il existe $w \in \Sigma^*$ tel que $u = w v$
* un **préfixe** de $u$ s'il existe $w \in \Sigma^*$ tel que $u = v w$

Lorsque $w \neq \mu$, on parle de suffixe ou de préfixe **propre**.

On dit que $v$ est un **bord** de $u$ lorsque $v$ est suffixe et préfixe propre
de $u$.
~~~

!exemple
~~~
Soit $u = a b a c a b a$. $ab a c$ est un préfixe de $u$, $c a ba$ un suffixe
et $aba$ un bord.
~~~

!definition
~~~
Soit $x = x_1 \dots x_n$ et $u,v$ deux suffixes **distincts** de $x$. On dit que $u$ et $v$
sont des suffixes **disjoints** quand on est dans l'un des cas suivants :

* $u = x$
* $v = x$
* $u \neq x, v \neq x$ et $x_{|x|-|u|} \neq x_{|x|-|v|}$.
~~~

Des suffixes disjoints sont donc des suffixes précédés par des lettres
différentes dans $x$. On définit de même la notion de préfixes disjoints.

On considère un motif $x = x_0 \dots x_{n-1}$ et
on va reprendre, en la précisant, la description précédente. Se faisant, on va
construire une table `bonsuffixe` appelée la table des **bons suffixes** du
motif $x$ et telle que, pour $i \in \range{0}{n-1}$, `bonsuffixe[i]` donne le
nombre de positions dont on doit décaler le motif vers la droite pour relancer
la recherche après la lecture du suffixe $x_{i+1} \dots x_{n-1}$.

Supposons qu'on vient de lire avec succès
un suffixe propre $u$. Ainsi $x = x_0 \dots x_i u$ et on vient de lire dans la
chaîne où on effectue la recherche $a u$ avec $a \neq x_i$.

* Soit il existe un autre suffixe $b u v$ de $x$ où $b \neq x_i$ et alors on 
  appelle bon suffixe pour $u$ un tel suffixe de longueur minimale et on pose
  alors `bonsuffixe[i]`$=|v|$
* Sinon, on cherche $v$ de longueur minimale tel que $x$ soit un suffixe de $u
  v$ et on pose également `bonsuffixe[i]`$=|v|$.

On remarque que si $x$ est suffixe de $u v$ et qu'on a également $b u v'$
suffixe de $x$, alors $|uv| = |u|+|v| \ge |x| \ge |buv'| \ge |u|+|v'|$ donc
$|v| \ge |v'|$ ce qui permet de considérer le plus petit $v$ sur l'ensemble des
cas.

!paragraph(Table des suffixes)

Afin de calculer efficacement `bonsuffixe` on va commencer par calculer la
table des suffixes du motif, il s'agit de la table `suffixe` où `suffixe[i]` contient 
la longueur du plus long suffixe de $x$ de la forme $x_j \dots x_i$. Ainsi,
si on note $S_i$ les suffixes de cette forme, on a :

$$
\texttt{suffixe}[i] = \begin{cases}
    0 & \text{si } S_i = \emptyset \\
    \max \enscomp{|s|}{s \in S_i} & \text{sinon }
    \end{cases}
$$

Nécessairement, `suffixe[n-1]=n` car $x$ convient.

!exemple
~~~
Pour $x = bcabc$ on a :

i           | 0 | 1 | 2 | 3 | 4
------------+---+---+---+---+---
`suffixe[i]`   | 0 | 2 | 0 | 0 | 5

et pour $x = abbabba$ :

i           | 0 | 1 | 2 | 3 | 4 | 5 | 6
------------+---+---+---+---+---+---+---
`suffixe[i]`   | 1 | 0 | 0 | 4 | 0 | 0 | 7
~~~

Il est possible de construire `suffixe` avec un simple parcours linéaire en tirant
partie de l'information déjà calculée. Pour cela, on va remplir `suffixe` de
droite à gauche.

A tout moment, on va conserver le meilleur suffixe rencontré, c'est-à-dire
celui pour lequel on est allé le plus loin à gauche avant d'avoir un échec de
comparaison. On note $s$ la position la plus à droite de ce suffixe et $k$ sa
longueur,
il s'agit donc de $u = x_{s-k+1} ... x_s$ et il y a eu un
échec de comparaison en $x_{s-k}$. 
Par définition de `suffixe` on a `suffixe[s]`$=k$.
Le mot $x$ s'écrit alors : 

!tikz[decoration=brace](texte_bm_complet_suff_ex)
~~~
\matrix[matrix of math nodes,nodes in empty cells,nodes={anchor=center,align=center,text width=.8cm,minimum height=.5cm}] (m)
{
x & = & x_0 & \dots & x_{s-k} & x_{s-k+1}  & \dots & x_{s} & \dots & 
x_{n-1-k} & x_{n-k} & \dots & x_{n-1} \\
};

\draw[rounded corners]
    (m-1-5.south west) rectangle (m-1-5.north east)
    (m-1-10.south west) rectangle (m-1-10.north east)
    ;
\draw (m-1-5.north) -- +(0,.5) -- node[circle,fill=red!20] {$\neq$} ($(m-1-10.north)+(0,.5)$)  -- (m-1-10.north);
\draw[decorate] (m-1-8.south) -- node[below=.2cm] {$=u$} (m-1-6.south west);
\draw[decorate] (m-1-13.south) -- node[below=.2cm] {$=u$} (m-1-11.south west);
~~~

Ce qu'on peut représenter schématiquement ainsi :
!tikz(texte_bm_complet_suff_ex2)
~~~
\draw (0,0) rectangle (2,.5);
\draw[fill=blue!20] (2,0) rectangle node[anchor=center]{$a$} (2.5,.5);
\draw[fill=green!20] (2.5,0) rectangle node[anchor=center]{$u$} (5,.5);
\draw (5,0) rectangle (6,.5);
\draw[fill=red!20] (6,0) rectangle node[anchor=center]{$b$} (6.5,.5);
\draw[fill=green!20] (6.5,0) rectangle node[anchor=center]{$u$} (9,.5);
\node[above left] at (9,.5) {$n-1$};
\node[above] at (6.25,.5) {$n-k-1$};
\node[above left] at (5,.5) {$s$};
\node[above] at (2.25,.5) {$s-k$};
~~~

Maintenant, on considère la position $s-i$ où  $s > s-i > s-k$, 
cela signifie qu'on
cherche un suffixe depuis une position interne au mot $u$ de gauche. Le point
clé permettant d'obtenir un algorithme linéaire est de remarquer que la
situation est la même que dans le mot $u$ de droite. Or, comme on procède de
gauche à droite, on a déjà calculé la valeur correspondante `suffixe[n-1-i]`. Là, on
a deux cas : 

* soit quand on a cherché le plus grand suffixe à partir de $n-i$, on s'est
    heurté à une erreur de comparaison à la position $n-k$. Dans ce cas, on a
    `suffixe[n-1-i]`$=k-i$ et on peut regarder, en partant de la position $s-k$,
    si on peut prolonger le suffixe finissant à la position $s-i$.

    !tikz(texte_bm_complet_suff_ex3)
    ~~~
    \draw (0,0) rectangle (2,.5);
    \draw[fill=blue!20] (2,0) rectangle node[anchor=center]{$a$} (2.5,.5);
    \draw[fill=green!20] (2.5,0) rectangle  (5,.5);
    \draw (5,0) rectangle (6,.5);
    \draw[fill=red!20] (6,0) rectangle node[anchor=center]{$b$} (6.5,.5);
    \draw[fill=green!20] (6.5,0) rectangle  (9,.5);
    \draw[fill=green!20] (6.5,0) rectangle  (9,.5);
    \draw[fill=purple!20] (6.5,0) rectangle  (8,.5);
    \draw[fill=purple!20] (2.5,0) rectangle  (4,.5);

    \node[above left] at (9.5,.5) {$n-1$};
    \node[above] at (6.25,.5) {$n-1-k$};
    \node[above left] at (5,.5) {$s$};
    \node[above] at (2.25,.5) {$s-k$};

    \node[above] at (7.75,.5) {$n-1-i$};
    \node[above] at (3.75,.5) {$s-i$};
    ~~~

    Pour effectuer ce prolongement, il suffit de comparer, caractère par
    caractère, vers la gauche en partant de la position $s-k$. On aboutira alors
    à une nouvelle position du suffixe finissant le plus à gauche qui finira en
    $s-i$.

    !tikz(texte_bm_complet_suff_ex4)
    ~~~
    \draw (0,0) rectangle (9,.5);
    \draw[fill=purple!20] (6,0) rectangle  (9,.5);
    \draw[fill=purple!20] (1,0) rectangle  (4,.5);
    \draw[fill=red!20] (5.5,0) rectangle node[anchor=center]{$c$} (6,.5);
    \draw[fill=orange!20] (0.5,0) rectangle node[anchor=center]{$d$} (1,.5);
    \draw[fill=blue!20] (2,0) rectangle node[anchor=center]{$a$} (2.5,.5);
    \draw[fill=blue!20] (7,0) rectangle node[anchor=center]{$a$} (7.5,.5);

    \node[above left] at (9.5,.5) {$n-1$};
    \node[above] at (5.75,.5) {$n-1-l$};
    \node[above] at (0.75,.5) {$s-i-l$};

    \node[above left] at (4,.5) {$s-i$};
    ~~~

    Remarquons qu'il n'est pas nécessaire que $s-i-l \neq s-k$. C'est-à-dire
    que même si $a$ ne permet pas de prolonger le suffixe déduit de la position
    $n-i$, on considère tout de même que la nouvelle position de référence est
    $s-i$. On en déduit également la valeur `suffixe[s-i]`$=l$.

* soit `suffixe[n-1-i]`$= p \neq k-i$ et alors 

    * soit $p < k-i$, on a alors poru ce suffixe un échec dans $u$, ce
      qui limite de la même manière la valeur en $s-i$ : `suffixe[s-i]`$=p$.

    * soit $p > k-i$, donc on doit avoir un $b$ après avoir le suffixe dans $u$
      depuis $s-i$ pour le prolonger, or, c'est impossible car il y a un $a
      \neq b$. Ainsi, le suffixe est limité par $u$ : `suffixe[s-i]`$=k-i$.

Il reste à traiter le cas où $s - i \le s-k$, ce qui revient à considérer qu'on
a dépassé le précédent suffixe pouvant apporter une information. On procède
donc naïvement pour trouver le plus grand suffixe depuis cette position.

On en déduit l'implémentation suivante :

!listing3l(SUFFIXE)(../../snippets/algorithmique/texte_boyer_moore)

!remarque
~~~~~
Cette implémentation est optimisée par rapport à la description précédente
en calculant directement sans introduire $i$ ou $k$, et en fusionnant deux cas
qui reviennent à dupliquer du code.

On donne ici le code maladroit qui correspond à la traduction exacte de la
description précédente :

!listing3l(SUFFIXINIT)(../../snippets/algorithmique/texte_boyer_moore)
~~~~~

On remarque que dans ce code, `plus_a_gauche` ne peut que diminuer, on effectue
donc au plus $n$ itérations dans la boucle `while` pour tout l'algorithme.
Donc, en considérant la boucle `for`, on effectue au plus $2n$ comparaisons de
caractères : au plus une pour chaque itération de la boucle `for` pour voir si
on entre dans le `while`, puis en tout au plus $n$ avant de sortir du `while`.

L'algorithme qu'on a obtenue est bien linéaire en $|x|$.

!paragraph(Obtention de `bonsuffixe` à partir de `suffixe`)

On reprend maintenant le calcul de `bonsuffixe[i]` dans le mot $x = x_0 \dots
x_{n-1}$. 

On cherche à obtenir 
des suffixe de la forme $b u v$ de $x$ où $b \neq x_i$ et $u = x_{i+1} \dots
x_{n-1}$ est un suffixe de $x$. Mais si `suffixe[k]`$=n-1-i$ cela signifie que ce
suffixe est exactement $u$ et qu'il est soit préfixe, soit précédé d'une lettre
différente de $x_i$, sinon $n-1-i$ ne serait pas maximal.

On a donc 
$$
\begin{array}{rcl}
\texttt{bonsuffixe}[n-1-i] & = & \min \enscomp{n-1-k}{\texttt{suffixe}[k] =
n-1-i} \\ & = & n -1 - \max \enscomp{k}{\texttt{suffixe}[k] = n-1-i}
\end{array}
$$
On remarque qu'on peut
ainsi faire croitre $k$ et poser :
$$\texttt{bonsuffixe}[n-1-\texttt{suffixe}[k]] = n-1-k$$
On a aura alors naturellement, à la fin de la boucle, la valeur minimale placée
en dernier.

Reste à considérer les valeurs non remplies ainsi dans le tableau `bonsuffixe`.
Elles correspondent aux positions $i$ telles qu'il n'existe pas de suffixe de
la forme $b u v$. On doit donc chercher un mot $uv$ de longueur minimale
dont $x$ est suffixe. Mais $u$ étant un suffixe de $x$, cela revient à
considérer les bords de $x$. La table `suffixe` permet également de détecter
les bords : si $x_0 ... x_k$ est un bord c'est que `suffixe[k]`$=k+1$.

Soit $k < n-1$ maximal vérifiant cette condition. 
Pour tout $u = x_{i+1} \dots x_n$ suffixe de $x$, pour qu'il ait $x_0 \dots
x_k$ comme suffixe, il faut qu'il soit strictement plus long (sinon on est dans le cas précédent), 
donc que $n-i > k+1 \iff
i < n-1-k$. Dans ce cas, $x$ est alors suffixe de $u v$ où $v = x_{k+1} \dots
x_{n-1}$ donc $|v|=n-1-k$. Les $k$ plus petits ne pourront alors que faire
augmenter $|v|$, on peut ainsi poser `bonsuffixe[i]`$=n-1-k$.

On en déduit un remplissage en parcourant
les $k$ dans l'ordre décroissant de $n-2$ à $0$, tout en maintenant l'indice
$i$ de la prochaine valeur à remplir dans `bonsuffixe`. Dès qu'on détecte un
bord, on place $n-1-k$ jusqu'à ce que $i \ge n-1-k$.

En sortie de boucle, il est possible que $i < n$ donc qu'il reste des valeurs à
remplir. On remarque dans ce cas là que pour que $x$ soit un suffixe de $u v$
il faut que $v = x$. On a donc pour ces valeurs restantes `bonsuffixe[i]`$=n$.

Comme ce second cas est toujours plus long que le premier quand les deux se
produisent en $i$, on implémente successivement les remplissages de sorte à
obtenir la valeur minimum. On en déduit le programme suivant :

!listing3l(BONSUFFIXE)(../../snippets/algorithmique/texte_boyer_moore)

Il est facile de constater que cet algorithme est de complexité temporelle linéaire en $|x|$.

!paragraph(Algorithme de Boyer-Moore)

On incorpore naturellement la table precédente à l'algorithme de Boyer-Moore en
choisissant le meilleur décalage entre cette table et la stratégie précédente.

!listing3l(BOYERMOORE)(../../snippets/algorithmique/texte_boyer_moore)

Supposons que le motif est de longueur $p$, que la chaîne dans laquelle on recherche
est de longueur $n$ et que la taille de l'alphabet est une
constante indépendante des entrées. La première partie de l'algorithme consiste
à construire les tables de sauts, comme on l'a vu, elle est en complexité en
temps et en espace en pire cas en $O(p)$.

On admet que l'algorithme Boyer-Moore complet, étant donné les deux tables de
saut et d'autres modifications mineures non présentées ici, 
est en complexité temporelle en pire cas en $O(n)$.

Il est assez raisonnable de penser que soit $p \le n$ quand on effectue une
recherche, soit on compte chercher un même motif dans plusieurs textes et on
réutilise ainsi les tables de sauts. Il n'est donc pas forcément très pertinent
de parler de la complexité globale de l'algorithme, mais lorsqu'on le fait, on
dit qu'elle est en $O(p+n)$. On rappelle ici le rôle de l'addition dans les
complexités qui fait référence à la succession de deux traitements, un en
$O(p)$ suivi d'un en $O(n)$.

!subsection(Algorithme de Rabin-Karp)
!subsubsection(Principe)
L'algorithme de Rabin-Karp est un algorithme de recherche d'un motif dans un
texte qui utilise une notion d'empreinte pour déterminer, en temps constant, si
il est probable que la position actuelle corresponde à une occurrence du motif.

Pour cela, si on cherche un motif de longueur $p$ sur l'alphabet $\Sigma$,
on considère une **fonction de hachage** $h : \Sigma^p \rightarrow X$. Les
éléments de l'ensemble $X$ sont appelés des empreintes et on suppose que
l'égalité entre deux empreintes se vérifie en temps constant contrairement à
l'égalité dans $\Sigma^p$ qui se vérifie en $O(p)$ dans le pire des cas. Le plus
souvent, on choisit pour $X$ un type entier machine.

!note(Sûrement mettre ici des renvois vers la partie portant le plus sur la
notion de fonction de hachage pour la définition la plus complète.)

Bien qu'il soit normalement aussi coûteux de calculer l'image par $h$ d'une
sous-chaîne de longueur $p$ que de tester l'égalité entre cette sous-chaîne et
le motif,  le point essentiel de l'algorithme de Rabin-Karp est d'utiliser une
fonction de hachage permettant un calcul incrémental en temps constant :

!center
~~~~~~~
!tikz(calcul_hachage_incremental)
```

\matrix (m) [matrix of math nodes,
    row sep=1mm,column sep=0mm, align=center,
    every node/.append style={rectangle,draw,anchor=center,minimum size=1cm,text height=2ex,text width=1em}]
    {
    a & c_2 & \dots & c_p & b \\
    };

    \draw [blue,decorate,decoration={mirror,brace,aspect=0.25,amplitude=10pt}] 
        (m-1-1.south) -- (m-1-4.south) node[near start,yshift=-.6cm] (s) {$s$};
    \node[anchor=base,text height=2ex,yshift=-1cm] (e) at (s.south) {$e$};
    \draw (s) edge[->] node[left] {$h$} (e);
    \draw [red,decorate,decoration={mirror,brace,aspect=0.75,amplitude=10pt}] 
        (m-1-2.south) -- (m-1-5.south) node[near end,yshift=-.6cm] (sp) {$s'$};
    \node[anchor=base,text height=2ex,yshift=-1cm] (ep) at (sp.south) {$e'$};
    \draw (sp) edge[->] node[right] {$h$} (ep);
    \draw(e) edge[thick,->] node[below] {$\delta_{a,b}$} (ep);
```
~~~~~~~

Ici, on considère donc, pour $a, b \in \Sigma$, une fonction de mise à jour
$\delta_{a,b} : X \rightarrow X$ telle que pour tout $c_2,\dots,c_p \in \Sigma$
on ait $\delta_{a,b}(h(ac_2 \dots c_p)) = h(c_2\dots c_p b)$.

L'algorithme de Rabin-Karp procède alors ainsi pour chercher $m$ de longueur $p$ dans la 
chaîne $s = c_0 \dots c_{n-1}$ où $n \ge p$ :

* calcul de $e_m = h(m)$ et $e = h(c_0..c_{p-1})$.
* Pour $i$ allant de $0$ à $n-p$ :

    * Si $e_m = e$, on renvoie un succès 
      pour la recherche à la position $i$ si $m = c_i \dots c_{i+p-1}$
    * si $i<n-p$ on met à jour l'empreinte $e \leftarrow \delta_{c_i,c_{i+p}}(e)$.

La complexité temporelle liée à la gestion des empreintes est donc en $O(n+p) =
O(n)$ car $n \ge p$. Par contre, pour calculer la complexité liée à la
recherche $m = c_i \dots c_{i+p-1}$, il est nécessaire d'estimer la proportion
de faux positifs, c'est-à-dire de positions $i$ telles que $e_m = e$ mais 
$m \neq c_i \dots c_{i+p-1}$. On va voir dans la partie suivante qu'on peut
supposer qu'elle est négligeable, ce qui permet de considérer que l'algorithme
de Rabin-Karp est linéaire.

!subsubsection(Choix d'une fonction de hachage)

Réaliser une bonne fonction de hachage est une question très complexe qui
dépasse le cadre du cours d'informatique de MPI. Cependant, il est possible
de réaliser ici une fonction de hachage répondant aux contraintes de Rabin-Karp
assez facilement.

Pour cela, on considère que les caractères sont des entiers compris entre 0 et
255, ce qui correspond au type des caractères non signés sur un octet. On peut
alors identifier une chaîne de longueur $p$ avec un nombre entre $0$ et $r^{p}
- 1$ où $r =2^8$, on note ainsi 
$$
P(c_0\dots c_{p-1}) = \sum_{i=0}^{p-1}
c_i r^{p-1-i} = c_0 r^{p-1} + c_1 r^{p-2} + \dots + c_{p-1}
$$

On considère de plus un entier premier $q$ et on pose $h(s)
= P(s) \mod q$ c'est-à-dire le reste de $P(s)$ dans la division euclidienne par
$q$. On peut ainsi définir $\delta_{a,b}(e) = (r (e - a r^{p-1}) + b) \mod q$.

Si on précalcule $r^{p-1} \mod q$  il suffit d'un nombre d'opération constant,
et indépendant de $p$,
pour calculer la nouvelle empreinte à l'aide de $\delta_{a,b}$.

Le point essentiel est alors de déterminer un nombre premier $q$ tel qu'il soit
peu probable d'obtenir des faux positifs. Une analyse mathématique permet
d'affirmer que chaque élément de $[|0;q-1|]$ a de l'ordre de $\frac{r^p}{q}$
antécédents par $h$. Ainsi, si on choisit deux chaînes aléatoirement dans
$\Sigma^p$, il y aura collision avec probabilité proche de $\frac{1}{q}$. En
considérant $q$ proche de la taille maximale pour le type entier considéré, on
minimise donc cette probabilité. 

!remarque
```
On peut également s'intéresser à des nombres $q$ pour lesquels le modulo 
soit rapide à calculer. Un exemple classique est $q = 2^{31} - 1$ car on peut
déduire la division euclidienne de $a$ par $q$ de l'écriture de $a$ en base
$2^{31}$. En effet, si $a = \sum_{k=0}^n a_k 2^{31k}$ comme $2^{31}-1 | 2^{31k} - 1$ pour $k \ge 1$, on
a $2^{31k} \equiv 1 ~[q]$ et ainsi $a \equiv \sum_{k=0}^n a_k [q]$. On
remarque que $a_k = (a >> 31k) \& 2^{31}$, on a alors soit $a_k < q$ et alors
$a_k \mod q = a_k$, soit $a_k = q$ et $a_k \mod q = 0$. Il suffit donc de faire un
masquage pour obtenir directement $a_k \mod q = (a >> 31k) \& q$.

On obtient alors le programme suivant :
!listing3(ocaml)(c)(python)
~~~
let rec fastmod a =
    let s = ref 0 in
    let x = ref a in
    let q = 0x7fffffff in
    while !x > 0 do
        s := !s + (!x land q);
        x := !x lsr 31
    done;
    if !s > q
    then fastmod !s
    else if !s = q
    then 0
    else !s
~~~
~~~
int64_t fastmod(int64_t a)
{
    int64_t s = 0;
    const int64_t q = 0x7fffffff;
    
    while (a > 0)
    {
        s = s + a & q;
        a = a >> 31;
    }

    if (s > q)
        return fastmod(s);
    if (s == q)
        return 0;
    return s;
}
~~~
~~~
def fastmod(a):
    s = 0
    q = 0x7fffffff
    while a > 0:
        s = s + a & q
        a = a >> 31
    if s > q:
        return fastmod(s)
    elif s == q:
        return 0

    return s
~~~
```

Le programme suivant implémente naïvement les calculs de $h$ et de
$\delta_{a,b}$ :

!listing3(ocaml)(c)(python)
~~~
let hash r q s =
    let p = ref 1 in
    let e = ref 0 in
    for i = String.length s - 1 downto 0 do
        e := (!p * (Char.code s.[i]) + !e) mod q;
        p := (r * !p) mod q
    done;
    !e

let delta r q rp a b e = (* rp est r^(p-1) mod q *)
    (r * (e - rp * (Char.code a)) + Char.code b) mod q
~~~
~~~
int64_t hash(int64_t r, int64_t q, char *s, int n)
{
    int64_t p = 1;
    int64_t e = 0;

    for (int i = n-1; i >= 0; i--)
    {
        e = (p * s[i] + e) % q;
        p = (r * p) % q;
    }

    return e;
}


int64_t delta(int64_t r, int64_t q, int64_t rp,
        char a, char b, int64_t e)
{
    return (r * (e - rp * a) + b) % q;
}
~~~
~~~
def hash(r,q,s):
    e = 0
    p = 1
    for c in reversed(s):
        e = (ord(c) * p + e) % q
        p = (r * p) % q
    return e

def delta(r,q,rp,a,b,e):
    return (r * (e - rp * ord(a)) + ord(b)) % q
~~~

!subsubsection(Implémentation)

Une implémentation directe de l'algorithme de Rabin-Karp est donnée dans le
programme qui suit. On se sert ici du caractère paresseux du `&&` pour
n'effecuter le test coûteux d'égalité des chaînes qu'en cas d'égalité des
empreintes.

!listing3(ocaml)(c)(python)
```
exception Trouve of int

let rabin_karp m s =
    let n = String.length s in
    let p = String.length m in
    let r = 256 in
    let q = 0x7fffffff in (* 2^(31)-1 *)
    let rp = pow r (p-1) q in
    let me = hash r q m in
    let e = ref (hash r q (String.sub s 0 p)) in
    try
        for i = 0 to n-p+1 do
            if me = !e && m = String.sub s i p
            then raise (Trouve i);
            if i+p < n then e := delta r q rp s.[i] s.[i+p] !e
        done; None
    with Trouve k -> Some k
```
```
int rabin_karp(char *m, char *s)
{
    const int64_t r = 256;
    const int64_t q = 0x7fffffff;
    const int p = strlen(m);
    const int n = strlen(s);
    const int64_t rp = powmod(r,p-1,q);
    const int64_t me = hash(r,q,m,p);
    int64_t e = hash(r,q,s,p);
    for (int i=0; i <n-p+1; i++)
    {
        if (me == e && strncmp(m,(s+i),p) == 0)
            return i;
        if (i+p < n)
            e = delta(r,q,rp,s[i],s[i+p],e);
    }
    return -1;
}
```
```
def rabin_karp(m, s):
    p, n = len(m), len(s)
    r, q = 256, 0x7fffffff
    rp = (r ** (p-1)) % q
    me, e = hash(r,q,m), hash(r,q,s[:p])
    for i in range(0,n-p+2):
        if me == e and m == s[i:i+p]:
            return i
        if i+p < n:
            e = delta(r,q,rp,s[i],s[i+p],e)
    return None
```

Si on suppose qu'il est improbable d'obtenir un faux positif, il est possible
de renvoyer un succès dès que les empreintes sont égales. L'avantage d'une
telle version est alors d'être un algorithme sans retour sur les données.
C'est-à-dire qu'il n'est pas nécessaire de garder en mémoire ou de réaccéder à
un caractère.

!subsubsection(L'algorithme originel de Rabin et Karp)

Si on regarde l'article originel de Rabin et Karp décrivant cette méthode, on
peut être étonné du fait que la méthode précédemment décrite était considérée
comme déjà connue dans la littérature par les auteurs.
En fait, ce qu'ils décrivent et annoncent comme étant novateur est
l'utilisation d'un algorithme probabiliste en choisissant aléatoirement une
fonction de hachage à chaque lancement de l'algorithme. En pratique, il s'agit
de choisir aléatoirement un nombre premier $q$ parmi un ensemble précalculé de
nombres premiers.

L'algorithme que l'on vient de décrire a un pire cas qui est très improbable
car on considère que la probabilité d'un faux positif est à peu près de $1/q$, 
donc moins de $5.10^{-10}$ pour $q = 2^{31}-1$. Le problème ici est la
notion de probabilité sur les entrées : est-on certain que l'algorithme recevra
une entrée choisie uniformément ? 
Rabin et Karp  parlent d'un
*adversaire intelligent* qui aurait connaissance de la fonction de hachage choisie
pour produire des entrées en pire cas. On pourrait ainsi imaginer une *attaque* sur serveur
effectuant une recherche avec Rabin-Karp suite à l'entrée d'un utilisateur. Un
adversaire pourrait construire une entrée en pire cas et tenter de surcharger le
serveur en l'effectuant de manière répétée.

Pour bien mettre en lumière ce phénomène, nous allons ici construire, dans un
cas très simple de fonction de hachage, une telle chaîne problématique. Pour
cela, considérons la fonction de hachage précédemment décrite dans le cas de
motif de taille 2, avec $\Sigma$ contenant les lettres de `a` à `z`, $r
= 26$ et $q = 17$. On considère une recherche du motif `aa` dont
l'empreinte est `0`, la même que celle des chaînes `ar` et `ra`. On peut donc
considérer la chaîne `arar...ar` qui produira un faux positif à
chaque étape.

!remarque
```
Détail des calculs. Ici on associe à `a` la valeur $0$, ..., à `z` la
valeur $25$. On a donc 
$$h(aa) = (0 \times 26 + 0) \mod 17 = 0$$
$$h(ar) = (0 \times 26 + 17) \mod 17 = 0$$
$$h(ra) = (17 \times 26 + 0) \mod 17 = 0$$
L'empreinte reste ainsi nulle tout au long de l'algorithme de Rabin-Karp et on
a un faux positif à chaque itération.
```

!section(Compression)
!subsection(Principe)

On s'intéresse ici à la compression parfaite d'un texte, c'est-à-dire, étant
donné un alphabet fixé $\Sigma$, qu'on cherche à réaliser un couple de
fonctions $\mathop{comp}, \mathop{dec} : \Sigma^* \rightarrow \Sigma^*$
telles que :

* pour tout mot $m \in \Sigma^*$, $\mathop{dec} (\mathop{comp}(m)) = m$
* pour la plupart des mots $m$ qui correspondent aux données qu'on cherche à
  compresser, $|\mathop{comp}(m)| < |m|$.

!remarque
```
Le fait que $\mathop{dec} \circ \mathop{comp} = \mathop{id}_{\Sigma^*}$
implique, comme on a pu le voir dans le cours de mathématique, que 
$\mathop{comp}$ est injective : deux mots différents ont nécessairement des
images distinctes.

Si $A$ et $B$ sont deux ensembles finis tels que $|A| < |B|$, 
il n'existe pas de fonction injective de $A$ dans $B$. Ainsi, si on note
$L_n$ les mots de $\Sigma^*$ de longueur au plus $n$, il ne peut exister de
fonction injective de $L_n$ dans $L_m$ où $n < m$. 

Autrement dit : il est impossible d'espérer pouvoir compresser toutes les
données de $L_n$. Si certains mots vont diminuer de longueur après compression, d'autres
vont nécessairement augmenter. 

Tout l'enjeu des algorithmes de compression
parfaites est alors de diminuer les longueurs des mots qui nous intéressent.
Par exemple, si on s'intéresse à des mots issus de textes en français, il est
plus important d'arriver à compresser une phrase comme `"ceci est un texte"`
plutôt qu'une suite de caractères non signifiante comme `"c2#$%1ajdn //@#3d!fn"`.
```

!subsection(Algorithme d'Huffman { #huffman })

La définition et la construction de l'arbre de Huffman ont été présentées au paragraphe
!ref[Algorithme d'Huffman - Compression](sec:huffman-arbre). On va s'intéresser
ici au processus complet permettant de compresser et décompresser des fichiers
avec cet algorithme.

!subsubsection(Calcul de la table d'occurences)
Par souci d'efficacité, on calcule une table d'occurences pour l'ensemble des
valeurs d'octets entre 0 et 255. Il suffit alors de parcourir le fichier pour
incrémenter les valeurs correspondant aux octets lus.

!listing3l(OCCURRENCES)(../../snippets/algorithmique/huffman)

!subsubsection(Sérialisation de l'arbre de Huffman)

Afin de décompresser, il est nécessaire de connaître l'arbre de Huffman donnant
le code préfixe. Pour cela, il faut stocker cet arbre dans le fichier comme une
série d'octet, on parle de *sérialisation*. Cette notion sera prolongée dans le
chapitre FIXME.

On choisit ici la représentation récursive `repr(a)` de l'arbre `a` définie
ainsi :

* Si `a = Noeud(g,d)`,  `repr(a) = 0 repr(g) repr(d)`
* Si `a = Feuille(c)`, `repr(a) = 1 c`.

!exemple
~~~~~
Si `a = Noeud(Feuille 42, Noeud(Feuille 16, Feuille 64))`, on obtient la suite
d'octets : `0 1 42 0 1 16 1 64`.
~~~~~

La lecture et l'écriture de la séralisation s'effectue alors simplement par
récurrence :

!listing3l(SERARBRE)(../../snippets/algorithmique/huffman)

!subsubsection(Écriture dans un fichier un bit à la fois)
!note
~~~~
À déplacer éventuellement dans une partie spécifique sur la gestion de
fichiers.
~~~~

Le propre de l'algorithme de Huffman est d'associer à chaque caractère un
codage binaire de longueur variable. Afin de pouvoir écrire ce codage dans un
fichier, il est nécessaire de grouper les bits par paquet de huit (octet en
français, byte en anglais). 

Ainsi, par exemple, si on a le codage suivant :
  
  c      | `'a'` | `'b'` | `'c'`
---------+-------+-------+--------
 code(c) |   0   |  100  |  101

et qu'on doit encoder `"abbaca"`, on obtient le mot binaire `010010001010`
qu'on complète avec des 0 à la fin et qu'on sépare en octets : 
`01001000 10100000`. On obtient donc les deux octets, convertis en décimal, 
72 et 160. Ce sont eux qu'on va écrire dans un fichier.

Une technique usuelle pour cela est de garder un accumulateur qui correspond à
l'octet en train d'être construit ainsi que le nombre de bits qui ont été
accumulé. Dès qu'on accumulé 8 bits, on peut construire l'octet, l'écrire dans
le fichier, puis reinitialiser ces variables.

Quand on rajoute un bit $b$ à l'accumulateur, on veut passer de $acc = b_1 \dots b_k$
à $b_1 \dots b_k b = 2 acc + b$.

On en déduit l'implémentation assez directe suivante :

!listing3l(OUTBIT)(../../snippets/algorithmique/bitpacking)

Il reste à traiter la question des zéros finaux, si l'accumulateur contient 
$k$ bits au moment de la fermeture du fichier, où $0 < k < 8$, il faut rajouter
$8-k$ zéros. On appelle cela du *padding* de l'anglais pour rembourrage. Ici,
cela correspond à faire un décalage binaire vers la gauche d'autant (*shift left* 
en anglais). Comme il sera nécessaire de se souvenir que ces zéros ne sont pas
signifiants à la lecture, on rajoute un octet final contenant cette valeur $k$.

On obtient alors la fonction de fermeture de fichier suivante :

!listing3l(OUTBITCLOSE)(../../snippets/algorithmique/bitpacking)

!remarque
~~~~
Une autre possibilité consiste à rajouter un entier décrivant la taille des
données non compressées. C'est d'ailleurs parfois un problème avec les formats
car si la taille est stockée sur 4 octets cela limite la taille d'un fichier
pouvant être compressé.
~~~~

Pour la lecture, on procède de même en faisant attention à deux points :

* on va lire les bits dans l'octet de la gauche vers la droite, c'est-à-dire du
  bit de poids le plus fort au bit de poids le plus faible. Ainsi, si
  l'accumulateur contient $acc = b_1 \dots b_8$, il suffit de faire un *et* bit
  à bit avec `b10000000=0x80=128` pour obtenir $acc \& 0x80 = b_1 0 \dots 0$
  donc un nombre qui vaut $0$ si et seulement si $b_1 = 0$. Après avoir
  effectué cette lecture, il suffit de décaler vers la gauche en multipliant
  l'accumulateur par 2 : $2 acc = b_2 \dots b_8 0$.
* on doit tenir compte des zéros finaux, pour ça, on a besoin de savoir qu'on
  est en train de lire le dernier caractère du fichier. On calcule donc la
  taille du fichier à son ouverture et on test si l'octet lu est
  l'avant-dernier, auquel cas on lit le dernier octet et on diminue d'autant le
  nombre de bits signifiants dans l'accumulateur.

On obtient alors le programme suivant pour la lecture :

!listing3l(INBIT)(../../snippets/algorithmique/bitpacking)

!subsubsection(Compression d'un octet)

Pour pouvoir compresser un octet, il est nécessaire d'obtenir le chemin qui
mène jusqu'à la feuille dont il est l'étiquette dans l'arbre de Huffman. Pour
cela, on commence par calculer l'ensemble des chemins de l'arbre de Huffman
sous forme d'une table à 256 entrées qui contient le chemin associé à un octet
s'il est présent dans l'arbre ou un chemin vide sinon. On parlera de
représentation plate de l'arbre de Huffman.

Il suffit de faire un parcours exhaustif de l'arbre (FIXME référence aux parcours 
d'arbres) pour réaliser cette table :

!listing3l(CHEMINS)(../../snippets/algorithmique/huffman)

Afin de compresser un octet, on va donc aller lire le chemin dans cette table
puis écrire le mot binaire correspond grâce aux fonctions d'écriture bit à bit
:

!listing3l(COMPRESSEBYTE)(../../snippets/algorithmique/huffman)

!remarque
~~~~
A chaque fois qu'on va compresser un octet, on va parcourir la liste
correspondant à son chemin. Comme les chemins les plus longs sont les moins
fréquents, cela ne pose pas vraiment de problèmes.

Cependant, il est possible d'optimiser cela en ne stockant pas le chemin mais
la fonction d'écriture elle-même. Ainsi, on ne va plus stocker des listes de
booléens mais des fonctions du type `Bitpacking.out_channel_bit -> unit` qui
vont réaliser l'écriture compressé de l'octet correspondant.

Au cours du parcours de l'arbre, on maintient une fonction correspondant à
l'écriture du préfixe du chemin `chemin`. Si on l'appel est effectué sur un noeud,
on remplace `chemin` par la fonction qui appelle `chemin` puis écrit le bit
correspondant au côté gauche ou droit.

Cela correspond au programme suivant :

!listing3l(COMPRESSEBYTECONTINUATION)(../../snippets/algorithmique/huffman)

Cette représentation des chemins partiels par des fonctions est très classique
dans le style de programmation fonctionnelle par passage de continuations.
~~~~

!subsubsection(Décompression d'un octet)

Pour décompresser un octet, il suffit de parcourir l'arbre de Hufmann en lisant
bit à bit le fichier compressé en descendant à gauche ou à droite selon que le
bit lu soit 0 ou non. Dès qu'on arrive sur une feuille, on écrit dans le
nouveau fichier le caractère correspondant.

!listing3l(DECOMPRESSEBYTE)(../../snippets/algorithmique/huffman)

!subsubsection(Compression et décompression de fichiers)

En mettant bout à bout l'ensemble des fonctions, on obtient la fonction suivante
qui réalise la compression complète d'un fichier :

!listing3l(COMPRESSEFICHIER)(../../snippets/algorithmique/huffman)

On obtient de même la fonction de décompression suivante :

!listing3l(DECOMPRESSEFICHIER)(../../snippets/algorithmique/huffman)

!exemple
~~~~~
En compressant ainsi l'intégrale de Proust, on passe de 7543767 octets à 
4249758 octets. A titre de comparaison, l'outil unix `zip` permet d'obtenir un
fichier de 2724213 octets.
~~~~~

!remarque
~~~~~
Le format de fichier présenté ici est rudimentaire. Les formats usuels sont en
général plus complexes pour gérer 

* l'identification : c'est-à-dire pouvoir déterminer qu'un fichier est un
  fichier compressé par un certain programme. Les fichiers `zip` commencent
  ainsi par les deux lettres `PK`.
* l'extensibilité : il est possible qu'on souhaite changer le format de
  sérialisation de l'arbre, ou même l'algorithme. En rajoutant un système de
  version sur les différentes parties, on peut permettre de faire évoluer un
  type de fichier en préservant la compatibilité avec les versions précédentes.

~~~~~

!subsection(Algorithme de Lempel-Ziv-Welch)

!section(Problèmes supplémentaires)
!subsection(La structure de donnée **corde**)
!subsection(L'algorithme de Knuth-Morris-Pratt)
!subsection(Extensions à l'analyse d'images)
