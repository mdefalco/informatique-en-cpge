---
title: Informatique
subtitle: MPI
author: Marc de Falco
date: !today
---

`\part{Algorithmique}`{=tex}

`\chapter{Jeux à deux joueurs}`{=tex}

L'objectif de ce chapitre est d'étudier les jeux à deux joueurs. De tels jeux 
partent de l'observation des jeux classiques.

# Représentation de jeux

## Arbres de jeux

On considère deux joueurs nommés _Joueur_ et _Opposant_, l'objectif étant
d'étudier les stratégies dont dispose Joueur pour gagner, d'où la terminologie.

