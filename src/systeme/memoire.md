!chapterimage(chap_memoire.jpg)(Gestion de la mémoire dans un programme compilé)

!remarque(Ce chapitre se concentre sur la manière dont un programme compilé
gère la mémoire. Il est question, en particulier, de la notion de variable. Le
modèle dans lequel on se place est celui du langage C où une variable est un
emplacement mémoire.)

!section(Organisation de la mémoire {#sec:orga})

!note(Ici, je fais le choix d'une présentation assez informelle pour ne pas
qu'elle soit trop liée à la réalité d'un compilateur en particulier.)

Un programme compilé gère la mémoire d'un ordinateur de deux manières très
différentes

* statiquement : c'est le cas des variables locales ou globales définies dans
  le programme. Au moment de la compilation, le compilateur dispose de
  l'information suffisante pour prévoir de la place en mémoire pour stocker ces
  données.
* dynamiquement : c'est le cas des objets dont la taille n'est connue qu'à
  l'exécution et peut varier selon l'état du programme. C'est alors au moment
  de l'exécution que le programme va faire une demande d'allocation pour
  obtenir une place mémoire.

!twocolumns[12]
~~~~~~~~

En terme d'allocation statique, on peut distinguer plusieurs types de mémoire :

* les variables globales initialisées qui seront stockées dans le binaire et placées en
  mémoire dans une zone spécifique chargée avec le binaire
* les variables globales non initialisées dont seule la déclaration sera dans
  le binaire et qui seront allouées, placées en mémoire et initialisées à 0
  au moment du chargement du binaire
* les variables locales et les paramètres qui sont placés dans une pile afin
  de les allouer uniquement au moment de l'exécution du bloc ou de la fonction


L'allocation dynamique utilise une zone mémoire appelée tas dont une possible organisation 
est développée dans la partie [@sec:custommalloc].

~~~~~~~~
~~~~~~~~

!tikz(memorymap)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\draw[thin, draw=red!20!white, fill=red!10!white] (0,0) rectangle node[scale=0.7] {code binaire} (3,-.5);
\draw[thin, draw=blue!20!white, fill=blue!10!white] (0,-.5) rectangle node[scale=0.7] {variables globales init.} (3,-1);
\draw[thin, draw=green!20!white, fill=green!10!white] (0,-1) rectangle node[scale=0.7] {var. globales non init.} (3,-1.5);
\draw[thin, draw=black!20!white, fill=black!10!white] (0,-1.5) rectangle node[scale=0.7] {tas} (3,-3.5);
\draw[thin, draw=orange!20!white, fill=orange!10!white] (0,-3.5) rectangle node[scale=0.7] {pile} (3,-4.5);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
`<br/>`{=html}Structure de la mémoire associée à un programme

~~~~~~~~

Considérons le programme c suivant.

!listing(#memoirecodeex1 .c)
~~~~
!include(../../snippets/systeme/memoire/orga.c)
~~~~

Il est possible d'observer la manière dont sa mémoire sera répartie en utilisant la commande `objdump` :

!listing(.zsh)
~~~~
!include(../../snippets/systeme/memoire/orga.map)
~~~~

On retrouve dans les sections mémoire :

!twocolumns[12]
~~~~~~~~

* `.text` contenant le programme binaire
* `.data` contenant les variables globales initialisées et non constantes
* `.bss`  contenant les variables non initialisées
* `.rodata` contenant les variables globales initialisées mais constantes.

~~~~~~~~
~~~~~~~~
Ici, le schéma mémoire est un peu plus compliqué car une zone mémoire distincte
est prévue pour les variables constantes initialisées pour des raisons de
sécurité.
~~~~~~~~

La pile et le tas sont automatiques et n'ont pas besoin de figurer dans le
binaire, c'est pour cela qu'on ne les trouve pas dans la liste.

Dans cette description mémoire, on trouve la table des symboles qui décrit où
vont se trouver en mémoire certaines variables.

   Nom de variable           Sorte de déclaration           Section mémoire
----------------------   ----------------------------- -------------------------
          a              constante globale initialisée          rodata 
          b              globale initialisée                    data 
          c              globale non init.                      bss 

La section suivante permettra de compléter le tableau en étudiant comment les
paramètres et variables locales sont gérés. On remarque cependant qu'il n y a
pas de symboles pour ceux-ci. En effet, le nom des variables locales est a
priori perdu après la compilation contrairement aux variables globales.

!section(Portée d'un identificateur)

En ce qui concerne une variable dans un programme, on peut définir deux notions
d'apparence assez similaire.

D'une part la portée d'un identificateur qui correspond au texte du
programme :

!definition
~~~
La **portée** d'un identificateur est définie par la zone du texte d'un
programme dans laquelle il est possible d'y faire référence sans erreurs. 
~~~

!remarque(Dans le langage C, un identificateur peut être utilisé dès sa
déclaration mais tant que la variable n'est pas initialisée, le comportement
n'est pas spécifié et il faut considérer cela comme une erreur. Le compilateur
produit ainsi un avertissement quand on utilise le paramètre `-Wall`.

Dans le cas d'une définition, il est ainsi possible de faire référence à l'identificateur
dans l'expression de son initialisation : `int x = x`. Ce cas est pathologique
et le fait qu'on compte la ligne de déclaration dans la portée ne devrait pas
inciter à écrire ce genre de code qui produira, de toutes façons, une erreur
avec les options `-Wall -Werror`.)

Dans le programme :

!listing(c .numberLines)
~~~
int a = 1;

int f (int x)
{
    int y = x + a;
    return y;
}

int g()
{
    int z = 3;
    return z + f(z);
}
~~~

La portée des identificateurs est :

 Identificateur        Portée
-----------------   -------------
      a                1-13
      x                3-7
      y                5-7
      f                4-13
      g                10-13
      z                11-13
      

Pour une fonction, afin
de pouvoir écrire des fonctions récursives, l'identificateur est utilisable
dans le corps de la fonction.

Comme la portée est une notion associée aux identificateurs, elle est
indépendante de la notion de variables. Si on considère le programme suivant :

!listing(c .numberLines)
~~~
int f() 
{
    int i = 3;

    return i;
}

int g() 
{
    int i = 5;

    return i+1;
}
~~~

L'identificateur `i` a pour portée les lignes 3-6 et 10-13.

Un autre phénomène plus complexe peut se produire quand on redéfinit 
un identificateur dans sa portée.

Considérons le programme suivant

!listing(c .numberLines)
~~~
int f() 
{
    int i = 3;

    for (int j = 0; j < 3; j++) 
    {
        int i = 4;
        
        i += j;
    }

    return i;
}
~~~

Ici, l'identificateur `i` a pour portée les lignes 3-13 mais dans les lignes
7-10 il y a un phénomène dit de masquage où la première définition est cachée
par la seconde.

L'identificateur associé à une variable globale a pour portée l'ensemble des
lignes suivant sa déclaration.


!remarque
~~~
En C, la portée d'un identificateur est **statique** : elle dépend uniquement
du texte du programme au moment de la compilation.

En Python, la portée d'un identificateur est **dynamique** : elle peut dépendre
de l'exécution d'un programme. Par exemple si on considère le programme Python

!listing(.python .numberLines)
```
if condition:
    x = 3
```

La portée de l'identificateur `x` dépend ici du fait que la `condition` soit
réalisée ou non.
~~~

!section(Durée de vie d'une variable)

!definition
~~~
La **durée de vie** d'une variable correspond à la période de son exécution
durant laquelle la variable est présente en mémoire.
~~~

Sauf indication contraire, la durée de vie d'une variable locale en C est définie par
la portée de l'identificateur qui lui est associé : la place est réservée au début 
de sa portée et libérée à la fin.

La durée de vie d'une variable globale est l'intégralité du programme. En effet,
comme on a pu le voir dans la section [@sec:orga], 
il est possible de définir en C des variables locales dont la durée de vie
dépasse sa portée. On parle alors de variables *statiques*. Ce point étant
assez technique, il sera ignoré ici.

!section(Piles d'exécution, variables locales et paramètres)

On a vu qu'en raison de leur durée de vie, les variables globales étaient
allouées dès le chargement du programme. Pour les variables locales ainsi
que la mécanique des appels, on utilise une **pile**.

Cette pile d'exécution est représentée en mémoire par un tableau et un 
indicateur de fond de pile. 

Le remplissage de ce tableau s'effectue souvent des adresses hautes vers les
adresses faibles : on empile en faisant diminuer les adresses.

!remarque
~~~~~~~~~
En fait, il existe des
architectures où les adresses sont croissantes. Ce qui importe est que le tas
et la pile aient des comportements opposées pour qu'ils puissent grandir dans
la même zone mémoire.
~~~~~~~~~


Un compilateur peut faire le choix d'utiliser directement des registres
processeurs pour les variables locales ou pour passer des paramètres à une
fonction. Ici, pour simplifier, on va supposer que ce n'est pas le cas et que
tout passe par la pile d'exécution.

!remarque
~~~~~~~~~~~~
Afin de pouvoir appeler une fonction dans une bibliothèque potentiellement compilée 
avec un autre compilateur, il est nécessaire d'avoir une convention d'appels de
fonctions. Une telle convention est appelée une *interface applicative binaire*
(Application Binary Interface).

La convention `System V AMD64 ABI` qui est celle de Linux et macOS sur des
architectures 64bits
consiste à utiliser des registres entiers pour les six premiers arguments
entiers ou pointeurs et des registres flottants pour les huit premiers arguments
flottants. Les arguments suivants sont alors passés sur la pile (donc dès le
septième entier/pointeur ou neuvième flottant).

La convention `cdecl` qui est assez répandue sur les architectures 32bits
consiste à utiliser la pile. Par contre la valeur de retour est présente dans
des registres comme pour la convention `System V AMD64 ABI`.
~~~~~~~~~~~~

!twocolumns[12]
~~~~~~~~
Lors d'un appel d'une fonction passant par la pile, 
on commence par empiler les paramètres (souvent de la droite vers la gauche) puis
on empile l'adresse à laquelle doit
revenir l'exécution une fois que la fonction aura terminé son exécution.

Au début de l'exécution de cette fonction, on place sur la pile l'adresse du
fond de pile et on déplace celui-ci pour réserver de la place pour les
variables locales.

L'empreinte sur la pile d'un appel de fonction est appelée une structure pile
(**stack frame** en anglais). La pile est alors organisée, depuis l'appel au
point d'entrée du programme, par empilement et dépilement de structures piles.
~~~~~~~~
~~~~~~~~
!tikz(stackframe)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\draw[thin, draw=red!20!white, fill=red!10!white] (0,0) rectangle node[scale=0.7] {paramètres} (3,-.5);
\draw[thin, draw=blue!20!white, fill=orange!10!white] (0,-1) rectangle node[scale=0.7] {sauvegarde de registres} (3,-1.5);
\draw[thin, draw=green!20!white, fill=green!10!white] (0,-1.5) rectangle node[scale=0.7] {variables locales} (3,-3);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
`<br/>`{=html}Structure pile associée à un appel de fonction
~~~~~~~~


Voici un exemple possible de l'état de la pile d'exécution lors de l'exécution d'un
programme compilé avec la norme `cdecl` (Il suffit d'ajouter l'argument
`-m32` pour compiler en 32bits).

!twocolumns
~~~~~~~~~~~~~~
!listing(c .numberLines)
```
!include(../../snippets/systeme/memoire/stackframe1.c)
```
~~~~~~~~~~~~~~
~~~~~~~~~~~~~~
!tikz(stackframe1)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\draw[thin, draw=orange!20!white, fill=orange!10!white] 
    (0,2) rectangle node[scale=0.7] {3 (x)} (3,2.5);
\draw[thin, draw=green!20!white, fill=green!10!white] 
    (0,1.5) rectangle node[scale=0.7] {sauvegarde registre} (3,2);
\draw[thin, draw=green!20!white, fill=green!10!white] 
    (0,1.5) rectangle node[scale=0.7] {adresse de retour} (3,1);
\draw[thin, draw=red!20!white, fill=red!10!white] (0,0.5) rectangle node[scale=0.7] {10 (a)} (3,1);
\draw[thin, draw=red!20!white, fill=red!10!white] (0,0) rectangle node[scale=0.7] {12 (b)} (3,.5);
\draw[thin, draw=black] 
    (0,0) rectangle node[scale=0.7] {} (3,-0.5)
    (0,-0.5) rectangle node[scale=0.7] {} (3,-1)
    ;

\node at (1.5,-1.5) {$\vdots$};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~

A chaque appel de fonction, on va donc empiler une structure de pile complète,
puis la dépiler à la sortie. Ce mécanisme est essentiel pour permettre la
récurrence car il permet d'effectuer plusieurs appels d'une même fonction sans
risquer que la mémoire utilisée lors d'un des appels interfère avec un autre. 
On comprend également les limites de la récursivité ici car cet empilement
successif de structures de piles peut dépasser la taille maximale de la pile
d'exécution : on parle alors de *dépassement de pile* ou **stack overflow** en
anglais.

!remarque
```
Il est possible de définir des variables locales qui soient situées au même
emplacement mémoire pour tous les appels d'une fonction, c'est ce qu'on appelé
des variables *statiques* dans le paragraphe précédent.

Ce mécanisme est essentiellement géré comme les
variables globales et il ne sera pas développé dans la suite.
```

!note
```
Je me demande s'il faudrait parler plus précisement de la manière dont la pile
est gérée avec les registres ebp/esp. Mais ça ne me semble pas apporter grand
chose ici.
```

!section(Allocation dynamique)

Comme cela a été vu dans la section [@sec:orga], il est possible d'allouer
dynamiquement de la mémoire. Pour gérer cette allocation dynamique, on passe
par une zone mémoire appelé le *tas* ainsi que par un mécanisme d'allocation et
de libération de mémoire au niveau du système.

Pour l'utilisateur, cette gestion interne est transparente et on peut se
contenter de considérer qu'il y a deux mécanismes :

* l'**allocation** mémoire où on demande à ce qu'une zone mémoire d'une
  certaine taille soit allouée
* la **libération** mémoire où on signale que la zone mémoire peut être
  récupérée par le système.

Naturellement, la mémoire d'un ordinateur étant finie, il est très important
de libérer au plus tôt la mémoire non utilisée pour éviter d'épuiser la
mémoire. Quand un programme ne libère pas toute la mémoire qu'il alloue, on
parle de **fuite mémoire**. L'empreinte mémoire d'un tel programme peut alors
croître jusqu'à rendre le programme ou le système inutilisable.

!subsection(Allocation)

Pour allouer une zone mémoire, on utilise la fonction `malloc` dans `stdlib.h`
de signature :

!listing(c)
~~~
void *malloc(size_t size)
~~~

Ici `size` indique le nombre d'**octets** à allouer et la fonction renvoie un
pointeur vers le premier octet alloué. Comme la fonction ne connait pas le type
d'objets alloués, on utilise ainsi le type `void *`.

Ce type joue un rôle spécial et on peut changer directement le type de la
valeur de retour sans rien avoir à écrire d'autre l'appel à malloc :

!listing(c)
~~~
char *t = malloc(n);
~~~

!remarque
~~~
Dans le langage C++ qui peut être vu comme un successeur de C, il est
obligatoire de préciser ici le nouveau type à l'aide d'un
un **transtypage**. Pour convertir la valeur `x` vers le type
`char *` on écrit alors `(char *)x`. Ainsi, pour allouer un tableau de `n`
caractères, on utilisera :

!listing(c)
```
char *t = (char *) malloc(n);
```

Bien que ce ne soit pas nécessaire en C, il est fréquent de rencontrer des
programmes présentant de tels transtypages qui sont superflus mais corrects
syntaxiquement en C.
~~~

Pour obtenir la taille à allouer, il peut être utile d'utiliser l'opérateur
`sizeof` qui prend en entrée un type ou une valeur et renvoie sa taille. Ainsi
si on peut allouer un tableau de $n$ entiers non signés ainsi :

!listing(c)
```
unsigned int *t = malloc( sizeof(unsigned int) * n );
```

et cet appel ne dépend de la taille prise par un `unsigned int` sur
l'architecture.

Une autre raison de l'utilisation de `sizeof` est l'extensibilité. Par exemple,
si on a un `struct point` représentant des points en 2D :

!listing(c)
```
struct point {
    float x;
    float y;
};
```

on peut allouer un tableau de $n$ points ainsi :

!listing(c)
```
struct point *t = malloc( sizeof(struct point) * n );
```

Si jamais on change la structure pour représenter des points en 3D ainsi :

!listing(c)
```
struct point {
    float x;
    float y;
    float z;
};
```
il sera inutile de changer le code d'allocation du tableau car `sizeof(point)`
tiendra compte automatiquement du changement.

Si jamais une erreur empêche d'allouer la mémoire - ce qui peut être le cas
s'il n'y a plus de mémoire disponible - le pointeur renvoyé par `malloc` a la
valeur spéciale `NULL`.

!remarque
```
La zone mémoire renvoyée par `malloc` n'est pas initialisée. On ne peut pas
supposer qu'elle soit remplie de la valeur 0. Il faut donc manuellement
initialiser la mémoire après le retour de `malloc`.
```

!subsection(Libération)

Pour libérer la mémoire, on utilise la fonction `free` également présente dans `stdlib.h` et
dont la signature est :

!listing(c)
~~~
void free(void *ptr);
~~~

!remarque
```
Il est très important d'utiliser uniquement un pointeur obtenu précédemment par
un appel à `malloc` et de ne pas l'utiliser plus d'une fois.

Le programme suivant provoque une erreur `free(): invalid pointer` à
l'exécution mais est détécté par un avertissement du compilateur : 
`warning: attempt to free a non-heap object ‘a’`.

!listing(c)
~~~
!include(../../snippets/systeme/memoire/free_pas_malloc.c)
~~~

Le programme suivant alloue un tableau de deux `char` et appelle `free` sur
l'adresse de la seconde case. En faisant cela, il n'y a pas d'avertissement car
on appelle `free` sur un objet qui est effectivement sur le tas. On obtient
alors à nouveau une erreur à l'exécution `free(): invalid pointer`.

!listing(c)
~~~
!include(../../snippets/systeme/memoire/free_pas_malloc_2.c)
~~~

Le programme suivant libère deux fois la mémoire et provoque l'erreur 
`free(): double free detected in tcache 2` à l'exécution.

!listing(c)
~~~
!include(../../snippets/systeme/memoire/double_free.c)
~~~

```

!subsection(Protection mémoire)

Comme on l'a vu dans la première partie, quand un programme s'exécute il a 
un environnement mémoire constitué de plusieurs zones,
parfois appelées **segments**, avec le droit d'écriture dans certaines d'entre elles.

Le système d'exploitation protège ainsi la mémoire et, au niveau matériel,
l'unité de gestion de la mémoire connait les adresses accessibles à un
programme. En cas d'accès anormal, le matériel provoque une erreur qui remonte
au système d'exploitation qui termine l'exécution du programme avec une erreur
souvent intitulée `Segmentation fault`.

Voici quelques exemples commentés produisant des erreurs de type `segmentation fault` 
à l'exécution.

!twocolumns
~~~~~~~~
Lecture à l'adresse 0, ce qui provoque toujours une erreur.

Même problème avec une adresse inaccessible ou invalide.
~~~~~~~~
~~~~~~~~
!listing(c)
```
!include(../../snippets/systeme/memoire/segfault1.c)
```
~~~~~~~~

!twocolumns
~~~~~~~~
Écriture dans une zone en lecture seule comme le segment du code.
~~~~~~~~
~~~~~~~~
!listing(c)
```
!include(../../snippets/systeme/memoire/segfault2.c)
```
~~~~~~~~

!subsection(Réalisation d'un système d'allocation de mémoire {#sec:custommalloc})

!note(Prérequis : listes chaînées)

Afin de comprendre comment fonctionne le tas, et en particulier malloc et free,
on va simuler ici ce comportement en allouant une grande plage de mémoire avec
malloc et en gérant le découpage et l'allocation de celle-ci.

Pour gérer les blocs mémoires libres, on utilise une liste circulaire. Une
liste circulaire est une liste chaînée avec un lien supplémentaire entre le
premier et le dernier maillon, ce qui fait qu'on peut considérer n'importe quel
maillon comme étant la *tête* de la liste.

!twocolumns
~~~~~~~~~~~~~~~
Une liste chaînée :

!tikz(listechainee)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\node[rectangle split, rectangle split parts=2, draw] (l1) {
a
  \nodepart{two}
};

\node[rectangle split, rectangle split parts=2, draw] (l2) at (1,0) {
b
  \nodepart{two}
};

\node[rectangle split, rectangle split parts=2, draw] (l3) at (2,0) {
c
  \nodepart{two}
};

\node[rectangle split, rectangle split parts=2, draw] (l4) at (3,0) {
d
  \nodepart{two}
  $\times$
};

\draw (l1.two) edge[thick,red,->] (l2);
\draw (l2.two) edge[thick,red,->] (l3);
\draw (l3.two) edge[thick,red,->] (l4);

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ici le dernier maillon comprend un pointeur qui ne pointe sur rien indiqué par
$\times$, en pratique il a la valeur `NULL`

~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~
Une liste circulaire :

!tikz(listecirculaire)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\node[rectangle split, rectangle split parts=2, draw] (l1) {
a
  \nodepart{two}
};

\node[rectangle split, rectangle split parts=2, draw] (l2) at (1,1) {
b
  \nodepart{two}
};

\node[rectangle split, rectangle split parts=2, draw] (l3) at (2,0) {
c
  \nodepart{two}
};

\node[rectangle split, rectangle split parts=2, draw] (l4) at (1,-1) {
d
  \nodepart{two}
};

\draw (l1.two) edge[thick,red,->] (l2);
\draw (l2.two) edge[thick,red,->] (l3);
\draw (l3.two) edge[thick,red,->] (l4);
\draw (l4.two) edge[thick,red,->] (l1);

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le seul changement est donc de faire pointer le dernier maillon sur le premier.
Le fait d'avoir preserver les valeurs dans les maillons permet ici de voir ce
qu'est devenu le premier maillon, mais on peut accéder à cette liste par
n'importe lequel de ces maillons.

~~~~~~~~~~~~~~~

Les noeuds de la liste circulaire de blocs libres auront pour valeur un triplet
`(adresse,taille,libre)` qui indique qu'à l'adresse `adresse` il y a un bloc de
`taille` octets et le booléen `libre` indique si ce bloc a été alloué ou non.

Pour cela on commence par définir une structure `bloc` et une fonctiond de
création d'un bloc :

!listing(c)
```
struct bloc {
    void *adresse;
    uint32_t taille;
    bool libre;
    struct bloc *suivant;
};

struct bloc *cree_bloc(void *adresse, uint32_t taille, bool libre)
{
    struct bloc *b = malloc(sizeof(struct bloc));
    b->adresse = adresse;
    b->taille = taille;
    b->libre = libre;
    return b;
}
```

On définit ensuite deux variables globales :

* `bloc_libres` qui va pointer sur un maillon de la liste circulaire des blocs
* `plage_memoire` qui pointe sur l'adresse de la plage mémoire que l'on va
  gérer et servira à la libérer en sortie de programme.

!listing(c)
```
struct bloc *blocs_libres;
void *plage_memoire;
```

La fonction `creation_blocs_libres` permet de créer la liste circulaire avec un premier
bloc qui pointe sur lui-même et qui correspond à l'adresse que l'on va placer
dans `plage_memoire`.

!listing(c)
```
void creation_blocs_libres(uint32_t taille_bloc_initial)
{
    plage_memoire = malloc(taille_bloc_initial);
    blocs_libres = cree_bloc(plage_memoire, taille_bloc_initial, true);
    blocs_libres->suivant = blocs_libres; // boucle initiale
}
```

Pour libérer la liste à la sortie du programme, on définit la fonction
`destruction_blocs_libres` qui présente ainsi le parcours usuel d'une liste
circulaire : on procède comme pour une liste chaînée classique mais, au lieu
d'utiliser un test `bloc_courant->suivant == NULL` pour l'arrêt, il faut se
souvenir du premier bloc et tester pour voir si on est revenu au point de
départ. On n'oublie pas de libérer l'espace `plage_memoire` à la fin.

!listing(c)
```
void destruction_blocs_libres()
{
    struct bloc *premier_bloc = blocs_libres;
    struct bloc *bloc_courant = premier_bloc;

    // on boucle pour libérer chaque maillon
    while (true) {
        struct bloc *bloc_suivant = bloc_courant->suivant;
        free(bloc_courant);
        if (bloc_suivant == premier_bloc) return;
        bloc_courant = bloc_suivant;
    }

    // on libère la plage mémoire initiale
    free(plage_memoire);
}
```

Pour allouer $t$ octets, on parcourt la liste des blocs jusqu'à trouver un bloc
$b$ libre de taille $b.t$ telle que $b.t \ge t$. Si un tel bloc n'existe pas, on renvoie le
pointeur `NULL` signe d'un échec d'allocation. Sinon, on indique que le bloc
est occupé, on va renvoyer l'adresse du bloc obtenu mais, si $b.t > t$ on
insère après $b$ un nouveau bloc libre de taille $b.t - t$. Dans tous les cas,
on fait pointer la liste des blocs libres vers le bloc qui suit $b$, qui est
peut-être le bloc nouvellement créé et a de grandes chances d'être libre.

Ce mécanisme est implementé dans la fonction `allocation` :

!listing(c)
```
void *allocation(uint32_t taille)
{
    struct bloc *premier_bloc = blocs_libres;
    struct bloc *bloc_courant = premier_bloc;

    while (!bloc_courant->libre || bloc_courant->taille < taille)
    {
        bloc_courant = bloc_courant->suivant;
        if (bloc_courant == premier_bloc)
        {
            // Retour au point de départ : échec d'allocation
            return NULL;
        }
    }
    
    // bloc_courant pointe sur un bloc libre de bonne taille

    void *adresse = bloc_courant->adresse;
    bloc_courant->libre = false;
    if (bloc_courant->taille > taille) {
        // on le sépare en deux pour récupérer la place
        struct bloc *bloc_libre = cree_bloc(adresse+taille,
                bloc_courant->taille-taille, true);
        bloc_courant->taille = taille;
        bloc_libre->suivant = bloc_courant->suivant;
        bloc_courant->suivant = bloc_libre;
    }

    // On pointe sur le bloc suivant qui est sûrement libre
    blocs_libres = bloc_courant->suivant;

    return adresse;
}
```

Pour libérer un bloc, on parcourt la liste jusqu'à trouver le bloc
correspondant à l'adresse à libérer et on indique que le bloc est libre. Ici,
il y a deux `assert` permettant de s'assurer que l'adresse est bien celle d'un
bloc et que le bloc n'a pas déjà été libéré.

!listing(c)
```
void liberation(void *adresse)
{
    struct bloc *premier_bloc = blocs_libres;
    struct bloc *bloc_courant = premier_bloc;

    while (bloc_courant->adresse != adresse)
    {
        bloc_courant = bloc_courant->suivant;
        // adresse invalide
        assert(bloc_courant != premier_bloc);
    }

    // pas de double libération
    assert(!bloc_courant->libre);

    // on libère l'adresse
    bloc_courant->libre = true;
}
```

Voici un premier programme de test de ces fonctions qui alloue 10 octets puis
effectue plusieurs allocations. L'allocation de `c` échoue car il n'y a plus
de place libre.

!listing(c .numberLines)
```
int main()
{
    creation_blocs_libres(10);

    uint8_t *a = allocation(5);
    uint8_t *b = allocation(3);
    uint8_t *c = allocation(3);
    liberation(b);
    uint8_t *d = allocation(2);

    printf("Allocation a:%p b:%p c:%p d:%p\n",
            (void *)a, (void *)b, (void *)c, (void *)d);

    destruction_blocs_libres();

    return 0;
}
```

Ce programme affiche alors 

!listing(c)
````
Allocation a:0x55c6c0c922a0 b:0x55c6c0c922a5 c:(nil) d:0x55c6c0c922a5
````

Voici l'évolution de la liste circulaire en présentant les maillons sous la
forme :

!tikz(maillon)
```
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white] (b1) {
\footnotesize adresse
  \nodepart{two} \footnotesize taille
  \nodepart{three} \footnotesize libre
  \nodepart{four} \footnotesize suivant
};

```

L'évolution de la liste des blocs entre les lignes 3 et 9 est alors :

* Ligne 3

!tikz(custom_malloc_evolution1)
```
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b1) {
\footnotesize 0x55c6c0c922a0
  \nodepart{two} \footnotesize 10
  \nodepart{three} \footnotesize true
  \nodepart{four} 
};

\draw[->] (b1.four) .. controls +(2,0) and +(2,0.5) .. (b1.east);
```

* Ligne 5

!tikz(custom_malloc_evolution2)
```
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b1) at (0,0) {
\footnotesize 0x55c6c0c922a0
  \nodepart{two} \footnotesize 5
  \nodepart{three} \footnotesize false
  \nodepart{four} 
};
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b2) at (3,0) {
\footnotesize 0x55c6c0c922a5
  \nodepart{two} \footnotesize 5
  \nodepart{three} \footnotesize true
  \nodepart{four} 
};
\draw (b1.four) edge[->] (b2);
\draw (b2.four) edge[->, bend left] (b1.south);

```

* Ligne 6

!tikz(custom_malloc_evolution3)
```
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b1) at (0,0) {
\footnotesize 0x55c6c0c922a0
  \nodepart{two} \footnotesize 5
  \nodepart{three} \footnotesize false
  \nodepart{four} 
};
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b2) at (3,0) {
\footnotesize 0x55c6c0c922a5
  \nodepart{two} \footnotesize 3
  \nodepart{three} \footnotesize false
  \nodepart{four} 
};
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b3) at (6,0) {
\footnotesize 0x55c6c0c922a8
  \nodepart{two} \footnotesize 2
  \nodepart{three} \footnotesize true
  \nodepart{four} 
};
\draw (b1.four) edge[->] (b2);
\draw (b2.four) edge[-> ] (b3);
\draw (b3.four) edge[->, bend left] (b1.south);
```

* Ligne 8

!tikz(custom_malloc_evolution4)
```
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b1) at (0,0) {
\footnotesize 0x55c6c0c922a0
  \nodepart{two} \footnotesize 5
  \nodepart{three} \footnotesize false
  \nodepart{four} 
};
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b2) at (3,0) {
\footnotesize 0x55c6c0c922a5
  \nodepart{two} \footnotesize 3
  \nodepart{three} \footnotesize true
  \nodepart{four} 
};
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b3) at (6,0) {
\footnotesize 0x55c6c0c922a8
  \nodepart{two} \footnotesize 2
  \nodepart{three} \footnotesize true
  \nodepart{four} 
};
\draw (b1.four) edge[->] (b2);
\draw (b2.four) edge[-> ] (b3);
\draw (b3.four) edge[->, bend left] (b1.south);
```

* Ligne 9

!tikz(custom_malloc_evolution5)
```
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b1) at (0,0) {
\footnotesize 0x55c6c0c922a0
  \nodepart{two} \footnotesize 5
  \nodepart{three} \footnotesize false
  \nodepart{four} 
};
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b2) at (2.5,0) {
\footnotesize 0x55c6c0c922a5
  \nodepart{two} \footnotesize 2
  \nodepart{three} \footnotesize true
  \nodepart{four} 
};
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b3) at (5,0) {
\footnotesize 0x55c6c0c922a7
  \nodepart{two} \footnotesize 1
  \nodepart{three} \footnotesize false
  \nodepart{four} 
};
\node[rectangle split, rectangle split parts=4, draw=red, fill=red!10!white]
(b4) at (7.5,0) {
\footnotesize 0x55c6c0c922a8
  \nodepart{two} \footnotesize 2
  \nodepart{three} \footnotesize false
  \nodepart{four} 
};
\draw (b1.four) edge[->] (b2);
\draw (b2.four) edge[-> ] (b3);
\draw (b3.four) edge[-> ] (b4);
\draw (b4.four) edge[->, bend left] (b1.south);
```



Cette méthode d'allocation a un défaut majeur : elle fragmente l'espace libre.
Dans le programme suivant, il sera impossible d'allouer `b` car la liste
circulaire contient deux blocs libres de 5 octets et non une bloc libre de 10
octets.

!listing(c)
```
creation_blocs_libres(10);

uint8_t *a = allocation(5)
liberation(a);
uint8_t *b = allocation(10);
```

!remarque(On peut observer ce phénomène sur le diagramme précédent à la ligne 8
où deux blocs contigus sont libres et pourraient être fusionnés en un unique
bloc de 5 octets.)

On peut éviter cela en effectuant une phase de coalescence des blocs libres à
la libération. En vertu de la nature de la liste circulaire, il est nécessaire
de fusionner un bloc libre avec les blocs suivants. En utilisant une liste
circulaire doublement chaînée, on pourrait également fusionner avec les blocs
précédents.

Pour cela on change la fonction `liberation` ainsi :

!listing(c)
```
void liberation(void *adresse)
{
    struct bloc *premier_bloc = blocs_libres;
    struct bloc *bloc_courant = premier_bloc;

    while (bloc_courant->adresse != adresse)
    {
        bloc_courant = bloc_courant->suivant;
        // adresse invalide
        assert(bloc_courant != premier_bloc);
    }

    // pas de double libération
    assert(!bloc_courant->libre);

    // on libère l'adresse
    bloc_courant->libre = true;

    premier_bloc = bloc_courant;
    bloc_courant = bloc_courant->suivant;

    while (bloc_courant != premier_bloc && bloc_courant->libre)
    {
        struct bloc* actuel = bloc_courant;
        premier_bloc->taille += bloc_courant->taille;
        bloc_courant = bloc_courant->suivant;
        free(actuel); // on libere le bloc inutile
    }

    premier_bloc->suivant = bloc_courant;
}
```

