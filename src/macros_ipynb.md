!define[1](widefigure)
~~~
`\begin{figure*}[tbp]`{=tex}

!1

`\end{figure*}`{=tex}
~~~

!define[1](center)
~~~
`<center><div class="ui image center">`{=html}!1`</div></center>`{=html}
~~~

!newcounter(marginnotecounter)

!define[1](marginnote)
~~~
`<span id="marginnote!marginnotecounter context" style="font-size: 0.4em">⮞<span class="ui marginnote" id="marginnote!marginnotecounter">`{=html}!1`</span></span>`{=html}!incrcounter(marginnotecounter)
~~~

!define[1](sidenote)
~~~
`<a href="#marginnote!marginnotecounter" id="marginnote!marginnotecounter context">!marginnotecounter<span class="ui marginnote" id="marginnote!marginnotecounter"><span style="text-decoration: underline">`{=html}!marginnotecounter`</span>`{=html} !1`</span></a>`{=html}!incrcounter(marginnotecounter)
~~~


!define[1](part)
~~~

# !1

~~~
!define[1](chapter)
~~~

## !1

~~~
!define[1](section)
~~~

### !1

~~~
!define[1](subsection)
~~~

#### !1

~~~
!define[1](subsubsection)
~~~

##### !1

~~~
!define[1](paragraph)
~~~

###### !1

~~~


!define[2](chapterimage)
~~~
## !2
`<div class="ui image fluid">
  <div class="ui inverted active dimmer">
  <div class="content">
    <h1 class="ui header" style="color:black; font-size: 4rem" >
    !2
    </h1>
  </div>
  </div>
  <img class="" src="assets/pics/!1">
</div>`{=html}
~~~

!define[1](definition)
~~~
`<div class="ui message"><div class="header">Définition</div>`{=html}
!1
`</div>`{=html}
~~~
!define[1](corollaire)
~~~
<div class="ui message olive">
<div class="header">Corollaire</div>
!1
</div>
~~~

!define[1](lemme)
~~~
<div class="ui message olive">
<div class="header">Lemme</div>
!1
</div>
~~~
!define[1](exercice)
~~~
<div class="ui message orange">
<div class="header">Exercice</div>
!1
</div>
~~~

!define[1](exemple)
~~~
<div class="ui message blue">
<div class="header">Exemple</div>
!1
</div>
~~~
!define[1](question)
~~~
**Question !counter(question_counter)**

!1

~~~

!define[1](theoreme)
~~~
<div class="ui message olive">
<div class="header">Théorème</div>
!1
</div>
~~~
!define[1](complement)
~~~
`<div class="ui styled accordion">
<div class="title">
    <i class="dropdown icon"></i>`{=html}
    Complément
`</div>
<div class="content">`{=html}
!1
`</div>
</div>`{=html}
~~~

!define[1](reponse)
~~~
`<div class="ui styled accordion">
<div class="title">
    <i class="dropdown icon"></i>`{=html}
    Réponse
`</div>
<div class="content">`{=html}
!1
`</div>
</div>`{=html}
~~~
!define[1](preuve)
~~~
!1
~~~

!define[6](langspec3)
```
`<div>
  <div class="ui top attached tabular menu code">
  <a class="active item" data-tab="!1-!callid">!1</a>
  <a class="item" data-tab="!2-!callid">!2</a>
  <a class="item" data-tab="!3-!callid">!3</a>
</div>
<div class="ui bottom attached active tab segment code" data-tab="!1-!callid">`{=html}
!4
`</div>
<div class="ui bottom attached tab segment code" data-tab="!2-!callid">`{=html}
!5
`</div>
<div class="ui bottom attached tab segment code" data-tab="!3-!callid">`{=html}
!6
`</div>
</div>`{=html}
```


!define[6](listingi3)
```
`<div>
  <div class="ui top attached tabular menu code">
  <a class="active item" data-tab="!1-!callid">!1</a>
  <a class="item" data-tab="!2-!callid">!2</a>
  <a class="item" data-tab="!3-!callid">!3</a>
</div>
<div class="ui bottom attached active tab segment code" data-tab="!1-!callid">`{=html}
`!4`
`</div>
<div class="ui bottom attached tab segment code" data-tab="!2-!callid">`{=html}
`!5`
`</div>
<div class="ui bottom attached tab segment code" data-tab="!3-!callid">`{=html}
`!6`
`</div>`{=html}
`</div>`{=html}
```

!define[6](listing3)
```
`<div>
  <div class="ui top attached tabular menu code">
  <a class="active item" data-tab="!1-!callid">!1</a>
  <a class="item" data-tab="!2-!callid">!2</a>
  <a class="item" data-tab="!3-!callid">!3</a>
</div>
<div class="ui bottom attached active tab segment code" data-tab="!1-!callid">`{=html}

~~~~ {.!1}
!4
~~~~

`</div>
<div class="ui bottom attached tab segment code" data-tab="!2-!callid">`{=html}

~~~~ {.!2}
!5
~~~~

`</div>
<div class="ui bottom attached tab segment code" data-tab="!3-!callid">`{=html}

~~~~ {.!3}
!6
~~~~

`</div>`{=html}
`</div>`{=html}
```

!define[3](listinglit)
```
!listing(!1)
~~~~~
!rawinclude[!2](!3)
~~~~~
```

!define[2](listing3l)
```
!listing3(ocaml)(c)(python)
~~~~
!rawinclude[!1](!2.ml)
~~~~
~~~~
!rawinclude[!1](!2.c)
~~~~
~~~~
!rawinclude[!1](!2.py)
~~~~
```

!define[4](listing2)
```
`<div class="ui top attached tabular menu code">
  <a class="active item" data-tab="!1">!1</a>
  <a class="item" data-tab="!2">!2</a>
</div>
<div class="ui bottom attached active tab segment code" data-tab="!1">`{=html}

~~~~ {.!1}
!3
~~~~

`</div>
<div class="ui bottom attached tab segment code" data-tab="!2">`{=html}

~~~~ {.!2}
!4
~~~~

`</div>`{=html}
```

!define[2](listing)
~~~~
``` code
!2
```
~~~~

!define[2][1](message)
~~~
`<div class="ui message !O1"><div class="header">`{=html}!1`</div>`{=html}

!2

`</div>`{=html}
~~~

!define[1](remarque)
~~~
!message[orange](Remarque)
```

!1

```
~~~

!define[1](note)
~~~
!message[blue](Note)(!1)
~~~

!define[2][1](twocolumns)
~~~
!if(!O1)
```
`<div class="ui internally celled grid"><div class="row"><div class="!numeral(!O1) wide column">`{=html}

!1

`</div><div class="!numeral(!eval(16 - !O1)) wide column">`{=html}

!2

`</div></div></div>`{=html}
```
```
`<div class="ui internally celled grid"><div class="row"><div class="eight wide column">`{=html}

!1

`</div><div class="eight wide column">`{=html}

!2

`</div></div></div>`{=html}
```
~~~

!define[2][1](twocolumnsnosep)
~~~
!if(!O1)
```
`<div class="ui internally celled grid"><div class="row"><div class="!numeral(!O1) wide column">`{=html}

!1

`</div><div class="!numeral(!eval(16 - !O1)) wide column">`{=html}

!2

`</div></div></div>`{=html}
```
```
`<div class="ui internally celled grid"><div class="row"><div class="eight wide column">`{=html}

!1

`</div><div class="eight wide column">`{=html}

!2

`</div></div></div>`{=html}
```
~~~

!define[3](algorithme)
~~~
<div class="ui message gray">
<div class="header">Algorithme - !1</div>
<div class="ui message gray">
<div class="header">Entrée(s)</div>

!2

</div>

!3

</div>
~~~

!define[3](probleme)
~~~
<div class="ui message gray">
<div class="header">Problème - !1</div>
<ul>
<li>Entrée(s) : 

!2 

</li>
<li>Sortie : 

!3 

</li>
</ul>
</div>
~~~


!define(clearpage)
~~~
~~~

!define[2](classe)
~~~
`<div class="ui label">
    !1 <div class="detail">!2</div>
</div>`{=html}
~~~

!define(OISup)
~~~
!classe(OI)(Sup)
~~~

!define(OISpe)
~~~
!classe(OI)(Spé)
~~~

!define(ITCSpe)
~~~
!classe(ITC)(Spé)
~~~

!define(ITCSup)
~~~
!classe(ITC)(Sup)
~~~

!define(MP2IS1)
~~~
!classe(MP2I)(S1)
~~~

!define(MP2IS2)
~~~
!classe(MP2I)(S2)
~~~

!define(MPI)
~~~
`<div class="ui label">
    MPI
</div>`{=html}
~~~


!define[1](image)
~~~
![](assets/pics/!1)
~~~

!define(and)(&)
!define[1](tikzre)
~~~
![](assets/pics/!1.png)
~~~

!define[2](box)
~~~
<div class="ui message !1">
!2
</div>
~~~

!define(extendgeometry)
```
```

!define(restoregeometry)
```
```

!define[1](huge)
```
!1
```
!define(noheaders)
```
```

!define(lightgreen)(green)
!define(lightred)(red)
!define(lightblue)(blue)
!define(lightorange)(orange)

!define[1](jupyter)
~~~~
!1
~~~~

