---
title: Informatique - Algorithmique - Algorithmique exacte
author: Marc de Falco
autoSectionLabels: true
---
!include(macros_html.md)
!include(algorithmique/analyse_algorithme.md)
