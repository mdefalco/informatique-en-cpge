---
title: Informatique
subtitle: CPGE
author: Marc de Falco
---

!include(macros.md)

!part(Introduction)
!include(introduction.md)
!include(introduction/introduction_informatique.md)

!part(Programmation)
!include(programmation/imperatif.md)
!include(programmation/recursivite.md)
!include(programmation/options_ocaml.md)
!include(programmation/exceptions_ocaml.md)

!part(Structures de données)
!include(structuresdonnees/structurededonnees.md)
!include(structuresdonnees/sequences.md)
!include(structuresdonnees/pilesfiles.md)
!include(structuresdonnees/arbres.md)
!include(structuresdonnees/graphes.md)

!part(Algorithmique)
!include(algorithmique/analyse_algorithme.md)
!include(algorithmique/complexite_amortie.md)
!include(algorithmique/force_brute.md)
!include(algorithmique/gloutons.md)
!include(algorithmique/diviser_pour_regner.md)
!include(algorithmique/programmation_dynamique.md)
!include(algorithmique/algorithmique_du_texte.md)

!part(Logique)
!include(logique/calculprop.md)

!part(Systèmes)
!include(systeme/memoire.md)

!part(Bases de données)
!include(bdd/bdd.md)

!part(Langages)
!include(langages/langages_reguliers_automates.md)
!include(langages/langages_reguliers_automates_exos.md)
!include(langages/langages_reguliers_automates_fiche.md)

!part(Travaux Pratiques)
!include(tps/graphes.md)

!part(Problèmes)
!include(problemes/d_and_diagrams.md)
