!tikz(logo)
```

\tikzset{
    logo/.pic={
    \begin{scope}[shift={(-2,-5)}]
    \fill[pic actions] (0,4) -- (2,4) -- (2,3) -- (3,3) -- (3,6)
    -- (2,6) -- (2,5) -- (0,5) -- cycle;
    \fill[white,opacity=0.5]
        (0,4) -- +(0.1,0.1) -- +(0.1,0.9) -- +(0.9,0.9) -- +(1,1) -- +(0,1) -- +(0,0)
        (1,4) -- +(0.1,0.1) -- +(0.1,0.9) -- +(0.9,0.9) -- +(1,1) -- +(0,1) -- +(0,0)
        (2,4) -- +(0.1,0.1) -- +(0.1,0.9) -- +(0.9,0.9) -- +(1,1) -- +(0,1) -- +(0,0)
        (2,5) -- +(0.1,0.1) -- +(0.1,0.9) -- +(0.9,0.9) -- +(1,1) -- +(0,1) -- +(0,0)
        (2,3) -- +(0.1,0.1) -- +(0.1,0.9) -- +(0.9,0.9) -- +(1,1) -- +(0,1) -- +(0,0)
        ;
    \fill[black,opacity=0.2]
        (0,4) -- +(0.1,0.1) -- +(0.9,0.1) -- +(0.9,0.9) -- +(1,1) -- +(1,0) -- +(0,0)
        (1,4) -- +(0.1,0.1) -- +(0.9,0.1) -- +(0.9,0.9) -- +(1,1) -- +(1,0) -- +(0,0)
        (2,4) -- +(0.1,0.1) -- +(0.9,0.1) -- +(0.9,0.9) -- +(1,1) -- +(1,0) -- +(0,0)
        (2,5) -- +(0.1,0.1) -- +(0.9,0.1) -- +(0.9,0.9) -- +(1,1) -- +(1,0) -- +(0,0)
        (2,3) -- +(0.1,0.1) -- +(0.9,0.1) -- +(0.9,0.9) -- +(1,1) -- +(1,0) -- +(0,0)
        ;
    \draw[very thin]
        (0,4) -- +(0.1,0.1) rectangle +(0.9,0.9) -- +(1,1) +(0,1) -- +(0.1,0.9) +(1,0) -- +(0.9,0.1)
        (1,4) -- +(0.1,0.1) rectangle +(0.9,0.9) -- +(1,1) +(0,1) -- +(0.1,0.9) +(1,0) -- +(0.9,0.1)
        (2,4) -- +(0.1,0.1) rectangle +(0.9,0.9) -- +(1,1) +(0,1) -- +(0.1,0.9) +(1,0) -- +(0.9,0.1)
        (2,5) -- +(0.1,0.1) rectangle +(0.9,0.9) -- +(1,1) +(0,1) -- +(0.1,0.9) +(1,0) -- +(0.9,0.1)
        (2,3) -- +(0.1,0.1) rectangle +(0.9,0.9) -- +(1,1) +(0,1) -- +(0.1,0.9) +(1,0) -- +(0.9,0.1)
        ;
    \draw[thin]
    (1,4) -- (1,5) (2,4) -- (2,5) (2,5) -- (3,5) (2,4) --(3,4);
    \draw[thick] (0,4) -- (2,4) -- (2,3) -- (3,3) -- (3,6)
    -- (2,6) -- (2,5) -- (0,5) -- cycle;
    \node[black,opacity=0.3,centered] at (0.52,4.47) {\LARGE M};
    \node[centered] at (0.5,4.5) {\LARGE M};
    \node[black,opacity=0.3,centered] at (1.52,4.47) {\LARGE P};
    \node[centered] at (1.5,4.5) {\LARGE P};
    \node[black,opacity=0.3,centered] at (2.52,4.47) {\LARGE I};
    \node[centered] at (2.5,4.5) {\LARGE I};
    \node[black,opacity=0.3,centered] at (2.52,5.47) {\LARGE C};
    \node[centered] at (2.5,5.5) {\LARGE C};
    \node[black,opacity=0.3,centered] at (2.52,3.47) {\LARGE V};
    \node[centered] at (2.5,3.5) {\LARGE V};
    \end{scope}
    },
    logo90/.pic={
        \pic[red,fill=red!20,rotate=90,transform shape] at (-1,0) {logo};
    },
    logo180/.pic={
        \pic[blue,fill=blue!20,rotate=180,transform shape] at (-1,-1) {logo};
    },
    logo270/.pic={
        \pic[green,fill=green!20,rotate=270,transform shape] at (0, -1) {logo};
    },
    logo0/.pic={
        \pic[black,fill=white,transform shape] {logo};
    }
}


\clip (-2.5,-.5) rectangle (1.5,3.5);
%\pic[green,fill=green!20,rotate=180,transform shape] at (-2,-2) {logo};
%\pic[orange,fill=orange!20,rotate=90,transform shape] at (-1,-3) {logo};
%\pic[green,fill=green!20,rotate=-90,transform shape] at (1,2) {logo};
%\pic[red,fill=red!20,rotate=180,transform shape] at (2,-1) {logo};
%\pic[green,fill=green!20,rotate=0,transform shape] at (3,-2) {logo};
%\pic[orange,fill=orange!20,rotate=-90,transform shape] at (-1,1) {logo};
%\pic[red,fill=red!20,transform shape] at (-3,3) {logo};
\pic at (2,4) {logo90};
\pic at (3,3) {logo270};
\pic at (3,0) {logo90};
\pic at (1,0) {logo180};
\pic at (0,5) {logo270};
\pic at (-1,3) {logo180};
\pic at (-2,0) {logo90};
\pic at (-1,-1) {logo270};

\pic at (0,2) {logo0};
```
