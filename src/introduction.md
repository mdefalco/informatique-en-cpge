!chapter(Introduction)

!section(Présentation du site)
Ce site présente mon poly de cours autour du programme de MP2I/MPI, et
donc également du programme d'option informatique de MPSI/MP et du tronc
commun.

La progression sur les deux années n'est pas indiquée mais les chapitres
suivent les programmes.

On pourra trouver des exercices et des TP complets au sein des chapitres.

!section(Mode de production des documents)

!tikz(intro_present)
```
\node[rectangle,rounded corners,blue,fill=blue!10] (pp) at (0,0) {Préprocesseur};
\node[rectangle,fill=gray!10] (doc) at (-3,0) {\scriptsize document.md};
\node[rectangle,fill=gray!10] (dochtml) at (3,1) {\scriptsize document.html.md};
\node[rectangle,fill=gray!10] (doctex) at (3,0) {\scriptsize document.tex.md};
\node[rectangle,fill=gray!10] (docnb) at (3,-1) {\scriptsize document.nb.md};
\node[rectangle,rounded corners,blue,fill=blue!10] (pandoc) at (5,0) {pandoc};
\node[rectangle,fill=gray!10] (html) at (7,1) {\scriptsize document.html};
\node[rectangle,fill=gray!10] (tex) at (7,0) {\scriptsize document.pdf};
\node[rectangle,fill=gray!10] (nb) at (7,-1) {\scriptsize document.ipynb};

\draw[->,thick,red]
    (doc) edge (pp)
    (pp) edge (dochtml)
    (pp) edge (doctex)
    (pp) edge (docnb)
    (dochtml) edge (pandoc)
    (doctex) edge (pandoc)
    (docnb) edge (pandoc)
    (pandoc) edge (html)
    (pandoc) edge (tex)
    (pandoc) edge (nb)
    ;

```
