---
title: Informatique - Algorithmique - Algorithmique du texte
author: Marc de Falco
autoSectionLabels: true
---
!include(macros_html.md)

!include(algorithmique/algorithmique_du_texte.md)
