#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "d_and_d.h"

board map_to_board(map m)
{
}

void board_to_map(board b, map m)
{
}

int popcount_slow(uint64_t v)
{
}

int popcount(uint64_t v)
{
}

void print_board(problem *p, board b)
{
}

int enum_popcount_byte[256];

uint64_t bit(int pos)
{
    return (uint64_t) 1 << pos;
}

uint64_t mask_column(int i)
{
}

bool test_columns_overload(problem *p, board b)
{
}

bool test_columns_underload(problem *p, board b)
{
}

bool empty(problem *p, board b, int pos)
{
}

bool treasure(problem *p, int pos)
{
}

bool monster(problem *p, int pos)
{
}

uint64_t neighbours(int pos)
{
}

int count_non_wall(problem *p, board b, int pos)
{
}

bool test_no_deadend_upto_row(problem *p, board b, int row)
{
}

bool test_trapped_monster(problem *p, board b)
{
}

bool test_deadends(problem *p, board b)
{
}

bool test_empty_squares(problem *p, board b)
{
}

bool test_treasure_rooms(problem *p, board *b)
{
}

void solve(problem *p, int row, board b)
{
    // Unsolvable from this board
    if (test_columns_overload(p, b))
        return;
    if (test_no_deadend_upto_row(p, b, row))
        return;
    if (test_trapped_monster(p, b))
        return;

    // Board is filled
    if(row == 8)
    {
        // Validate columns count
        if (test_columns_underload(p, b))
            return;

        // Full board with monsters
        board fb = b | p->monsters;

        if (test_treasure_rooms(p, &fb))
            return;
        // Every treasure room is now filled in fb

        if (test_empty_squares(p, fb))
            return;

        printf("Solution %lx\n", b); 
        print_board(p, b);

        return;
    }

    for(int i = 0; i < 256; i++)
    {
        if (enum_popcount_byte[i] == p->rows[row])
        {
            uint64_t move = (uint64_t)i << (8*row);

            // ensure there's no overlap with walls/monsters/treasures
            if ((move & (p->monsters | p->treasures)) != 0)
                continue;

            solve(p, row+1, b + move);
        }
    }
}

int main(void)
{
    // the 64 problems found in Last Call BBS
    problem *problems[] = {
        &p00, &p01, &p02, &p03, &p04, &p05, &p06, &p07,
        &p08, &p09, &p10, &p11, &p12, &p13, &p14, &p15,
        &p16, &p17, &p18, &p19, &p20, &p21, &p22, &p23,
        &p24, &p25, &p26, &p27, &p28, &p29, &p30, &p31,
        &p32, &p33, &p34, &p35, &p36, &p37, &p38, &p39,
        &p40, &p41, &p42, &p43, &p44, &p45, &p46, &p47,
        &p48, &p49, &p50, &p51, &p52, &p53, &p54, &p55,
        &p56, &p57, &p58, &p59, &p60, &p61, &p62, &p63
    };

    for (int i = 0; i < 64; i++)
    {
        printf("Solving problem %d\n", i);
        solve(problems[i], 0, 0);
    }
}
