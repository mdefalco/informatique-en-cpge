#include <stdint.h>
typedef uint64_t board;
typedef bool map[64];
typedef char constraint[8];

struct problem {
    board monsters;
    board treasures;
    constraint rows;
    constraint columns;
};
typedef struct problem problem;

problem p00 = {
	.monsters = 0x8000800080048000,
	.treasures = 0x20000000000,
	.rows = { 3,2,5,3,4,1,4,4 },
	.columns = { 1,4,2,7,0,4,4,4 }
};
problem p01 = {
	.monsters = 0x1100800200000040,
	.treasures = 0x20000,
	.rows = { 4,4,4,4,3,4,2,6 },
	.columns = { 6,2,4,1,5,4,4,5 }
};
problem p02 = {
	.monsters = 0xa801000000040000,
	.treasures = 0x400000,
	.rows = { 5,2,2,1,5,3,2,5 },
	.columns = { 4,2,5,0,6,2,4,2 }
};
problem p03 = {
	.monsters = 0x2800018001800014,
	.treasures = 0x0,
	.rows = { 6,2,5,3,2,5,2,6 },
	.columns = { 6,2,4,3,4,4,2,6 }
};
problem p04 = {
	.monsters = 0x8100400000820081,
	.treasures = 0x0,
	.rows = { 0,7,2,4,2,2,7,0 },
	.columns = { 2,4,4,3,2,3,4,2 }
};
problem p05 = {
	.monsters = 0x90000010000a001,
	.treasures = 0x2000000000,
	.rows = { 2,3,5,4,1,4,5,5 },
	.columns = { 2,3,3,4,4,3,4,6 }
};
problem p06 = {
	.monsters = 0x4000102400000a0,
	.treasures = 0x10000000020000,
	.rows = { 3,1,2,6,6,1,4,3 },
	.columns = { 4,1,3,4,3,3,2,6 }
};
problem p07 = {
	.monsters = 0x2011000000000040,
	.treasures = 0x40000,
	.rows = { 1,3,3,6,0,6,3,4 },
	.columns = { 1,5,1,4,3,5,4,3 }
};
problem p08 = {
	.monsters = 0x8000800000000005,
	.treasures = 0x200000,
	.rows = { 6,3,1,3,6,1,6,0 },
	.columns = { 0,6,1,5,3,2,3,6 }
};
problem p09 = {
	.monsters = 0x205000000800200,
	.treasures = 0x20000000000000,
	.rows = { 4,3,5,3,4,4,2,7 },
	.columns = { 4,2,6,5,2,4,4,5 }
};
problem p10 = {
	.monsters = 0x4000000000,
	.treasures = 0x4000000002000,
	.rows = { 2,1,4,4,5,1,4,0 },
	.columns = { 1,3,2,4,4,3,2,2 }
};
problem p11 = {
	.monsters = 0x1104200400040,
	.treasures = 0x80000,
	.rows = { 6,4,3,4,4,4,4,1 },
	.columns = { 6,6,2,2,3,7,3,1 }
};
problem p12 = {
	.monsters = 0x4010000a1000040,
	.treasures = 0x400,
	.rows = { 4,4,4,3,4,3,1,4 },
	.columns = { 6,1,2,2,7,4,1,4 }
};
problem p13 = {
	.monsters = 0x8000048000000100,
	.treasures = 0x1000,
	.rows = { 2,3,4,3,4,3,3,2 },
	.columns = { 1,5,2,5,0,4,2,5 }
};
problem p14 = {
	.monsters = 0xa100001000200020,
	.treasures = 0x4,
	.rows = { 3,4,2,6,4,4,1,4 },
	.columns = { 6,4,1,4,4,4,2,3 }
};
problem p15 = {
	.monsters = 0x0,
	.treasures = 0x42000000004200,
	.rows = { 2,2,1,5,7,0,2,2 },
	.columns = { 2,2,1,6,4,2,2,2 }
};
problem p16 = {
	.monsters = 0x2a00140000000000,
	.treasures = 0x200400,
	.rows = { 5,2,1,4,6,4,1,5 },
	.columns = { 3,3,3,3,6,3,2,5 }
};
problem p17 = {
	.monsters = 0x44aa000000000000,
	.treasures = 0x4400,
	.rows = { 2,2,1,6,1,6,2,6 },
	.columns = { 5,3,1,3,7,2,1,4 }
};
problem p18 = {
	.monsters = 0x8400010000000020,
	.treasures = 0x20000,
	.rows = { 7,3,4,1,7,1,6,3 },
	.columns = { 4,4,2,6,2,3,4,7 }
};
problem p19 = {
	.monsters = 0xa00a000000a00114,
	.treasures = 0x0,
	.rows = { 5,6,4,3,2,3,5,5 },
	.columns = { 5,4,6,1,6,3,5,3 }
};
problem p20 = {
	.monsters = 0x8000008001000081,
	.treasures = 0x4000000000000,
	.rows = { 6,0,4,1,5,3,3,4 },
	.columns = { 4,4,1,3,6,3,4,1 }
};
problem p21 = {
	.monsters = 0xa00008000000000,
	.treasures = 0x2800,
	.rows = { 5,2,2,3,6,0,6,1 },
	.columns = { 5,3,4,3,5,1,3,1 }
};
problem p22 = {
	.monsters = 0x8000000000,
	.treasures = 0x1000000200200,
	.rows = { 2,2,2,5,5,2,1,2 },
	.columns = { 2,2,1,6,2,1,3,4 }
};
problem p23 = {
	.monsters = 0x441004200800240,
	.treasures = 0x0,
	.rows = { 4,4,4,6,4,4,5,3 },
	.columns = { 6,4,5,2,3,6,1,7 }
};
problem p24 = {
	.monsters = 0x80000,
	.treasures = 0x24000000000000,
	.rows = { 3,3,5,1,6,1,2,2 },
	.columns = { 2,2,3,3,7,1,3,2 }
};
problem p25 = {
	.monsters = 0xa000020000000,
	.treasures = 0x40000000000000,
	.rows = { 0,6,2,3,5,2,2,5 },
	.columns = { 1,5,3,4,6,2,4,0 }
};
problem p26 = {
	.monsters = 0x800020080001000,
	.treasures = 0x20000001000000,
	.rows = { 5,2,2,2,6,1,4,3 },
	.columns = { 5,4,1,6,4,1,3,1 }
};
problem p27 = {
	.monsters = 0x8000020000002000,
	.treasures = 0x8000000,
	.rows = { 1,4,3,2,3,2,4,1 },
	.columns = { 1,4,3,1,3,3,4,1 }
};
problem p28 = {
	.monsters = 0x8000800000000100,
	.treasures = 0x2000,
	.rows = { 2,3,4,3,7,0,4,0 },
	.columns = { 1,4,1,5,1,3,2,6 }
};
problem p29 = {
	.monsters = 0x800000000800822,
	.treasures = 0x800000000000,
	.rows = { 5,5,1,5,2,2,3,5 },
	.columns = { 2,2,6,2,6,2,4,4 }
};
problem p30 = {
	.monsters = 0x180004000000200,
	.treasures = 0x4000,
	.rows = { 5,4,1,7,3,4,4,2 },
	.columns = { 3,3,6,3,3,3,2,7 }
};
problem p31 = {
	.monsters = 0x8020800090000080,
	.treasures = 0x400000100,
	.rows = { 1,2,4,5,2,3,3,6 },
	.columns = { 5,2,2,4,5,4,1,3 }
};
problem p32 = {
	.monsters = 0x10001000,
	.treasures = 0x100,
	.rows = { 5,2,4,1,6,2,4,2 },
	.columns = { 3,2,4,5,4,3,4,1 }
};
problem p33 = {
	.monsters = 0x20000000000000,
	.treasures = 0x4000000,
	.rows = { 0,6,2,3,2,5,3,1 },
	.columns = { 1,5,3,1,3,5,3,1 }
};
problem p34 = {
	.monsters = 0x4200801000800042,
	.treasures = 0x0,
	.rows = { 3,6,0,5,4,0,6,3 },
	.columns = { 4,2,4,2,3,4,2,6 }
};
problem p35 = {
	.monsters = 0x80010008,
	.treasures = 0x80000000000000,
	.rows = { 1,4,2,6,2,3,3,1 },
	.columns = { 1,5,2,2,6,1,4,1 }
};
problem p36 = {
	.monsters = 0x802080208020,
	.treasures = 0x40000,
	.rows = { 6,3,3,3,5,3,4,3 },
	.columns = { 5,3,2,4,6,4,1,5 }
};
problem p37 = {
	.monsters = 0x4010020000001540,
	.treasures = 0x0,
	.rows = { 6,4,1,6,2,3,6,6 },
	.columns = { 6,5,4,6,4,5,3,1 }
};
problem p38 = {
	.monsters = 0x980000000000090,
	.treasures = 0x40000,
	.rows = { 6,2,3,3,3,4,4,5 },
	.columns = { 4,1,4,5,2,7,4,3 }
};
problem p39 = {
	.monsters = 0xa200800180010051,
	.treasures = 0x0,
	.rows = { 4,4,2,4,6,2,3,4 },
	.columns = { 3,5,3,4,4,2,5,3 }
};
problem p40 = {
	.monsters = 0xa500000000000002,
	.treasures = 0x200000,
	.rows = { 4,4,4,4,3,2,5,3 },
	.columns = { 4,1,4,7,3,1,4,5 }
};
problem p41 = {
	.monsters = 0x94000100000002a0,
	.treasures = 0x0,
	.rows = { 5,6,2,6,1,5,3,5 },
	.columns = { 4,5,4,4,4,5,2,5 }
};
problem p42 = {
	.monsters = 0x9000000800000042,
	.treasures = 0x2000000000,
	.rows = { 3,4,3,3,1,5,1,3 },
	.columns = { 1,4,1,4,5,2,3,3 }
};
problem p43 = {
	.monsters = 0x0,
	.treasures = 0x200000,
	.rows = { 5,3,3,2,5,0,4,1 },
	.columns = { 2,3,1,5,3,1,3,5 }
};
problem p44 = {
	.monsters = 0x8008000080008000,
	.treasures = 0x4000000,
	.rows = { 1,5,3,2,2,7,2,2 },
	.columns = { 1,5,3,2,3,4,2,4 }
};
problem p45 = {
	.monsters = 0x8400001a4008200,
	.treasures = 0x0,
	.rows = { 5,2,7,4,2,4,4,6 },
	.columns = { 5,6,3,2,5,5,3,5 }
};
problem p46 = {
	.monsters = 0x5200128000000000,
	.treasures = 0x20000,
	.rows = { 5,2,2,1,5,3,6,3 },
	.columns = { 5,3,3,3,3,5,1,4 }
};
problem p47 = {
	.monsters = 0x0,
	.treasures = 0x8000000,
	.rows = { 5,1,5,2,3,2,6,0 },
	.columns = { 1,5,3,3,3,5,2,2 }
};
problem p48 = {
	.monsters = 0x4180000100000120,
	.treasures = 0x20000000,
	.rows = { 5,6,1,4,1,5,3,4 },
	.columns = { 4,4,4,2,6,2,3,4 }
};
problem p49 = {
	.monsters = 0x2180020021000002,
	.treasures = 0x0,
	.rows = { 6,4,1,4,2,4,6,2 },
	.columns = { 5,4,2,4,2,4,5,3 }
};
problem p50 = {
	.monsters = 0x8100000000000081,
	.treasures = 0x0,
	.rows = { 5,2,6,2,3,1,6,3 },
	.columns = { 3,5,3,6,2,3,4,2 }
};
problem p51 = {
	.monsters = 0x1000000000,
	.treasures = 0x2000,
	.rows = { 2,2,3,4,3,5,3,1 },
	.columns = { 2,2,6,1,6,3,2,1 }
};
problem p52 = {
	.monsters = 0x2802800080008020,
	.treasures = 0x0,
	.rows = { 4,1,5,2,4,5,2,6 },
	.columns = { 5,3,4,2,4,4,2,5 }
};
problem p53 = {
	.monsters = 0x5400000004001082,
	.treasures = 0x0,
	.rows = { 5,2,6,2,3,5,1,5 },
	.columns = { 3,4,4,4,4,5,2,3 }
};
problem p54 = {
	.monsters = 0x9000000000002000,
	.treasures = 0x10000020000,
	.rows = { 2,3,4,2,5,1,2,2 },
	.columns = { 2,2,1,6,3,3,3,1 }
};
problem p55 = {
	.monsters = 0x80048000,
	.treasures = 0x8000000000000,
	.rows = { 5,1,4,1,6,2,3,2 },
	.columns = { 5,2,1,2,5,4,2,3 }
};
problem p56 = {
	.monsters = 0x1000001800000a0,
	.treasures = 0x2000000000000400,
	.rows = { 3,3,1,4,5,2,2,2 },
	.columns = { 2,3,1,4,6,1,4,1 }
};
problem p57 = {
	.monsters = 0x80008040010020,
	.treasures = 0x40000000000,
	.rows = { 3,4,3,6,3,1,3,5 },
	.columns = { 2,4,2,5,2,5,3,5 }
};
problem p58 = {
	.monsters = 0x2081000001002089,
	.treasures = 0x0,
	.rows = { 4,2,5,1,4,3,4,2 },
	.columns = { 2,5,2,3,3,3,4,3 }
};
problem p59 = {
	.monsters = 0x240000240000,
	.treasures = 0x0,
	.rows = { 3,3,4,4,7,3,2,4 },
	.columns = { 2,6,4,5,5,3,4,1 }
};
problem p60 = {
	.monsters = 0x5800000010000,
	.treasures = 0x20000000,
	.rows = { 5,2,2,2,7,1,4,5 },
	.columns = { 5,4,4,2,4,3,2,4 }
};
problem p61 = {
	.monsters = 0x2000200800080,
	.treasures = 0x20000,
	.rows = { 1,4,2,4,5,6,3,5 },
	.columns = { 5,3,1,6,4,3,3,5 }
};
problem p62 = {
	.monsters = 0x50000020000a2,
	.treasures = 0x40000000000000,
	.rows = { 3,3,5,1,6,1,3,5 },
	.columns = { 6,5,3,4,3,1,3,2 }
};
problem p63 = {
	.monsters = 0x2000000000000000,
	.treasures = 0x0,
	.rows = { 1,4,3,2,4,5,3,3 },
	.columns = { 1,3,6,2,4,2,3,4 }
};
