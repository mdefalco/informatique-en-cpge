#include <stdio.h>
#include <string.h>
#include <assert.h>
//IGNORE ABOVE
/* recherche_naive(m,s) recherche le motif m dans la chaine
 * s et renvoie l'indice de la première occurrence s'il est présent
 * ou -1 sinon */
int recherche_naive(const char *m, const char *s)
{
    int n = strlen(s);
    int p = strlen(m);

    for (int i = 0; i <= n-p; i++)
    {
        int j;
        for (j = 0; j < p; j++)
        {
            if (s[i+j] != m[j])
                break;
        }
        if (j == p)
            return i;
    }

    return -1;
}
// IGNORE BELOW
int main(int argc, char **argv)
{
    assert(recherche_naive("toto", "totitotota") == 4);
    assert(recherche_naive("tata", "totitotota") == -1);
    assert(recherche_naive("aaab", "aaaab") == 1);

    return 0;
}
