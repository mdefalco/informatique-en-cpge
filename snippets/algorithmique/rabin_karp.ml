let fastmod2 a =
    let q = 0x7fffffff in
    let r1 = a land q in
    let r2 = (a lsr 31) land q in
    let s = (r1 + r2) land q in
    s

let rec fastmod a =
    let s = ref 0 in
    let x = ref a in
    let q = 0x7fffffff in
    while !x > 0 do
        s := !s + (!x land q);
        x := !x lsr 31
    done;
    if !s > q
    then fastmod !s
    else if !s = q
    then 0
    else !s

let rec pow r k q = 
    if k = 0 
    then 1 
    else (r * pow r (k-1) q) mod q

let hash r q s =
    let p = ref 1 in
    let e = ref 0 in
    for i = String.length s - 1 downto 0 do
        e := (!p * (Char.code s.[i]) + !e) mod q;
        p := (r * !p) mod q
    done;
    if !e < 0
    then !e + q
    else !e

let delta r q rp a b e = 
    let ep = 
        (r * (e - rp * Char.code a) + Char.code b) mod q
    in if ep < 0 then ep+q else ep

exception Trouve of int

let rabin_karp m s =
    let n = String.length s in
    let p = String.length m in
    let r = 256 in
    let q = 0x7fffffff in (* 2^(31)-1 *)
    let rp = pow r (p-1) q in
    let me = hash r q m in
    let e = ref (hash r q (String.sub s 0 p)) in
    try
        for i = 0 to n-p+1 do
            if me = !e && m = String.sub s i p
            then raise (Trouve i);
            if i+p < n then e := delta r q rp s.[i] s.[i+p] !e
        done; None
    with Trouve k -> Some k

let _ =
    let r = 256 in
    let s = "test" in
    let q = 2147483647 in

    Printf.printf "%s %d\n" 
        s (hash r q s);

    let n = 349384938438 in
    Printf.printf "%d %d %d\n" 
        (n mod q) (fastmod n) (fastmod2 n);

    match rabin_karp "toto" "ttotatitotutototati"
    with Some k -> Printf.printf "%d\n" k
    | None -> Printf.printf "choux blanc\n"
