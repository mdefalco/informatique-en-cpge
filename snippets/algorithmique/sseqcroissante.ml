
let t = [| 1; 0; 2; 3; 1; 5; 3; 2 |]

(* BEGIN REC *)
let rec croissante t i =
    let n = Array.length t in
    if i = n-1
    then 1
    else let m = ref 1 in
        for j = i+1 to n-1 do
            if t.(j) > t.(i)
            then m := max !m (1 + croissante t j)
        done;
        !m
(* END REC *)

(* BEGIN RECMEMO *)
let croissante t =
    let n = Array.length t in
    let cache = Hashtbl.create n in
    let rec aux i =
        try
            Hashtbl.find cache i
        with Not_found ->
            let v = if i = n-1
                    then 1
                    else let m = ref 1 in
                        for j = i+1 to n-1 do
                            if t.(j) > t.(i)
                            then m := max !m (1 + aux j)
                        done;
                    !m
            in Hashtbl.add cache i v; v
    in aux
(* END RECMEMO *)

(* BEGIN TAB *)
let croissante t =
    let n = Array.length t in
    let f = Array.make n 1 in
    for i = n-2 downto 0 do
        for j = i+1 to n-1 do
            if t.(j) > t.(i)
            then f.(i) <- 1 + f.(j)
        done
    done;
    f
(* END TAB *)

(* BEGIN TABREC *)
let croissante t =
    let n = Array.length t in
    let f = Array.make n 1 in
    let suivant = Array.make n None in
    for i = n-2 downto 0 do
        for j = i+1 to n-1 do
            if t.(j) > t.(i) && f.(i) < 1 + f.(j)
            then begin
                f.(i) <- 1 + f.(j);
                suivant.(i) <- Some j
            end
        done;
    done;
    f, suivant

let sous_sequence_croissante t =
    let f, suivant = croissante t in
    let n = Array.length t in
    let mi = ref 0 in
    for i = 1 to n-1 do
        if f.(i) > f.(!mi)
        then mi := i
    done;
    let seq = ref [ !mi ] in
    while suivant.(!mi) <> None do
        mi := Option.get suivant.(!mi);
        seq := !mi :: !seq
    done;
    List.rev !seq
(* END TABREC *)


let _ = 
    for i = 0 to Array.length t - 1 do
        Printf.printf "%d-%d\n" i (fst (croissante t)).(i)
    done;
    List.iter print_int (sous_sequence_croissante t);
    print_newline();
