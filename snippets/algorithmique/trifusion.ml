let rec separe_en_deux l =
    match l with
    | [] -> ([], [])
    | [x] -> ([x], [])
    | x::y::q -> let l1, l2 = separe_en_deux q in
        (x::l1, y::l2)

let rec fusionne l1 l2 =
    match l1, l2 with
    | [], _ -> l2
    | _, [] -> l1
    | x::q1, y::q2 ->
        if x < y
        then x :: (fusionne q1 l2)
        else y :: (fusionne l1 q2)

let rec tri_fusion l =
    match l with
    | [] -> []
    | [x] -> [x]
    | _ -> 
        let l1, l2 = separe_en_deux l in
        let l1p = tri_fusion l1 in
        let l2p = tri_fusion l2 in
        fusionne l1p l2p

