from bitpacking import Bitpacking

# BEGIN INTBITS
def entier_vers_bits(n, longueur_code):
    assert(n < 1 << longueur_code)
    bits = []
    for i in range(longueur_code):
        bits.append( n % 2 == 1 )
        n //= 2
    return bits

def bits_vers_entier(l):
    acc = 0
    for x in reversed(l):
        acc = 2 * acc + (1 if x else 0)
    return acc
# END INTBITS

def compresse(nom_in, nom_out, longueur_code=12):
    table = [ bytes([c]) for c in range(256) ]
    l = []
    orig = open(nom_in, 'r').read()
    nsteps = 0

    def step(m, c, i, j):
        nonlocal nsteps

        nsteps += 1
        print(f'!tikz[framed](lzw_comp_ex{nsteps})')
        print('```')
        print('!textmatrix\n(m) {')
        print('&'.join(map(str,range(len(orig)))) + '\\\\')
        print('&'.join(list(orig)) + '\\\\')
        print('};')
        print(f'\\draw[red,rounded corners] (m-2-{i+1}.north west) rectangle' \
                + 'node[below=.25cm] {m}' + f'(m-2-{j}.south east);')
        print('\\node[align=center,draw,rectangle,right=1cm,text width=1cm,minimum height=.5cm] (mm) at (m.east) {' + \
                f'{m.decode("ascii")}' + '};')
        print('\\node[below] (mml) at (mm.south) {m};')

        print('\\node[draw,align=center,rectangle,right,text width=.25cm,minimum height=.5cm] (c) at (mm.east) {' + \
                f'{c.decode("ascii")}' + '};')
        print('\\node[below] (cl) at (c.south) {x};')
    
        print('\\node[draw,align=center,rectangle,right,text width=.5cm,minimum height=.5cm] (p) at (c.east) {' + \
                f'{table.index(m)}' + '};')
        print('\\node[below] (pl) at (p.south) {\\emph{sortie}};')


        if c != b'':
            print(f'\\node[below] (cc) at (m-2-{j+1}.south) '+'{x};')


        print('\\matrix[below=.6cm,matrix of nodes,nodes in empty cells,nodes={anchor=center,align=center,text width=1cm,minimum height=.5cm},row 1/.style={nodes={text=white,fill=black}},row 2/.style={nodes={fill=gray!20}}]')
        print('(mt) at (m.south) {')
        print('\dots &' + '&'.join(map(str,range(256,len(table)))) + '\\\\')
        print('\dots & ' + '&'.join([ t.decode('ascii') for t in table[256:]]) + '\\\\')
        print('};')
        print('\\node[above] at (m.north) { \\emph{entrée} };')
        print('\\node[above] at (mt.north) { \\emph{table} };')

        print('```\n')

    with open(nom_in, 'rb') as fin, \
            Bitpacking(nom_out, 'wb') as fout:
        i = j = 0
        m = fin.read(1)
        while True:
            c = fin.read(1)
            j += 1
            if c == b'': break
            if m+c in table:
                m = m+c
            else:
                l.append(table.index(m))
                fout.write_code(table.index(m), longueur_code)
                step(m,c,i,j)
                if len(table) < 1 << longueur_code:
                    table.append(m+c)
                m = c
                i = j
        # code final
        step(m,b'',i,j)
        l.append(table.index(m))
        fout.write_code(table.index(m), longueur_code)

def read_code(f, longueur_code=12):
    bits = f.read_bits(longueur_code)
    if bits == []:
        return None
    code = bits_vers_entier(bits)
    return code

def decompresse(nom_in, nom_out, longueur_code=12):
    table = [ bytes([c]) for c in range(256) ]
    nsteps = 0

    orig = []
    with Bitpacking(nom_in, 'rb') as fin:
        while True:
            code = fin.read_code(longueur_code) 
            if code is None:
                break
            orig.append(code)

    def step(nouveau, code, dernier_car):
        nonlocal nsteps
        return
        nsteps += 1
        print(f'!tikz[framed](lzw_decomp_ex{nsteps})')
        print('```')
        print('!textmatrix\n(m) {')
        print('&'.join(map(str,range(len(orig)))) + '\\\\')
        print('&'.join(list(map(str,orig))) + '\\\\')
        print('};')
        print('\\node[above] at (m.north) { \\emph{entrée} };')

        if nsteps > 1:
            print(f'\\draw[red,rounded corners] (m-2-{nsteps-1}.north west) rectangle' \
                    + 'node[below=.25cm] {c}' + f'(m-2-{nsteps-1}.south east);')
        print(f'\\draw[green!40!black,rounded corners] (m-2-{nsteps}.north west) rectangle' \
                + 'node[below=.25cm] {n}' + f'(m-2-{nsteps}.south east);')

        print('\\matrix[below=.6cm,matrix of nodes,nodes in empty cells,nodes={anchor=center,align=center,text width=1cm,minimum height=.5cm},row 1/.style={nodes={text=white,fill=black}},row 2/.style={nodes={fill=gray!20}}]')
        print('(mt) at (m.south) {')
        print('\dots &' + '&'.join(map(str,range(256,len(table)))) + '\\\\')
        print('\dots & ' + '&'.join([ t.decode('ascii') for t in table[256:]]) + '\\\\')
        print('};')
        print('\\node[above] at (mt.north) { \\emph{table} };')

        if len(table) > nouveau:
            print('\\node[align=center,draw,rectangle,right=1cm,text width=1cm,minimum height=.5cm] (mm) at (m.east) {' + \
                    f'{table[nouveau].decode("ascii")}' + '};')
            print('\\node[below] (mml) at (mm.south) {table[n]};')
            print('\\node[right,green!40!black] at (mt.east) { $n < |table|$};')
        else:
            print('\\node[align=center,draw,rectangle,right=1cm,text width=1cm,minimum height=.5cm] (mm) at (m.east) {' + \
                    f'{table[code].decode("ascii")}' + '};')
            print('\\node[below] (mml) at (mm.south) {table[c]};')
            print('\\node[align=center,draw,rectangle,right,text width=1cm,minimum height=.5cm] (mm2) at (mm.east) {' + \
                    f'{table[code].decode("ascii")[0]}' + '};')
            print('\\node[right] (mm2l) at (mml.east) {$\leftarrow$ premier car};')
            print('\\node[right,red] at (mt.east) { $n \ge |table|$ };')

        print('```\n')
     
    with Bitpacking(nom_in, 'rb') as fin, \
            open(nom_out, 'wb') as fout:
        code = fin.read_code(longueur_code) 
        dernier_car = table[code]
        step(code, None, dernier_car)
        fout.write(dernier_car)
        while True:
            nouveau = fin.read_code(longueur_code) 
            if nouveau is None:
                break
            if len(table) <= nouveau:
                s = table[code]+dernier_car
            else:
                s = table[nouveau]
            fout.write(s) 
            dernier_car = s[:1]
            step(nouveau, code, dernier_car)
            if len(table) < 1 << longueur_code:
               table.append(table[code]+dernier_car)
            code = nouveau

# BEGIN COMPRESSE
def compresse_(nom_in, nom_out, longueur_code=12):
    table = [ bytes([c]) for c in range(256) ]
    indices = { bytes([c]) : c for c in range(256) }

    with open(nom_in, 'rb') as fin, \
            Bitpacking(nom_out, 'wb') as fout:
        m = fin.read(1)
        while True:
            c = fin.read(1)
            if c == b'': break
            if m+c in indices:
                m = m+c
            else:
                fout.write_code(indices[m], longueur_code)
                if len(table) < 1 << longueur_code:
                    indices[m+c] = len(table)
                    table.append(m+c)
                m = c

        # code final
        fout.write_code(indices[m], longueur_code)
# END COMPRESSE

# BEGIN DECOMPRESSE
def decompresse_(nom_in, nom_out, longueur_code=12):
    table = [ bytes([c]) for c in range(256) ]
     
    with Bitpacking(nom_in, 'rb') as fin, \
            open(nom_out, 'wb') as fout:

        code = fin.read_code(longueur_code) 
        fout.write(table[code])

        while True:
            nouveau = fin.read_code(longueur_code) 
            if nouveau is None:
                break

            if len(table) == nouveau:
                x = table[code][0:1] # car bytes
                s = table[code]+x
            else:
                s = table[nouveau]
                x = s[0:1] # car bytes
            fout.write(s) 
            if len(table) < 1 << longueur_code:
               table.append(table[code]+x)
            code = nouveau
# END DECOMPRESSE

if __name__ == "__main__":
    nom_in = "test.txt"
    open(nom_in,'w').write('AABABAAAB')
    # nom_in = "snippets/algorithmique/huffman_proust.txt"
    nom_out = "test.cmp"
    nom_out2 = "test.dec"

    compresse(nom_in, nom_out, longueur_code=9)
    decompresse(nom_out, nom_out2, longueur_code=9)

    assert(open(nom_out2,'rb').read() \
            == open(nom_in,'rb').read())
