
(* BEGIN LECTURE *)
let lit_triangle f =
    let rec aux () =
        try
            let l = input_line f in
            Array.of_list
                (List.map int_of_string
                    (String.split_on_char ' ' l))
            :: aux ()
        with End_of_file -> []
    in Array.of_list (aux ())
(* END LECTURE *)
 
(* BEGIN RECBRUTE *)
let rec maxpath t i j =
    if i = Array.length t - 1
    then t.(i).(j)
    else t.(i).(j) + max (maxpath t (i+1) j)
                         (maxpath t (i+1) (j+1)) 
(* END RECBRUTE *)

(* BEGIN RECMEMO *)
let cache = Hashtbl.create 42

let rec maxpath t i j =
    try
        Hashtbl.find cache (i,j)
    with Not_found ->
        let v = if i = Array.length t - 1
            then t.(i).(j)
            else t.(i).(j) + max (maxpath t (i+1) j)
                                 (maxpath t (i+1) (j+1)) 
        in Hashtbl.add cache (i,j) v; v
(* END RECMEMO *)

(* BEGIN RECMEMOCLOTURE *)
let maxpath t =
    let n = Array.length t in
    let cache = Hashtbl.create (n*n) in
    let rec aux i j =
        try
            Hashtbl.find cache (i,j)
        with Not_found ->
            let v = if i = Array.length t - 1
                then t.(i).(j)
                else t.(i).(j) + max (aux (i+1) j)
                                     (aux (i+1) (j+1)) 
        in Hashtbl.add cache (i,j) v; v
    in aux
(* END RECMEMOCLOTURE *)

(* BEGIN TAB *)
let maxpath t =
    let n = Array.length t in
    let som = Array.make_matrix n n 0 in
    for j = 0 to n-1 do
        som.(n-1).(j) <- t.(n-1).(j)
    done;
    for i = n-2 downto 0 do
        for j = 0 to i do
            som.(i).(j) <- t.(i).(j)
                + max som.(i+1).(j) som.(i+1).(j+1)
        done
    done;
    som.(0).(0)
(* END TAB *)

(* BEGIN TABREC *)
type choix = Gauche | Droite

let maxpath t =
    let n = Array.length t in
    let som = Array.make_matrix n n 0 in
    let ch = Array.make_matrix n n None in
    for j = 0 to n-1 do
        som.(n-1).(j) <- t.(n-1).(j)
    done;
    for i = n-2 downto 0 do
        for j = 0 to i do
            ch.(i).(j) <- 
                Some (if som.(i+1).(j) > som.(i+1).(j+1)
                      then Gauche else Droite);
            som.(i).(j) <- t.(i).(j)
                + max som.(i+1).(j) som.(i+1).(j+1)
        done
    done;
    let rec chemin i j =
        (i,j) :: match ch.(i).(j)
        with None -> [ ]
        | Some Gauche -> chemin (i+1) j
        | Some Droite -> chemin (i+1) (j+1)
    in chemin 0 0
(* END TABREC *)


let _ =
    let f = open_in "snippets/algorithmique/triangle.txt" in
    let t = lit_triangle f in
    List.iter (fun (i, j) ->
            Printf.printf "%dx%d\n" i j)
        (maxpath t)



