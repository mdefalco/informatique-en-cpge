(* BEGIN DROITE *)
let taille_alphabet = 256

let calcule_droite (motif:string) : int option array =
    (* Calcule le tableau droite associé au motif *)
    let droite = Array.make taille_alphabet None in
    let p = String.length motif in
    for i = 0 to p-2 do
        let j = p-2-i in
        let c = motif.[j] in
        if droite.(Char.code c) = None
        then droite.(Char.code c) <- Some j
    done;
    droite
(* END DROITE *)

(* BEGIN HORSPOOL *)
exception Difference
exception Trouve of int

let recherche_BMH (motif:string) (droite:int option array) 
    (chaine:string) : int option =
    let n = String.length chaine in
    let p = String.length motif in
    let i = ref 0 in
    try
        while !i <= n-p do
            try
                for j = p-1 downto 0 do
                    if chaine.[!i+j] <> motif.[j]
                    then begin
                        let dec =  match droite.(Char.code chaine.[!i+j]) with
                                | None -> j+1
                                | Some k when k < j -> j-k
                                | _ -> 1 in
                        i := !i + dec;
                        raise Difference
                    end
                done;
                raise (Trouve !i)
            with Difference -> ()
        done;
        None
    with Trouve k -> Some k
(* END HORSPOOL *)

let _ =
    let droite = calcule_droite "abaa" in
    assert(recherche_BMH "abaa" droite "aaabababbaa" = None);
    assert(recherche_BMH "abaa" droite "aaababaabaa" = Some 4)

(* BEGIN SUFFIXINIT *)
let calcule_suffixe_rep (x:string) : int array =
    (* Prend en entrée un mot non vide x et renvoie son tableau de suffixe *)
    let n = String.length x in
    let suffixe = Array.make n 0 in
    suffixe.(n-1) <- n;
    let plus_a_gauche = ref (n-1) in
    let depart = ref (-1) in
    
    for j = n-2 downto 0 do
        if !plus_a_gauche < j
        then begin
            (* on a j = depart - i avec *)
            let i = !depart - j in
            (* et plus_a_gauche = depart - k avec *)
            let k = !depart - !plus_a_gauche in
            if suffixe.(n-1-i) <> k-i
            then suffixe.(j) <- min suffixe.(n-1-i) (k-i)
            else begin
                depart := j;
                while !plus_a_gauche >= 0 
                    && x.[!plus_a_gauche] = x.[n-1-j + !plus_a_gauche] do
                    plus_a_gauche := !plus_a_gauche - 1
                done;
                suffixe.(j) <- !depart - !plus_a_gauche
            end
        end else begin
            plus_a_gauche := j;
            depart := j;
            while !plus_a_gauche >= 0 
                && x.[!plus_a_gauche] = x.[n-1-j + !plus_a_gauche] do
                plus_a_gauche := !plus_a_gauche - 1
            done;
            suffixe.(j) <- !depart - !plus_a_gauche
        end
    done;
    suffixe
(* END SUFFIXINIT *)

let _ =
    assert (calcule_suffixe_rep "bcabc" = [|0;2;0;0;5|]);
    assert (calcule_suffixe_rep "abbabba" = [|1;0;0;4;0;0;7|])

(* BEGIN SUFFIXE *)
let calcule_suffixe (x:string) : int array =
    (* Prend en entrée un mot non vide x et renvoie son tableau de suffixe *)
    let n = String.length x in
    let suffixe = Array.make n (-1) in
    suffixe.(n-1) <- n;
    let plus_a_gauche = ref (n-1) in
    let depart = ref (-1) in
    
    for j = n-2 downto 0 do
        if !plus_a_gauche < j
            && suffixe.(n-1- !depart+j) <> j - !plus_a_gauche
        then suffixe.(j) <- min suffixe.(n-1- !depart+j) (j - !plus_a_gauche)
        else begin
            plus_a_gauche := min j !plus_a_gauche;
            depart := j;
            while !plus_a_gauche >= 0 
                && x.[!plus_a_gauche] = x.[n-1-j + !plus_a_gauche] do
                plus_a_gauche := !plus_a_gauche - 1
            done;
            suffixe.(j) <- !depart - !plus_a_gauche
        end
    done;
    suffixe
(* END SUFFIXE *)

let _ =
    assert (calcule_suffixe "bcabc" = [|0;2;0;0;5|]);
    assert (calcule_suffixe "abbabba" = [|1;0;0;4;0;0;7|])

(* BEGIN BONSUFFIXE *)
let calcule_bonsuffixe (x:string) : int array =
    (* Prend en entrée un mot non vide x et renvoie son tableau de suffixe *)
    let n = String.length x in
    let suffixe = calcule_suffixe x in
    let bonsuffixe = Array.make n n in

    let suivant = ref 0 in
    for k = n-2 downto 0 do
        if suffixe.(k) = k+1 (* c'est un bord *)
        then begin
            for i = !suivant to n-2-k do
                bonsuffixe.(i) <- n-1-k
            done;
            suivant := n-1-k
        end
    done;

    for k = 0 to n-2 do
        bonsuffixe.(n-1-suffixe.(k)) <- n-1-k
    done;

    bonsuffixe
(* END BONSUFFIXE *)

let _ =
    assert (calcule_bonsuffixe "baacababa" = [|7;7;7;7;7;2;7;4;1|]);
    assert (calcule_bonsuffixe "abaababaaba" = [|5;5;5;5;5;8;8;3;10;2;1|]);
    assert (calcule_bonsuffixe "bcabc" = [|3;3;3;5;1|])

(* BEGIN BOYERMOORE *)
let recherche_BM (motif:string) 
    (droite:int option array) (bonsuffixe:int array)
    (chaine:string) : int option =
    let n = String.length chaine in
    let p = String.length motif in
    let i = ref 0 in
    try
        while !i <= n-p do
            try
                for j = p-1 downto 0 do
                    if chaine.[!i+j] <> motif.[j]
                    then begin
                        let dec =  match droite.(Char.code chaine.[!i+j]) with
                                | None -> j+1
                                | Some k when k < j -> j-k
                                | _ -> 1 in
                        i := !i + max dec bonsuffixe.(j);
                        raise Difference
                    end
                done;
                raise (Trouve !i)
            with Difference -> ()
        done;
        None
    with Trouve k -> Some k
(* END BOYERMOORE *)

let _ =
    let chaine = "caabccbbc" in
    let motif = "bcabc" in
    let droite = calcule_droite motif in
    let bonsuffixe = calcule_bonsuffixe motif in
    assert (recherche_BMH motif droite chaine 
            = recherche_BM motif droite bonsuffixe chaine)
