let distance (x1,y1) (x2,y2) = sqrt ((x1-.x2)*.(x1-.x2) +. (y1-.y2)*.(y1-.y2))

let plus_proche_paire points =
    let n = Array.length points in
    let min_paire = ref (distance points.(0) points.(1), (0, 1)) in
    for i = 0 to n - 1 do
        for j = i+1 to n - 1 do
            let d = distance points.(i) points.(j) in
            if d < fst !min_paire
            then min_paire := (d, (i, j))
        done
    done;
    snd !min_paire

module Point = struct
    type t = float * float
    let compare (x1,y1) (x2,y2) = Stdlib.compare y1 y2
end

module PointSet = Set.Make(Point)

let set_iter_entre f set bas haut =
    try
        let e = PointSet.find_first (fun p -> snd p >= bas) set in
        let seq = PointSet.to_seq_from e set in
        let rec aux seq =
            match seq () with
            | Seq.Nil -> ()
            | Seq.Cons (p, seq_suite) -> 
                    if snd p <= haut
                    then begin
                        f p;
                        aux seq_suite
                    end
        in aux seq
    with Not_found -> ()

let plus_proche_paire_balayage points =
    let compare (x1,y1) (x2,y2) =
        if x1 = x2
        then if y1 < y2 then -1 else 1
        else if x1 < x2 then -1 else 1
    in
    Array.sort compare points;
    let n = Array.length points in
    let d = ref (distance points.(0) points.(1)) in
    let couple = ref (points.(0), points.(1)) in
    let actifs = ref (PointSet.empty 
            |> PointSet.add points.(0) |> PointSet.add points.(1)) in

    let gauche = ref 0 in

    for i = 2 to n-1 do
        let xi, yi = points.(i) in
        let fout = open_out (Printf.sprintf "tmp/test%04d.tex" i) in

        Printf.fprintf fout "\\documentclass{standalone}\n\\usepackage{tikz}\n";
        Printf.fprintf fout "\\usepackage[french]{babel}\n\\usepackage{avant}\n\\usepackage{mathptmx}\n";
        Printf.fprintf fout "\\usepackage{microtype}\n\\usepackage[utf8]{inputenc}\n\\usepackage[T1]{fontenc}\n\\usepackage{fontspec}";
        Printf.fprintf fout "\\usetikzlibrary{matrix,calc,shapes.multipart}\n\\begin{document}\n\\begin{tikzpicture}[anchor=base,baseline]";

        Printf.fprintf fout "\\fill [white] (-1,-1) rectangle (11,11);\n";

        Printf.fprintf fout "\\fill [gray!40] (%f,0) rectangle (%f,10);\n"
            (max 0. (xi-. !d)) xi;

        Printf.fprintf fout "\\fill [gray!80] (%f,%f) rectangle (%f,%f);\n"
            (max 0. (xi-. !d)) (max 0. (yi-. !d)) xi (min 10. (yi+. !d));

        Printf.fprintf fout "\\draw[black] (%f,%f) edge[<->] node[below] {$d$} (%f,%f);\n"
            (max 0. (xi-. !d)) (max 0. (yi-. !d)) xi (max 0. (yi-. !d));
        Printf.fprintf fout "\\draw[black] (%f,%f) edge[<->] node[left] {$d$} (%f,%f);\n"
            (max 0. (xi-. !d)) (max 0. (yi-. !d)) (max 0. (xi -. !d)) yi;
        Printf.fprintf fout "\\draw[black] (%f,%f) edge[<->] node[left] {$d$} (%f,%f);\n"
            (max 0. (xi-. !d)) yi (max 0. (xi -. !d)) (min 10. (yi+. !d));
        
        Printf.fprintf fout "\\draw [black] (%f,0) -- (%f,10);\n" xi xi;

        Array.iter (fun p -> 
            Printf.fprintf fout "\\node [inner sep=0.02cm, draw=black, fill=gray!80, circle] at (%f,%f) {};\n"
                (fst p) (snd p)) points;
        PointSet.iter (fun p ->
            Printf.fprintf fout "\\node [inner sep=0.02cm, draw=red, fill=red!80, circle] at (%f,%f) {};\n"
                (fst p) (snd p)) !actifs;

        Printf.fprintf fout "\\node [inner sep=0.05cm, draw=red, fill=red!80, circle] at (%f,%f) {};\n"
            xi yi;

        let (x1,y1), (x2,y2) = !couple in
        Printf.fprintf fout "\\draw [thick,blue] (%f,%f) -- node[above] {$d$} (%f,%f);\n"
            x1 y1 x2 y2;

        Printf.fprintf fout "\\end{tikzpicture}\n\\end{document}\n";

        while fst points.(!gauche) < xi -. !d do
            actifs := PointSet.remove points.(!gauche) !actifs;
            incr gauche
        done;

        set_iter_entre (fun pj -> 
            let dip = distance points.(i) pj in
            if dip < !d
            then begin
                couple := (points.(i), pj);
                d := dip
            end) !actifs (yi -. !d) (yi +. !d);

        actifs := PointSet.add points.(i) !actifs
    done;
    !d

let _ =
    Random.init 3000;
    let n = 20 in
    let d = 10. in
    let points = Array.init n (fun i -> (Random.float d, Random.float d)) in

    let i, j = plus_proche_paire points in
    let d = distance points.(i) points.(j) in
    let d2 = plus_proche_paire_balayage points in
    Printf.printf "%f %f\n" d d2


