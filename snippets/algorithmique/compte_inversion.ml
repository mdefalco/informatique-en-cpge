let inversions_croisees t1 t2 =
    let n1 = Array.length t1 in
    let n2 = Array.length t2 in
    let k1 = ref 0 in
    let inv = ref 0 in
    for k2 = 0 to n2 - 1 do
        while !k1 < n1 && t1.(!k1) <= t2.(k2) do
            incr k1
        done;
        if !k1 < n1
        then inv := !inv + n1 - !k1
    done;
    !inv

let rec inversions_aux t i j =
    let n = j-i in
    if n >= 1
    then begin
        let m = i + n / 2 in
        let i1 = inversions_aux t i m in
        let i2 = inversions_aux t (m+1) j in
        let t1 = Array.sub t i (m-i+1) in
        let t2 = Array.sub t (m+1) (j-m) in
        Array.sort compare t1;
        Array.sort compare t2;
        let n12 = inversions_croisees t1 t2 in
        i1+i2+n12
    end else 0
    
let inversions t = inversions_aux t 0 (Array.length t - 1)
