let t = [| 30; 35; 15; 5; 10; 20; 25 |]

(* BEGIN REC *)
let rec matmult t i j =
    if i = j
    then 0
    else let m = ref (t.(i) * t.(i+1) * t.(j+1) 
            + matmult t (i+1) j) in
        for k = i+1 to j-1 do
            m := min !m 
                (t.(i) * t.(k+1) * t.(j+1)
                    + matmult t i k + matmult t (k+1) j)
        done;
        !m
(* END REC *)

(* BEGIN RECMEMO *)
let matmult t =
    let n = Array.length t in
    let cache = Hashtbl.create (n*n) in
    let rec aux i j =
        try 
            Hashtbl.find cache (i,j)
        with Not_found ->
            let v =  if i = j
                then 0
                else 
                    let m = ref (t.(i) * t.(i+1) * t.(j+1) 
                        + matmult t (i+1) j) in
                    for k = i+1 to j-1 do
                        m := min !m 
                            (t.(i) * t.(k+1) * t.(j+1)
                                + aux i k 
                                + aux (k+1) j)
                    done;
                    !m
            in Hashtbl.add cache (i,j) v; v
    in aux
(* END RECMEMO *)


(* BEGIN TABULE *)
let matmult t =
    let n = Array.length t in
    let cout = Array.make_matrix (n-1) (n-1) 0 in
    for i = n-2 downto 0 do
        for j = i+1 to n-2 do
            let m = ref (t.(i) * t.(i+1) * t.(j+1) 
                        + cout.(i+1).(j)) in
            for k = i+1 to j-1 do
                m := min !m 
                    (t.(i) * t.(k+1) * t.(j+1)
                        + cout.(i).(k) 
                        + cout.(k+1).(j))
            done;
            cout.(i).(j) <- !m
        done
    done;
    cout.(0).(n-2)
(* END TABULE *)

(* BEGIN TABREC *)
type arbre = Feuille of int | Noeud of arbre * arbre

let matmult t =
    let n = Array.length t in
    let cout = Array.make_matrix (n-1) (n-1) 0 in
    let arbres = Array.make_matrix (n-1) (n-1) (Feuille (-1)) in
    for i = n-2 downto 0 do
        arbres.(i).(i) <- Feuille i;
        for j = i+1 to n-2 do
            let m = ref (t.(i) * t.(i+1) * t.(j+1) 
                        + cout.(i+1).(j)) in
            let a = ref (Noeud(arbres.(i).(i), 
                               arbres.(i+1).(j))) in
            for k = i+1 to j-1 do
                let v = t.(i) * t.(k+1) * t.(j+1)
                        + cout.(i).(k) 
                        + cout.(k+1).(j)
                in
                if v < !m
                then begin
                    m := v;
                    a := Noeud(arbres.(i).(k),
                               arbres.(k+1).(j))
                end
            done;
            cout.(i).(j) <- !m;
            arbres.(i).(j) <- !a
        done
    done;
    arbres.(0).(n-2)
(* END TABREC *)

let rec pp_arbre a = 
    match a with
    | Feuille i -> "A"^string_of_int i
    | Noeud(Feuille i, Feuille j) -> 
        "A"^string_of_int i^"A"^string_of_int j
    | Noeud(Feuille i, d) -> 
        "A"^string_of_int i^"("^pp_arbre d^")"
    | Noeud(g, Feuille i) -> 
        "("^pp_arbre g^")A"^string_of_int i
    | Noeud(g,d) -> "("^pp_arbre g^")("^pp_arbre d^")"

let _ =
    let n = Array.length t in
    Printf.printf "%s\n"
        (pp_arbre (matmult t))
