#include <stdio.h>

void echange(int *t, int i, int j)
{
    int temp = t[i];
    t[i] = t[j];
    t[j] = temp;
}

void drapeau(int *t, int nb, int i)
{
    int v = t[i];
    int l = 0;
    int c = nb-1;
    int r = nb-1;

    while (l <= c)
    {
        int w = t[l];
        if (w < v)
        {
            l = l+1;
        }
        else if (w == v)
        {
            echange(t, l, c);
            c = c-1;
        }
        else
        {
            echange(t, l, c);
            echange(t, c, r);
            c = c-1;
            r = r-1;
        }
    }
    printf("%d %d %d\n", l, c, r);
}

int main()
{
    int t[] = { 5, 4, 2, 3, 1, 5, 0 };

    for (int i = 0; i < 7; i++)
        printf("%d ", t[i]);
    printf("\n");

    drapeau(t, 7, 3);

    for (int i = 0; i < 7; i++)
        printf("%d ", t[i]);
    printf("\n");
    return 0;
}
