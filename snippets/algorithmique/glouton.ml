
type arbre = Noeud of arbre * arbre | Feuille of int

let construit f =
    let n = Array.length f in
    let file = PriorityQueue.cree n (Feuille 0) 0 in
    for c = 0 to n-1 do
        PriorityQueue.ajoute file (Feuille c) (-f.(c))
    done
        


