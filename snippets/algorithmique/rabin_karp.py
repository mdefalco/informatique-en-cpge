r = 26
q = 17
k = 2

def hash(r,q,s):
    e = 0
    p = 1
    for c in reversed(s):
        e = (ord(c) * p + e) % q
        p = (r * p) % q
    return e

def delta(r,q,rp,a,b,e):
    return (r * (e - rp * ord(a)) + ord(b)) % q

def fastmod(a):
    s = 0
    q = 0x7fffffff
    while a > 0:
        s = s + a & q
        a = a >> 31
    if s > q:
        return fastmod(s)
    elif s == q:
        return 0

    return s

n = 9438394839
print(fastmod(n), n % 0x7fffffff)

r = 256
q = 0x7fffffff
print(hash(r,q,'test'))
print(delta(r,q,(r*r*r)%q,'t','u',hash(r,q,'test')))

def rabin_karp(m, s):
    p, n = len(m), len(s)
    r, q = 256, 0x7fffffff
    rp = (r ** (p-1)) % q
    me, e = hash(r,q,m), hash(r,q,s[:p])
    for i in range(0,n-p+2):
        if me == e and m == s[i:i+p]:
            return i
        if i+p < n:
            e = delta(r,q,rp,s[i],s[i+p],e)
    return None

print(rabin_karp('to','tatatutitotatuto'))
print(rabin_karp('tito','tatatutitotatuto'))
print(rabin_karp('tita','tatatutitotatuto'))

