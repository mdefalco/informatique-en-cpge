type 'a tas = {
    elements : 'a array;
    mutable taille : int
}

let cree_tas taille_max defaut = 
    { 
        elements = Array.make taille_max defaut; 
        taille = 0 
    }

let pere a =
    if a = 0
    then failwith "racine"
    else (a+1)/2 - 1

let gauche a = 2*(a+1) - 1
let droite a = 2*(a+1)

let tas_nth tas i = tas.elements.(i)

let swap v x y =
    let t = v.(x) in
    v.(x) <- v.(y);
    v.(y) <- t

let tas_insere x tas =
    let pos = ref tas.taille in
    tas.elements.(tas.taille) <- x;
    tas.taille <- tas.taille + 1;
    while !pos > 0 && tas_nth tas (pere !pos) < tas_nth tas !pos do
        let p = pere !pos in
        swap tas.elements p !pos;
        pos := p
    done

let max_pere_fils tas a =
    let max_courant = ref a in

    if tas.taille > gauche a 
        && tas_nth tas (gauche a) > tas_nth tas !max_courant
    then max_courant := gauche a;
    if tas.taille > droite a 
        && tas_nth tas (droite a) > tas_nth tas !max_courant
    then max_courant := droite a;
    !max_courant

let supprime_racine tas =
    assert (tas.taille > 0);
    tas.taille <- tas.taille - 1;
    let x = tas.elements.(0) in
    if tas.taille > 0
    then begin
        let pos = ref 0 in
        tas.elements.(0) <- tas.elements.(tas.taille);
        while max_pere_fils tas !pos <> !pos do
            let max_fils = max_pere_fils tas !pos in
            swap tas.elements !pos max_fils;
            pos := max_fils
        done
    end;
    x

type arbre = Feuille of int | Noeud of arbre * arbre

(* BEGIN ARBRE *)
let huffman_construit_arbre freq =
    let arbres = cree_tas 2000 (0, Feuille 0) in
    for i = 0 to Array.length freq - 1 do
        let c, f = freq.(i) in
        if f > 0 (* on ignore les frequences nulles *)
        then tas_insere (-f, Feuille c) arbres
    done;
    while arbres.taille > 1 do
        let fx, x = supprime_racine arbres in
        let fy, y = supprime_racine arbres in
        tas_insere (fx+fy, Noeud(x,y)) arbres
    done;
    snd (supprime_racine arbres)
(* END ARBRE *)


type out_channel_bits = {
    o_fichier : out_channel;
    mutable o_accumulateur : int;
    mutable o_bits_accumules : int
}

type in_channel_bits = {
    i_fichier : in_channel;
    mutable i_accumulateur : int;
    mutable i_bits_accumules : int;
    i_taille : int
}

let open_in_bits fn =
    let fichier = open_in_bin fn in
    { i_fichier = fichier; 
      i_accumulateur = 0; i_bits_accumules = 0; i_taille = in_channel_length fichier }

let open_out_bits fn =
    { o_fichier = open_out_bin fn; o_accumulateur = 0; o_bits_accumules = 0 }

let input_bit f =
    if f.i_bits_accumules = 0
    then begin
        f.i_accumulateur <- input_byte f.i_fichier;
        f.i_bits_accumules <- 8;
        if pos_in f.i_fichier = f.i_taille - 1
        then begin
            let pad = input_byte f.i_fichier in
            f.i_bits_accumules <- f.i_bits_accumules - pad
        end
    end;
    let bit = Int.logand 0x80 f.i_accumulateur <> 0 in
    f.i_accumulateur <- 2 * f.i_accumulateur;
    f.i_bits_accumules <- f.i_bits_accumules - 1;
    bit

let close_in_bits f = close_in f.i_fichier

let close_out_bits f =
    let padding = 8 - f.o_bits_accumules in
    if padding > 0
    then output_byte f.o_fichier (Int.shift_left f.o_accumulateur padding);
    output_byte f.o_fichier padding;
    close_out f.o_fichier

let output_bit f b =
    if f.o_bits_accumules = 8
    then begin
        output_byte f.o_fichier f.o_accumulateur;
        f.o_accumulateur <- 0;
        f.o_bits_accumules <- 0
    end;
    f.o_accumulateur <- 2 * f.o_accumulateur + (if b then 1 else 0);
    f.o_bits_accumules <- 1 + f.o_bits_accumules

(* BEGIN SERARBRE *)
let rec output_arbre f a =
    match a with
    | Noeud (x, y) -> begin
        output_byte f 0;
        output_arbre f x;
        output_arbre f y
    end
    | Feuille c -> begin
        output_byte f 1;
        output_byte f c
    end

let rec input_arbre f = 
    let code = input_byte f in
    match code with
    | 0 -> let g = input_arbre f in
        let d = input_arbre f in
        Noeud(g,d)
    | _ -> Feuille (input_byte f)

(* END SERARBRE *)

let rec huffman_pprint_arbre arbre = match arbre with
    | Feuille c -> Printf.printf "%02x" c
    | Noeud(x, y) ->
            print_string "(";
            huffman_pprint_arbre x;
            print_string ",";
            huffman_pprint_arbre y;
            print_string ")"

let calcule_frequences nomdefichier =
    let fichier = open_in_bin nomdefichier in
    let occurrences = Array.make 256 0 in
    let caracteres = ref 0 in
    begin
    try
        while true do
            let c = input_byte fichier in
            occurrences.(c) <- occurrences.(c) + 1;
            incr caracteres
        done
    with End_of_file -> ()
    end;
    close_in fichier;
    Array.mapi (fun i f -> (i, f)) occurrences

let huffman_chemins a =
    let a_plat = Array.make 256 (fun _ -> ()) in
    let rec parcours a chemin =
        match a with
        | Noeud (g, d) -> 
            parcours g (fun f -> chemin f; output_bit f false);
            parcours d (fun f -> chemin f; output_bit f true)
        | Feuille c -> a_plat.(c) <- chemin
    in parcours a (fun _ -> ()); a_plat

let rec huffman_chemin_to_string ch =
    match ch with
    | true::q -> "1" ^ huffman_chemin_to_string q
    | false::q -> "0" ^ huffman_chemin_to_string q
    | [] -> ""

let huffman_pprint_arbre_plat a_plat =
    for i = 0 to 255 do
        let l = a_plat.(i) in
        if l <> []
        then Printf.printf "%02x : %s\n" i (huffman_chemin_to_string l)
    done

let rec huffman_decompresse_byte f a =
    match a with
    | Feuille c -> c
    | Noeud(g,d) ->
        huffman_decompresse_byte f (if input_bit f then d else g)

let huffman_compresse_byte f a_plat c =
    a_plat.(c) f

let _ =
    let nomdefichier = "snippets/algorithmique/huffman_proust.txt" in
    (* let nomdefichier = "test.txt" in *)
    let freq = calcule_frequences nomdefichier in
    (*
    for i = 0 to 255 do
        if snd freq.(i) > 0
        then Printf.printf "%02x : %d\n" i (snd freq.(i))
    done;
    *)
    let a = huffman_construit_arbre freq in
    let f = open_out_bits "test.out" in
    output_arbre f.o_fichier a; 
    let a_plat = huffman_chemins a in
    (*
    huffman_pprint_arbre a;
    print_newline ();
    huffman_pprint_arbre_plat a_plat; 
    *)
    let fichier = open_in_bin nomdefichier in
    let t1 = Unix.gettimeofday () in
    begin
        try
            while true do
                let c = input_byte fichier in
                huffman_compresse_byte f a_plat c
            done
        with End_of_file -> ()
    end;
    close_in fichier;
    close_out_bits f;
    Printf.printf "Temps compression %f\n" (Unix.gettimeofday () -. t1);

    let f = open_in_bits "test.out" in
    let fo = open_out "test-back.txt" in
    let a = input_arbre f.i_fichier in
    huffman_pprint_arbre a;

    let t1 = Unix.gettimeofday () in
    begin
        try
            while true do
                output_byte fo (huffman_decompresse_byte f a)
            done
        with End_of_file -> ()
    end;
    close_in_bits f;
    close_out fo;
    Printf.printf "Temps decompression %f\n" (Unix.gettimeofday () -. t1);


