exception Trouve of int
exception PasDeMotif

(* renvoie un booléen indiquant si m est présent
 * dans s à l'indice i *)
let cherche_motif (m:string) (s:string) (i:int) : bool =
    let p = String.length m in
    try
        for j = 0 to p-1 do
            if s.[i+j] <> m.[j]
            then raise PasDeMotif
        done;
        true
    with PasDeMotif -> false

let recherche_naive m s =
    let n = String.length s in
    let p = String.length m in
    try
        for i = 0 to n-p do
            if cherche_motif m s i
            then raise (Trouve i)
        done;
        None
    with Trouve i -> Some i
(* IGNORE BELOW *)
let _ =
    assert (recherche_naive "toto" "totitotota" = Some 4);
    assert (recherche_naive "tata" "totitotota" = None);
    assert (recherche_naive "aaab" "aaaab" = Some 1)
