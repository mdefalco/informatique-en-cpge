(* BEGIN OUTBIT *)
type out_channel_bits = {
    o_fichier : out_channel;
    mutable o_accumulateur : int;
    mutable o_bits_accumules : int
}

let open_out_bits fn =
    { o_fichier = open_out_bin fn; o_accumulateur = 0; o_bits_accumules = 0 }

let output_bit f b =
    if f.o_bits_accumules = 8
    then begin
        output_byte f.o_fichier f.o_accumulateur;
        f.o_accumulateur <- 0;
        f.o_bits_accumules <- 0
    end;
    if b
    then f.o_accumulateur <- f.o_accumulateur + 1 lsl f.o_bits_accumules;
    f.o_bits_accumules <- 1 + f.o_bits_accumules
(* END OUTBIT *)

(* BEGIN OUTBITCLOSE *)
let close_out_bits f =
    if f.o_bits_accumules = 0
    then output_byte f.o_fichier 0
    else begin
        let padding = 8 - f.o_bits_accumules in
        output_byte f.o_fichier (Int.shift_left f.o_accumulateur padding);
        output_byte f.o_fichier padding
    end;
    close_out f.o_fichier
(* END OUTBITCLOSE *)

(* BEGIN INBIT *)
type in_channel_bits = {
    i_fichier : in_channel;
    mutable i_accumulateur : int;
    mutable i_bits_accumules : int;
    i_taille : int
}

let open_in_bits fn =
    let fichier = open_in_bin fn in
    { i_fichier = fichier; 
      i_accumulateur = 0; i_bits_accumules = 0; i_taille = in_channel_length fichier }

let input_bit f =
    if f.i_bits_accumules = 0
    then begin
        f.i_accumulateur <- input_byte f.i_fichier;
        f.i_bits_accumules <- 8;
        if pos_in f.i_fichier = f.i_taille - 1
        then begin
            let pad = input_byte f.i_fichier in
            f.i_bits_accumules <- f.i_bits_accumules - pad
        end
    end;
    let bit = f.i_accumulateur mod 2 = 1 in
    f.i_accumulateur <- f.i_accumulateur / 2;
    f.i_bits_accumules <- f.i_bits_accumules - 1;
    bit

let close_in_bits f = close_in f.i_fichier
(* END INBIT *)

(* BEGIN FILEBITS *)
let input_code f longueur_code =
    let acc = ref 0 in
    for i = 0 to longueur_code - 1 do
        let b = input_bit f in
        if b
        then acc := !acc + (1 lsl i)
    done;
    !acc

let output_code f code longueur_code =
    assert (code < 1 lsl longueur_code);

    let acc = ref code in
    for i = 0 to longueur_code - 1 do
        output_bit f (!acc mod 2 = 1);
        acc := !acc / 2
    done
(* END FILEBITS *)
