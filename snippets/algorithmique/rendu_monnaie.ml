let rec rendus v n =
    let ajoute i l =
        List.iter 
            (fun s -> s.(i) <- s.(i) + 1) l;
        l
    in
    if n = 0 
    then [ Array.make (Array.length v) 0 ]
    else let acc : int array list ref = ref [] in
        for i = 0 to Array.length v - 1 do
            let d = v.(i) in
            if d <= n 
            then acc := !acc @ (ajoute i (rendus v (n-d)))
        done;
        !acc

let rec unique l = match l with
    | [] -> []
    | t::q -> if List.mem t q then unique q else t :: unique q

let _ =
    List.map (fun v ->
        for i = 0 to Array.length v - 1 do print_int v.(i) done; print_newline ())
        (unique (rendus [|1;2;5;10|] 14))
