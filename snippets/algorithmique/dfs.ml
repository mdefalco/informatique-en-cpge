(* BEGIN DFS *)
type statut = Inconnu | EnTraitement | Traite

type etat_dfs = {
    statut : statut array;
    mutable clock : int;
    entree : int array;
    sortie : int array
}

let tick etat =
    let t = etat.clock in
    etat.clock <- t+1;
    t

let rec dfs ladj etat x =
    if etat.statut.(x) <> Traite
    then begin
        etat.statut.(x) <- EnTraitement;
        etat.entree.(x) <- tick etat;
        List.iter (fun y ->
            if etat.statut.(y) = Inconnu
            then dfs ladj etat y) ladj.(x);
        etat.sortie.(x) <- tick etat;
        etat.statut.(x) <- Traite
    end

let initialise_dfs ladj =
    let n = Array.length ladj in
    {
        statut = Array.make n Inconnu;
        clock = 1;
        entree = Array.make n 0;
        sortie = Array.make n 0
    }
(* END DFS *)

let g = [| [ 1 ]; [ 2; 4 ]; [ 0; 3 ]; [ 4 ]; [ 3 ] |]
let etat = initialise_dfs g

