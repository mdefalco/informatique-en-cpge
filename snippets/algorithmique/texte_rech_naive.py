def recherche_naive(motif, chaine):
    '''Recherche le motif dans la chaine et renvoie l'indice de 
    la première occurrence du motif s'il est présent ou None sinon.'''
    n, p = len(chaine), len(motif)
    for i in range(n-p+1):
        present = True
        for j in range(p):
            if chaine[i+j] != motif[j]:
                present = False
                break
        if present:
            return i
    return None
# IGNORE BELOW
if __name__ == "__main__":
    assert(recherche_naive("toto", "totitotota") == 4)
    assert(recherche_naive("tata", "totitotota") == None)
    assert(recherche_naive("aaab", "aaaab") == 1)
