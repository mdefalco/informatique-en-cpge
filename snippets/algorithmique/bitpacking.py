import os

# BEGIN OUTBIT
class Bitpacking:
    '''La classe Bitpacking se comporte comme un fichier Python
    tout en fournissant des méthodes spécifiques à la lecture
    et à l'ecriture de bit.'''

    def __init__(self, filename, mode):
        self.file = open(filename, mode)
        self.accumulateur = 0
        self.bits_accumules = 0

        self.input = mode[0] == 'r'
        if self.input:
            self.init_input()
    
    def write_bit(self, b):
        if self.bits_accumules == 8:
            self.write(bytes([self.accumulateur]))
            self.accumulateur = 0
            self.bits_accumules = 0
        self.accumulateur = 2 * self.accumulateur \
                + (1 if b else 0)
        self.bits_accumules += 1
    # END OUTBIT

    # BEGIN INBIT
    def init_input(self):
        self.file.seek(0, os.SEEK_END)
        self.taille = self.file.tell()
        self.file.seek(0, os.SEEK_SET)
    
    def read_bit(self):
        if self.bits_accumules == 0:
            c = self.file.read(1)
            if len(c) == 0: # fin de fichier
                return b''
            self.accumulateur = ord(c)
            self.bits_accumules = 8
            if self.file.tell() == self.taille - 1:
                pad = ord(self.file.read(1))
                self.bits_accumules -= pad
        bit = (self.accumulateur & 0x80) != 0
        self.accumulateur *= 2
        self.bits_accumules -= 1
        return bit
    # END INBIT

    # BEGIN FILEBITS
    def read_code(self, longueur_code):
        acc = 0
        for i in range(longueur_code):
            b = self.read_bit()
            if b == b'':
                assert(i == 0) # pas au milieu d'un code
                return None
            if b:
                acc += 1 << i
        return acc

    def write_code(self, code, longueur_code):
        assert(code < 1 << longueur_code)

        for i in range(longueur_code):
            self.write_bit( code % 2 == 1 )
            code //= 2
    # END FILEBITS

    # BEGIN OUTBITCLOSE
    def close(self):
        if not self.input:
            self.close_output()
        self.file.close()

    def close_output(self):
        if self.bits_accumules == 0:
            self.file.write(b'\x00')
        else:
            padding = 8 - self.bits_accumules
            self.file.write(bytes([
                self.accumulateur << padding,
                padding ]))
    # END OUTBITCLOSE

    # nécessaires pour manipuler comme un fichier
    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        self.close()

    def write(self, *args, **kwargs):
        self.file.write(*args, **kwargs)

    def read(self, *args, **kwargs):
        self.file.read(*args, **kwargs)


if __name__ == "__main__":
    l = [ True, True, False, True ]

    f = Bitpacking('/tmp/test','wb')
    f.write_bits(l)
    f.close()

    f = Bitpacking('/tmp/test','rb')
    assert(f.read_bits(len(l)) == l)
    f.close()
