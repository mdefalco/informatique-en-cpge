from PIL import Image

N = 128
im = Image.open('rotdiv.jpg').resize((N,N))
arr = im.load()

count = 0

def blit(src, xsrc, ysrc, tgt,xtgt, ytgt, w, h):
    for i in range(w):
        for j in range(h):
            tgt[xtgt + i, ytgt+j] = src[xsrc+i,ysrc+j]


def rotate(x1,y1,sz):
    global count

    if sz <= 1:
        return
    
    
    sz //= 2
    xm = x1 + sz
    ym = y1 + sz

    m2 = {}
    blit(arr,x1,y1, m2, 0, 0, sz, sz)
    blit(arr,x1,ym,arr,x1,y1, sz, sz)
    blit(arr,xm,ym,arr,x1,ym, sz, sz)
    blit(arr,xm,y1,arr,xm,ym, sz, sz)
    blit(m2,0,0,arr,xm,y1, sz, sz)


    rotate(x1,y1,sz)
    rotate(xm,y1,sz)
    rotate(x1,ym,sz)
    rotate(xm,ym,sz)

    count += 1
    im.resize((1024,1024),Image.NEAREST).save('sest_%09d.png' % count)

rotate(0,0,N)

#im.resize((128,128), Image.NEAREST).save('rot.png')
