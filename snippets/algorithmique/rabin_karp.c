#include <stdint.h>
#include <stdio.h>
#include <string.h>

int64_t fastmod(int64_t a)
{
    int64_t s = 0;
    const int64_t q = 0x7fffffff;
    
    while (a > 0)
    {
        s = s + a & q;
        a = a >> 31;
    }

    if (s > q)
        return fastmod(s);
    if (s == q)
        return 0;
    return s;
}

int64_t hash(int64_t r, int64_t q, char *s, int n)
{
    int64_t p = 1;
    int64_t e = 0;

    for (int i = n-1; i >= 0; i--)
    {
        e = (p * s[i] + e) % q;
        p = (r * p) % q;
    }

    return e;
}

int64_t delta(int64_t r, int64_t q, int64_t rp,
        char a, char b, int64_t e)
{
    return (r * (e - rp * a) + b) % q;
}

int64_t powmod(int64_t r, int k, int64_t q)
{
    int64_t p = 1;
    for (int i = 0; i < k; i++)
    {
        p = (r * p) % q;
    }
    return p;
}

int rabin_karp(char *m, char *s)
{
    const int64_t r = 256;
    const int64_t q = 0x7fffffff;
    const int p = strlen(m);
    const int n = strlen(s);
    const int64_t rp = powmod(r,p-1,q);
    const int64_t me = hash(r,q,m,p);
    int64_t e = hash(r,q,s,p);
    for (int i=0; i <n-p+1; i++)
    {
        if (me == e && strncmp(m,(s+i),p) == 0)
            return i;
        if (i+p < n)
            e = delta(r,q,rp,s[i],s[i+p],e);
    }
    return -1;
}

int main(void)
{
    printf("%ld\n", fastmod(74232318234));
    int64_t r = 256;
    int64_t e = hash(r, 0x7fffffff, "test", 4);
    int64_t e3 = hash(r, 0x7fffffff, "estu", 4);

    printf("%ld %ld %ld\n", e, e3, delta(r, 0x7fffffff, 
                    (r * r * r) % 0x7fffffff, 't', 'u', e));
    printf("%d %d %d\n",
            rabin_karp("to", "tataitoati"),
            rabin_karp("tait", "tataitoati"),
            rabin_karp("toit", "tataitoati"));

    return 0;
}
