# BEGIN DROITE
def calcule_droite(taille_alphabet, num, motif):
    '''taille_alphabet est le nombre de caractères et num
    une fonction qui à tout caractère de l'alphabet associe
    un unique numéro dans 0 .. taille_alphabet-1.'''
    droite = [ None ] * taille_alphabet
    p = len(motif)
    for i in range(len(motif)-1):
        j = p-2-i # indice en partant de la fin
        c = motif[j]
        if droite[num(c)] is None:
            droite[num(c)] = j
    return droite
# END DROITE

# BEGIN EX DROITE
def num_minuscule(c):
    return ord(c) - ord('a')
droite = calcule_droite(26, num_minuscule, 'abaa')
# END EX DROITE

# BEGIN HORSPOOL
def recherche_bmh(num, motif, droite, chaine):
    '''Cherche motif dans chaine en utilisant la table de saut
    précalculée droite et la correspondance num entre caractères
    et indices dans la table'''
    n, p = len(chaine), len(motif)
    i = 0
    print(chaine)
    while i <= n-p:
        present = True
        print(' '*i+motif)
        for j in reversed(range(p)):
            if chaine[i+j] != motif[j]:
                present = False
                k = droite[num(chaine[i+j])]
                if k is None:
                    i = i + j + 1
                elif k < j:
                    i = i + j - k
                else:
                    i = i + 1
                break
        if present:
            return i
    return None
# END HORSPOOL

assert(recherche_bmh(num_minuscule, 'abaa', droite, 'aaabababbaa') == None)
assert(recherche_bmh(num_minuscule, 'abaa', droite, 'aaababaabaa') == 4)

# BEGIN SUFFIXINIT
def calcule_suffixe_rep(x):
    '''Prend en entrée un mot non vide x et renvoie son tableau de suffixe'''
    n = len(x)
    suffixe = [ None ] * n
    suffixe[n-1] = n
    plus_a_gauche = n-1
    depart = None

    for j in reversed(range(0,n-1)):
        if plus_a_gauche < j:
            # on a j = depart - i avec
            i = depart - j
            # et plus_a_gauche = depart - k avec
            k = depart - plus_a_gauche
            if suffixe[n-1-i] != k-i:
                suffixe[j] = min(suffixe[n-1-i],k-i)
            else:
                depart = j
                while plus_a_gauche > 0 \
                        and x[plus_a_gauche] == x[n-1-j+plus_a_gauche]:
                    plus_a_gauche = plus_a_gauche - 1
                suffixe[j] = depart - plus_a_gauche
        else:
            plus_a_gauche = depart = j
            while plus_a_gauche >= 0 \
                    and x[plus_a_gauche] == x[n-1-j+plus_a_gauche]:
                plus_a_gauche = plus_a_gauche - 1
            suffixe[j] = depart - plus_a_gauche

    return suffixe
# END SUFFIXINIT

assert(calcule_suffixe_rep('bcabc') == [0,2,0,0,5])
assert(calcule_suffixe_rep('abbabba') == [1,0,0,4,0,0,7])

# BEGIN SUFFIXE
def calcule_suffixe(x):
    '''Prend en entrée un mot non vide x et renvoie son tableau de suffixe'''
    n = len(x)
    suffixe = [ None ] * n
    suffixe[n-1] = n
    plus_a_gauche = n-1
    depart = None

    for j in reversed(range(0,n-1)):
        if plus_a_gauche < j and suffixe[n-1-depart+j] != j-plus_a_gauche:
            suffixe[j] = min(suffixe[n-1-depart+j],j-plus_a_gauche)
        else:
            plus_a_gauche = min(plus_a_gauche, j)
            depart = j
            while plus_a_gauche >= 0 \
                    and x[plus_a_gauche] == x[n-1-j+plus_a_gauche]:
                plus_a_gauche = plus_a_gauche - 1
            suffixe[j] = depart - plus_a_gauche

    return suffixe
# END SUFFIXE

assert(calcule_suffixe('abbabba') == [1,0,0,4,0,0,7])
assert(calcule_suffixe('bcabc') == [0,2,0,0,5])
assert(calcule_suffixe('abaababaaba') == [1,0,3,1,0,6,0,3,1,0,11])

# BEGIN BONSUFFIXE
def calcule_bonsuffixe(x):
    '''Prend en entrée un mot non vide x et renvoie son tableau de suffixe'''
    n = len(x)
    suffixe = calcule_suffixe(x)
    bonsuffixe = [ None ] * n

    suivant = 0
    for k in reversed(range(0,n-1)):
        if suffixe[k] == k+1: # c'est un bord
            for i in range(suivant,n-1-k):
                bonsuffixe[i] = n-1-k
            suivant = n-1-k
    for i in range(suivant,n):
        bonsuffixe[i] = n

    for k in range(0,n-1):
        bonsuffixe[n-1-suffixe[k]] = n-1-k

    return bonsuffixe
# END BONSUFFIXE

(calcule_bonsuffixe('baacababa'))
(calcule_bonsuffixe('abaababaaba'))
print(calcule_bonsuffixe('bcabc'))

# BEGIN BOYERMOORE
def recherche_bm(num, motif, droite, bonsuffixe, chaine):
    '''Cherche motif dans chaine en utilisant les tables de sauts
    - droite et la correspondance num entre caractères
    et indices dans la table
    - bonsuffixe'''
    n, p = len(chaine), len(motif)
    i = 0
    print(chaine)
    while i <= n-p:
        present = True
        print(' '*i+motif)
        for j in reversed(range(p)):
            if chaine[i+j] != motif[j]:
                present = False
                k = droite[num(chaine[i+j])]
                if k is None:
                    dec = j + 1
                elif k < j:
                    dec = j - k
                else:
                    dec = 1
                dec = max(dec, bonsuffixe[j])
                i = i + dec
                break
        if present:
            return i
    return None
# END BOYERMOORE

chaine = 'caabccbbc'
motif =  'bcabc'
droite = calcule_droite(26, num_minuscule, motif)
bonsuffixe = calcule_bonsuffixe(motif)
recherche_bmh(num_minuscule, motif, droite, chaine)
recherche_bm(num_minuscule, motif, droite, bonsuffixe, chaine)
