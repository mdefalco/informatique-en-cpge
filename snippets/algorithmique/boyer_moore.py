

def decalage_mauvais(motif):
    p = len(motif)
    dec = [ p for _ in range(256) ]
    for i in range(1,p):
        c = ord(motif[p-1-i])
        if dec[c] == p:
            dec[c] = i
    return dec

def horspool(motif, chaine):
    dec = decalage_mauvais(motif)
    p = len(motif)
    i = len(chaine)-1
    while i >= p:
        if chaine[i] == motif[p-1] and chaine[i-p+1:i+1] == motif:
            return i-p
        i -= dec[ord(chaine[i])]
        
    return None

print(horspool('to','tatutoti'))
