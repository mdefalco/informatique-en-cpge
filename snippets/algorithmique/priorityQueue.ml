
type ('a, 'b) t

let cree taille_max defval defprio  =
    Tas.cree taille_max (defprio, defval)

let ajoute q x prio =
    Tas.insere (prio, x) q

let retire q = snd (Tas.supprime_racine q)

