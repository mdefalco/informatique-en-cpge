type grille = int option array array

let probleme_1 : grille = [|
    [| Some 1; None; None;    None; None; None;   None; None; Some 6 |];
    [| None; None; Some 6;    None; Some 2; None;   Some 7; None; None |];
    [| Some 7; Some 8; Some 9;    Some 4; Some 5; None;   Some 1; None; Some 3 |];

    [| None; None; None;    Some 8; None; Some 7;   None; None; Some 4 |];
    [| None; None; None;    None; Some 3; None;   None; None; None |];
    [| None; Some 9; None;    None; None; Some 4;   Some 2; None; Some 1 |];

    [| Some 3; Some 1; Some 2;    Some 9; Some 7; None;   None; Some 4; None |];
    [| None; Some 4; None;    None; Some 1; Some 2;   None; Some 7; Some 8 |];
    [| Some 9; None; Some 8;    None; None; None;   None; None; None |];
    |]

let pprint p =
    let sofcase c = match c with
        | None -> "{}"
        | Some k -> Printf.sprintf "%d" k
    in
    Printf.printf "\\begin{tikzpicture}\n";
    Printf.printf "\\matrix (m) [matrix of math nodes, 
row sep=1mm,column sep=1mm, align=center, 
every node/.append style={ anchor=base,text height=2ex,text width=1em}]\n{";
    for y = 0 to 8 do
        for x = 0 to 8 do
            Printf.printf "%s %s" (sofcase p.(y).(x)) (if x < 8 then "& " else "\\\\\n")
        done
    done;
    Printf.printf "};
\\draw[thick] 
(m-1-1.north west) -- (m-1-9.north east)
(m-4-1.north west) -- (m-4-9.north east)
(m-7-1.north west) -- (m-7-9.north east)
(m-9-1.south west) -- (m-9-9.south east)
(m-1-1.north west) -- (m-9-1.south west)
(m-1-4.north west) -- (m-9-4.south west)
(m-1-7.north west) -- (m-9-7.south west)
(m-1-9.north east) -- (m-9-9.south east)
;
\\draw[thin]
(m-2-1.north west) -- (m-2-9.north east)
(m-3-1.north west) -- (m-3-9.north east)
(m-5-1.north west) -- (m-5-9.north east)
(m-6-1.north west) -- (m-6-9.north east)
(m-8-1.north west) -- (m-8-9.north east)
(m-9-1.north west) -- (m-9-9.north east)
(m-1-2.north west) -- (m-9-2.south west)
(m-1-3.north west) -- (m-9-3.south west)
(m-1-5.north west) -- (m-9-5.south west)
(m-1-6.north west) -- (m-9-6.south west)
(m-1-8.north west) -- (m-9-8.south west)
(m-1-9.north west) -- (m-9-9.south west)
;";
    Printf.printf "\\end{tikzpicture}\n\\clearpage"

(* vérification du prolongement d'une solution partielle *)
let valide g x y =
    let v = ref true in
    let vus_colonne = Array.make 9 false in
    for y0 = 0 to 8 do
        match g.(y0).(x) with
        | None -> ()
        | Some k -> 
                if vus_colonne.(k-1)
                then v := false;
                vus_colonne.(k-1) <- true
    done;
    let vus_ligne = Array.make 9 false in
    for x0 = 0 to 8 do
        match g.(y).(x0) with
        | None -> ()
        | Some k -> 
                if vus_ligne.(k-1)
                then v := false;
                vus_ligne.(k-1) <- true
    done;
    let vus_grille = Array.make 9 false in
    let xb = (x / 3) * 3 in
    let yb = (y / 3) * 3 in
    for xd = 0 to 2 do
        for yd = 0 to 2 do
            match g.(yb+yd).(xb+xd) with
            | None -> ()
            | Some k -> 
                    if vus_grille.(k-1)
                    then v := false;
                    vus_grille.(k-1) <- true
        done
    done;
    !v

let rec suivant g (x,y) =
    if y > 8
    then None
    else if g.(y).(x) = None
    then Some (x,y)
    else if x < 8 then suivant g (x+1, y)
    else suivant g (0, y+1)

exception Solution

let resout g =
    let rec aux xi yi = match suivant g (xi, yi) with
        | None -> raise Solution
        | Some (x,y) ->
            for i = 1 to 9 do
                g.(y).(x) <- Some i;
                if valide g x y
                then begin
                    aux x y
                end
            done;
            g.(y).(x) <- None
    in 
    try 
        aux 0 0
    with Solution -> ()

let _ =
    resout probleme_1;
    pprint probleme_1
