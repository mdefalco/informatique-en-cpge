#include <stdio.h>

int compare_entiers(const void *t1, const void *t2)
{
    return *((int*)t1) - *((int *)t2);
}

size_t inversions_naif(int *t, size_t taille)
{
    size_t inv = 0;
    for (size_t i = 0; i < taille; i++)
    {
        for (size_t j = i+1; j < taille; j++)
        {
            if (t[i] > t[j]) {
                inv++;
            }
        }
    }

    return inv;
}

size_t inversions(int *t, size_t taille)
{
}

int main()
{
    int t[] = { 3, 4, 2, 1, 5, 7, 3 };
    size_t taille = sizeof(t) / sizeof(int);
    
    printf("Nb d'inversions %d\n", inversions_naif(t, taille));
}
