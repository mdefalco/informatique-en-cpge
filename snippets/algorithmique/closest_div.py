from random import randint
from math import sqrt

npoints = 100
points = [ (randint(0,1000), randint(0,1000)) for _ in range(npoints) ]

# IGNORE ABOVE
def distance(p1,p2):
    x1, y1 = p1
    x2, y2 = p2
    return sqrt( (x1-x2)**2 + (y1-y2)**2 )

def closest_brute(points, i, j):
    m = distance(points[i], points[i+1])
    for k in range(i,j+1):
        for l in range(k+1,j+1):
            d = distance(points[k], points[l])
            if d < m:
                m = d
    return m

def closest_rec(points_x, points_y, i, j):
    if j-i+1 <= 3:
        return closest_brute(points_x, i, j)
    m = (i+j)//2
    d1 = closest_rec(points_x, points_y, i, m)
    d2 = closest_rec(points_x, points_y, m+1, j)
    d = min(d1, d2)
    min_x = points_x[m][0]-d
    max_x = points_x[m+1][0]+d
    bande = [ p for p in points_y if min_x <= p[0] <= max_x ]
    for k in range(len(bande)):
        for l in range(1,8):
            if k+l >= len(bande):
                break
            d_kl = distance(bande[k], bande[k+l])
            if d_kl < d:
                d = d_kl
    return d

def closest(points):
    n = len(points)
    points.sort(key=lambda p: p[0])
    points_y = points[:]
    points_y.sort(key=lambda p: p[1])
    return closest_rec(points, points_y, 0, n-1)
# IGNORE BELOW

if __name__ == "__main__":
    print('Bruteforce', closest_brute(points, 0, npoints-1))
    print('Closest', closest(points))
