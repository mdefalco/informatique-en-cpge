(* BEGIN COMPRESSE *)
type table_bidir = {
    elements : string array;
    indices : (string, int) Hashtbl.t;
    mutable n_elements : int
}

let string_of_byte b = String.make 1 (Char.chr b)

let cree_table longueur_code =
    let taille_table = 1 lsl longueur_code in
    let elements = Array.make taille_table "" in
    let indices = Hashtbl.create taille_table in
    for i = 0 to 255 do
        let s = string_of_byte i in
        elements.(i) <- s;
        Hashtbl.add indices s i
    done;
    { elements = elements; indices =  indices; n_elements = 256 }

let ajoute_entree table s =
    table.elements.(table.n_elements) <- s;
    Hashtbl.add table.indices s table.n_elements;
    table.n_elements <- table.n_elements + 1

let compresse_fichier nom_in nom_out longueur_code =
    let table = cree_table longueur_code in
    let taille_table = 1 lsl longueur_code in
    let f_out = Bitpacking.open_out_bits nom_out in
    let f_in = open_in_bin nom_in in
    let m = ref (string_of_byte (input_byte f_in)) in
    begin
      try
        while true do
            let c = string_of_byte (input_byte f_in) in
            let mc = !m ^ c in
            match Hashtbl.find_opt table.indices mc with
            | Some _ -> m := mc
            | None -> begin
                let i = Hashtbl.find table.indices !m in
                Bitpacking.output_code f_out i longueur_code;
                if table.n_elements < taille_table
                then ajoute_entree table mc;
                m := c
            end
        done
      with End_of_file -> ()
    end;
    (* code final *)
    let i = Hashtbl.find table.indices !m in
    Printf.printf "%d " table.n_elements;
    Bitpacking.output_code f_out i longueur_code;
    close_in f_in;
    Bitpacking.close_out_bits f_out
(* END COMPRESSE *)


(* BEGIN DECOMPRESSE *)
let decompresse_fichier nom_in nom_out longueur_code =
    let table = cree_table longueur_code in
    let taille_table = 1 lsl longueur_code in
    let f_in = Bitpacking.open_in_bits nom_in in
    let f_out = open_out_bin nom_out in
    let code = ref (Bitpacking.input_code f_in longueur_code) in
    output_string f_out table.elements.(!code);

    begin
       try
          while true do
              let nouveau = Bitpacking.input_code f_in longueur_code in
              let s_code = table.elements.(!code) in
              let s = 
                  if nouveau = table.n_elements 
                  then let x = String.sub s_code 0 1
                       in s_code ^ x
                  else table.elements.(nouveau) in
              let x = String.sub s 0 1 in
              output_string f_out s;
              if table.n_elements < taille_table
              then ajoute_entree table (s_code ^ x);
              code := nouveau
        done
      with End_of_file -> ()
    end;
    Bitpacking.close_in_bits f_in;
    close_out f_out
(* END DECOMPRESSE *)

let _ =
    for longueur_code = 8 to 20 do
        Printf.printf "\nLongueur %d | " longueur_code;
        flush stdout;
        let nom_in = "snippets/algorithmique/huffman_proust.txt" in
        let nom_out = Printf.sprintf "/tmp/proust_%02d.cmp" longueur_code in
        (*
        let nom_out = "snippets/algorithmique/huffman_proust.cmp" in
        let nom_out2 = "snippets/algorithmique/huffman_proust.dec" in
        *)
        compresse_fichier nom_in nom_out longueur_code;
        (*
        let nom_out = "snippets/algorithmique/lzw.cmp" in
        let nom_out2 = "snippets/algorithmique/lzw.dec" in
        *)

        let nom_in = "snippets/algorithmique/lzw.py" in
        let nom_out = Printf.sprintf "/tmp/lzw_%02d.cmp" longueur_code in
        compresse_fichier nom_in nom_out longueur_code
    done

