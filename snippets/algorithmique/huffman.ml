type 'a arbre = Feuille of 'a | Noeud of 'a arbre * 'a arbre

(* BEGIN ARBRE *)
let construit_arbre occ =
    let arbres = Tas.cree 2000 (0, Feuille 0) in
    for i = 0 to 255 do
        let f = occ.(i) in
        if f > 0 (* on ignore les occurrences nulles *)
        then Tas.insere (-f, Feuille i) arbres
    done;
    while Tas.taille arbres > 1 do
        let fx, x = Tas.supprime_racine arbres in
        let fy, y = Tas.supprime_racine arbres in
        Tas.insere (fx+fy, Noeud(x,y)) arbres
    done;
    snd (Tas.supprime_racine arbres)
(* END ARBRE *)

(* BEGIN SERARBRE *)
let rec output_arbre f a =
    match a with
    | Noeud (x, y) -> begin
        output_byte f 0;
        output_arbre f x;
        output_arbre f y
    end
    | Feuille c -> begin
        output_byte f 1;
        output_byte f c
    end

let rec input_arbre f = 
    let code = input_byte f in
    match code with
    | 0 -> let g = input_arbre f in
        let d = input_arbre f in
        Noeud(g,d)
    | _ -> Feuille (input_byte f)
(* END SERARBRE *)

let rec pprint_arbre arbre = match arbre with
    | Feuille c -> Printf.printf "%c" (Char.chr c)
    | Noeud(x, y) ->
            print_string "(";
            pprint_arbre x;
            print_string ",";
            pprint_arbre y;
            print_string ")"

(* BEGIN OCCURRENCES *)
let table_occurrences nomdefichier =
    let fichier = open_in_bin nomdefichier in
    let occurrences = Array.make 256 0 in
    begin
     try
        while true do
            let c = input_byte fichier in
            occurrences.(c) <- occurrences.(c) + 1;
        done
      with End_of_file -> ()
    end;
    close_in fichier;
    occurrences
(* END OCCURRENCES *)

(* BEGIN CHEMINS *)
let chemins a =
    let a_plat = Array.make 256 [] in
    let rec parcours a chemin =
        match a with
        | Noeud (g, d) -> 
            parcours g (false::chemin);
            parcours d (true::chemin)
        | Feuille c -> a_plat.(c) <- List.rev chemin
    in parcours a []; a_plat
(* END CHEMINS *)

let rec chemin_to_string ch =
    match ch with
    | true::q -> "1" ^ chemin_to_string q
    | false::q -> "0" ^ chemin_to_string q
    | [] -> ""

let pprint_arbre_plat a_plat =
    for i = 0 to 255 do
        let l = a_plat.(i) in
        if l <> []
        then Printf.printf "%c : %s\n" (Char.chr i) (chemin_to_string l)
    done

(* BEGIN DECOMPRESSEBYTE *)
let rec decompresse_byte f a =
    match a with
    | Feuille c -> c
    | Noeud(g,d) ->
        decompresse_byte f (if Bitpacking.input_bit f then d else g)
(* END DECOMPRESSEBYTE *)

(* BEGIN COMPRESSEBYTE *)
let compresse_byte f a_plat c =
    let rec output_bool_list l =
        match l with
        | t::q -> Bitpacking.output_bit f t; output_bool_list q
        | [] -> ()
    in output_bool_list a_plat.(c)
(* END COMPRESSEBYTE *)

(* BEGIN COMPRESSEFICHIER *)
let compresse_fichier nom_in nom_out =
    let occ = table_occurrences nom_in in
    let a = construit_arbre occ in
    let f_out = Bitpacking.open_out_bits nom_out in
    output_arbre f_out.o_fichier a; 
    let a_plat = chemins a in
    let f_in = open_in_bin nom_in in
    begin
      try
        while true do
            let c = input_byte f_in in
            compresse_byte f_out a_plat c
        done
      with End_of_file -> ()
    end;
    close_in f_in;
    Bitpacking.close_out_bits f_out
(* END COMPRESSEFICHIER *)

(* BEGIN DECOMPRESSEFICHIER *)
let decompresse_fichier nom_in nom_out =
    let f_in = Bitpacking.open_in_bits nom_in in
    let a = input_arbre f_in.i_fichier in
    let f_out = open_out_bin nom_out in
    begin
      try
          while true do
              let c = decompresse_byte f_in a in
              output_byte f_out c
          done
      with End_of_file -> ()
    end;
    Bitpacking.close_in_bits f_in;
    close_out f_out
(* END DECOMPRESSEFICHIER *)

let _ =
    let nom_in = "snippets/algorithmique/huffman_proust.txt" in
    let nom_out = "snippets/algorithmique/huffman_proust.cmp" in
    let nom_out2 = "snippets/algorithmique/huffman_proust.dec" in

    compresse_fichier nom_in nom_out;
    decompresse_fichier nom_out nom_out2

(* BEGIN COMPRESSEBYTECONTINUATION *)
let chemins_continuation a =
    let a_plat = Array.make 256 (fun _ -> ()) in
    let rec parcours a chemin =
        match a with
        | Noeud (g, d) -> 
            parcours g (fun f -> chemin f; Bitpacking.output_bit f false);
            parcours d (fun f -> chemin f; Bitpacking.output_bit f true)
        | Feuille c -> a_plat.(c) <- chemin
    in parcours a (fun _ -> ()); a_plat

let compresse_byte_continuation f a_plat c =
    a_plat.(c) f
(* END COMPRESSEBYTECONTINUATION *)
