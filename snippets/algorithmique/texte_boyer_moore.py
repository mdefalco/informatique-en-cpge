# BEGIN DROITE
taille_alphabet = 256
def calcule_droite(motif):
    '''Calcule le tableau droite associé au motif'''
    droite = [ None ] * taille_alphabet
    p = len(motif)
    for i in range(p-1):
        j = p-2-i # indice en partant de la fin
        c = motif[j]
        if droite[ord(c)] is None:
            droite[ord(c)] = j
    return droite
# END DROITE

# BEGIN HORSPOOL
def recherche_BMH(motif, droite, chaine):
    '''Cherche motif dans chaine en utilisant la table de saut
    précalculée droite'''
    n, p = len(chaine), len(motif)
    i = 0
    while i <= n-p:
        present = True
        for j in reversed(range(p)):
            if chaine[i+j] != motif[j]:
                present = False
                k = droite[ord(chaine[i+j])]
                if k is None:
                    i = i + j + 1
                elif k < j:
                    i = i + j - k
                else:
                    i = i + 1
                break
        if present:
            return i
    return None
# END HORSPOOL

droite = calcule_droite('abaa')
assert(recherche_BMH('abaa', droite, 'aaabababbaa') == None)
assert(recherche_BMH('abaa', droite, 'aaababaabaa') == 4)

# BEGIN SUFFIXINIT
def calcule_suffixe_rep(x):
    '''Prend en entrée un mot non vide x et renvoie son tableau de suffixe'''
    n = len(x)
    suffixe = [ None ] * n
    suffixe[n-1] = n
    plus_a_gauche = n-1
    depart = None

    for j in reversed(range(0,n-1)):
        if plus_a_gauche < j:
            # on a j = depart - i avec
            i = depart - j
            # et plus_a_gauche = depart - k avec
            k = depart - plus_a_gauche
            if suffixe[n-1-i] != k-i:
                suffixe[j] = min(suffixe[n-1-i],k-i)
            else:
                depart = j
                while plus_a_gauche >= 0 \
                        and x[plus_a_gauche] == x[n-1-j+plus_a_gauche]:
                    plus_a_gauche = plus_a_gauche - 1
                suffixe[j] = depart - plus_a_gauche
        else:
            plus_a_gauche = depart = j
            while plus_a_gauche >= 0 \
                    and x[plus_a_gauche] == x[n-1-j+plus_a_gauche]:
                plus_a_gauche = plus_a_gauche - 1
            suffixe[j] = depart - plus_a_gauche

    return suffixe
# END SUFFIXINIT

print(calcule_suffixe_rep('abcdef'))
print(calcule_suffixe_rep('aaaaaa'))
assert(calcule_suffixe_rep('bcabc') == [0,2,0,0,5])
assert(calcule_suffixe_rep('abbabba') == [1,0,0,4,0,0,7])

# BEGIN SUFFIXE
def calcule_suffixe(x):
    '''Prend en entrée un mot non vide x et renvoie son tableau de suffixe'''
    n = len(x)
    suffixe = [ None ] * n
    suffixe[n-1] = n
    plus_a_gauche = n-1
    depart = None

    for j in reversed(range(0,n-1)):
        if plus_a_gauche < j and suffixe[n-1-depart+j] != j-plus_a_gauche:
            suffixe[j] = min(suffixe[n-1-depart+j],j-plus_a_gauche)
        else:
            plus_a_gauche = min(plus_a_gauche, j)
            depart = j
            while plus_a_gauche >= 0 \
                    and x[plus_a_gauche] == x[n-1-j+plus_a_gauche]:
                plus_a_gauche = plus_a_gauche - 1
            suffixe[j] = depart - plus_a_gauche

    return suffixe
# END SUFFIXE

assert(calcule_suffixe('abbabba') == [1,0,0,4,0,0,7])
assert(calcule_suffixe('bcabc') == [0,2,0,0,5])
assert(calcule_suffixe('abaababaaba') == [1,0,3,1,0,6,0,3,1,0,11])

# BEGIN BONSUFFIXE
def calcule_bonsuffixe(x):
    '''Prend en entrée un mot non vide x et renvoie son tableau de suffixe'''
    n = len(x)
    suffixe = calcule_suffixe(x)
    bonsuffixe = [ n ] * n

    suivant = 0
    for k in reversed(range(0,n-1)):
        if suffixe[k] == k+1: # c'est un bord
            for i in range(suivant,n-1-k):
                bonsuffixe[i] = n-1-k
            suivant = n-1-k

    for k in range(0,n-1):
        bonsuffixe[n-1-suffixe[k]] = n-1-k

    return bonsuffixe
# END BONSUFFIXE

assert(calcule_bonsuffixe('baacababa') == [7, 7, 7, 7, 7, 2, 7, 4, 1])
assert(calcule_bonsuffixe('abaababaaba') == [5, 5, 5, 5, 5, 8, 8, 3, 10, 2, 1])
assert(calcule_bonsuffixe('bcabc') == [3, 3, 3, 5, 1])

# BEGIN BOYERMOORE
def recherche_BM(motif, droite, bonsuffixe, chaine):
    '''Cherche motif dans chaine en utilisant les tables de sauts
    droite et bonsuffixe'''
    n, p = len(chaine), len(motif)
    i = 0
    while i <= n-p:
        present = True
        for j in reversed(range(p)):
            if chaine[i+j] != motif[j]:
                present = False
                k = droite[ord(chaine[i+j])]
                if k is None:
                    dec = j + 1
                elif k < j:
                    dec = j - k
                else:
                    dec = 1
                dec = max(dec, bonsuffixe[j])
                i = i + dec
                break
        if present:
            return i
    return None
# END BOYERMOORE

chaine = 'caabccbbc'
motif =  'bcabc'
droite = calcule_droite(motif)
bonsuffixe = calcule_bonsuffixe(motif)
recherche_BMH(motif, droite, chaine)
recherche_BM(motif, droite, bonsuffixe, chaine)
