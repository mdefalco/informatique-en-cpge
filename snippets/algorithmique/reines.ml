let rec valide (x1,y1) l =
    match l with
    | [] -> true
    | (x2,y2)::q ->
        x1 <> x2 && abs (x2-x1) <> abs(y2-y1) && valide (x1,y1) q

let print_solution l = 
    let m = Array.make_matrix 8 8 false in
    List.iter (fun (x,y) -> m.(x).(y) <- true) l;
    for y = 0 to 7 do
        for x = 0 to 7 do
            print_char (if m.(x).(y) then 'X' else '.')
        done;
        print_char '\n'
    done;
    print_char '\n'

let x_to_letter x = 
    let t = [| "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h" |] in
    t.(x)

let string_of_board l =
    let pieces = String.concat ","
        (List.map 
            (fun (x,y) -> "q" ^ (x_to_letter x) ^ (string_of_int (y+1))) l) in
    "\\chessboard[setpieces={" ^ pieces ^ "}]"

let rec resout_reines_p part =
    let k = List.length part in
    Printf.printf "[{%s}\n" (string_of_board part);
    if k = 8 
    then ( Printf.printf "]\n"; [ part ] )
    else begin
        let resultats = ref [] in
        let rec aux x acc =
            if x < 0
            then acc
            else
                let essai = (x,k) :: part in
                let nacc = if valide (x,k) part
                           then (resout_reines_p essai) @ acc
                           else acc in
                aux (x-1) nacc
        in
        for x = 0 to 7 do
            let essai = (x,k) :: part in
            if valide (x,k) part
            then begin
                resultats := (resout_reines_p essai) @ !resultats;
            end
        done;
        Printf.printf "]\n"; !resultats
    end

let rec resout_reines part =
    let k = List.length part in
    if k = 8 
    then [ part ]
    else begin
        let resultats = ref [] in
        for x = 0 to 7 do
            let essai = (x,k) :: part in
            if valide (x,k) part
            then begin
                resultats := (resout_reines essai) @ !resultats;
            end
        done;
        !resultats
    end

let rec resout_reines part =
    let k = List.length part in
    if k = 8 
    then [ part ]
    else 
        let rec aux x acc =
            if x < 0
            then acc
            else let essai = (x,k) :: part in
                 let nacc = if valide (x,k) part
                           then (resout_reines essai) @ acc
                           else acc in
                    aux (x-1) nacc
        in
        aux 7 [] 

let f () =
    print_string "\\begin{forest}\n";
    resout_reines_p [ (5,0); (2,1) ];
    print_string "\\end{forest}\n"

let _ = 
    let solutions = resout_reines [ ] in
    Printf.printf "%d\n" (List.length solutions)


