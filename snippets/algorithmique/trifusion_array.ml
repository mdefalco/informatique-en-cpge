let fusionne t1 t2 =
    let n1 = Array.length t1 in
    let n2 = Array.length t2 in
    let i1 = ref 0 in
    let i2 = ref 0 in
    let i = ref 0 in
    let t = Array.make (n1+n2) t1.(0) in
    while !i1 < n1 || !i2 < n2 do
        if !i1 = n1 || (!i2 < n2 && t1.(!i1) > t2.(!i2))
        then begin
            t.(!i) <- t2.(!i2);
            incr i2
        end else begin
            t.(!i) <- t1.(!i1);
            incr i1
        end;
        incr i
    done;
    t

let rec tri_fusion_aux t i j =
    let n = j - i in
    if n >= 1
    then begin
        let m = i + n / 2 in
        let t1 = tri_fusion_aux t i m in
        let t2 = tri_fusion_aux t (m+1) j in
        print_t t1;
        print_t t2;
        fusionne t1 t2
    end
    else if n = 0
    then [|t.(i)|]
    else [||]

let tri_fusion t =
    let n = Array.length t in 
    tri_fusion_aux t 0 (n-1)
