
let npoints = 10
let points = Array.init npoints
    (fun i -> Random.float 100., Random.float 100.)

let array_filter f a =
    let n = ref 0 in
    for i = 0 to Array.length a - 1 do
        if f a.(i)
        then incr n
    done;
    let b = Array.make !n a.(0) in
    n := 0;
    for i = 0 to Array.length a - 1 do
        if f a.(i)
        then (b.(!n) <- a.(i); incr n)
    done;
    b

(* IGNORE ABOVE *)
let distance (x1,y1) (x2,y2) =
    let v = (x1-.x2)*.(x1-.x2)+.(y1-.y2)*.(y1-.y2) in
    sqrt v

let closest_brute points i j =
    let d = ref (distance points.(i) points.(i+1)) in
    for k = i to j do
        for l = k+1 to j do
            let dist_kl = distance points.(k) points.(l) in
            if !d > dist_kl
            then d := dist_kl
        done
    done;
    !d

let rec closest_rec p_x p_y i j =
    if j-i+1 <= 3
    then closest_brute p_x i j
    else begin
        let m = (i+j) / 2 in
        let d1 = closest_rec p_x p_y i m in
        let d2 = closest_rec p_x p_y (m+1) j in
        let d = min d1 d2 in
        let min_x = fst p_x.(m) -. d in
        let max_x = fst p_x.(m+1) +. d in

        let bande = array_filter 
            (fun (x,_) -> min_x <= x && x <= max_x) p_y in
        let m_d = ref d in
        for k = 0 to Array.length bande - 1 do
            for l = k+1 to min (k+7) (Array.length bande - 1) do
                let d_kl = distance bande.(k) bande.(l) in
                if d_kl < !m_d
                then m_d := d_kl
            done
        done;
        !m_d
    end
    
let closest points =
    let p_y = Array.copy points in
    Array.sort Stdlib.compare points;
    Array.sort Stdlib.compare p_y;
    closest_rec points p_y 0 (Array.length points - 1)
(* IGNORE BELOW *)

let _ =
    Random.self_init ();
    let d = closest_brute points 0 (npoints-1) in
    let d2 = closest points in
    Printf.printf "Bruteforce %0.5f\n" d;
    Printf.printf "Div %0.5f\n" d2
