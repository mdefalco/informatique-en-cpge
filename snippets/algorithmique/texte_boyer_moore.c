#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#define MIN(a,b) ((a<b)?a:b)
#define MAX(a,b) ((a<b)?b:a)

// BEGIN DROITE
int taille_alphabet = 256; // on pourrait passer par un define

/* Calcule le tableau droite associé au motif
 * le tableau renvoyé a été alloué, il devra être libéré après utilisation */
int *calcule_droite(char *motif)
{
    int *droite = malloc(sizeof(int) * taille_alphabet);
    int p = strlen(motif);

    memset(droite, -1, sizeof(int) * taille_alphabet);

    for (int i = 0; i < p-2; i++)
    {
        int j = p-2-i;
        char c = motif[j];
        if (droite[c] < 0)
            droite[c] = j;
    }

    return droite;
}
// END DROITE

// BEGIN HORSPOOL
/* cherche motif dans chaine en utilisant la table de saut précalculée
 * droite. Renvoie l'indice de la première occurrence ou -1 s'il n'est pas
 * présent */
int recherche_BMH(char *motif, int *droite, char *chaine)
{
    int n = strlen(chaine);
    int p = strlen(motif);

    for(int i = 0; i <= n-p; )
    {
        bool present = true;
        for (int j = p-1; j >= 0; j--)
        {
            if (chaine[i+j] != motif[j])
            {
                int k = droite[chaine[i+j]];
                present = false;
                if (k < 0)
                    i = i + j + 1;
                else if (k < j)
                    i = i + j - k;
                else
                    i = i + 1;
                break;
            }
        }
        if (present)
            return i;
    }

    return -1;
}
// END HORSPOOL

// BEGIN SUFFIXINIT
/* Calcule le tableau suffixe associé au mot x
 * le tableau renvoyé a été alloué, il devra être libéré après utilisation */
int *calcule_suffixe_rep(char *x)
{
    int n = strlen(x);
    int *suffixe = malloc(sizeof(int) * n);
    int plus_a_gauche = n-1;
    int depart = -1;

    memset(suffixe, -1, sizeof(int) * n);
    suffixe[n-1] = n;

    for (int j = n-2; j >= 0; j--)
    {
        if (plus_a_gauche < j)
        {
            // on a j = depart - i avec
            int i = depart - j;
            // et plus_a_gauche = depart -k avec
            int k = depart - plus_a_gauche;

            if (suffixe[n-1-i] != k-i)
                suffixe[j] = MIN(suffixe[n-1-i], k-i);
            else {
                depart = j;
                while (plus_a_gauche >=0 
                        && x[plus_a_gauche] == x[n-1-j+plus_a_gauche])
                    plus_a_gauche--;
                suffixe[j] = depart - plus_a_gauche;
            }
        } else {
            plus_a_gauche = depart = j;
            while (plus_a_gauche >=0 
                    && x[plus_a_gauche] == x[n-1-j+plus_a_gauche])
                plus_a_gauche--;
            suffixe[j] = depart - plus_a_gauche;
        }
    }

    return suffixe;
}
// END SUFFIXINIT
//
// BEGIN SUFFIXE
/* Calcule le tableau suffixe associé au mot x
 * le tableau renvoyé a été alloué, il devra être libéré après utilisation */
int *calcule_suffixe(char *x)
{
    int n = strlen(x);
    int *suffixe = malloc(sizeof(int) * n);
    int plus_a_gauche = n-1;
    int depart = -1;

    memset(suffixe, -1, sizeof(int) * n);
    suffixe[n-1] = n;

    for (int j = n-2; j >= 0; j--)
    {
        if (plus_a_gauche < j
            && suffixe[n-1-depart+j] != j-plus_a_gauche)
            suffixe[j] = MIN(suffixe[n-1-depart+j], j-plus_a_gauche);
        else {
            plus_a_gauche = MIN(plus_a_gauche, j);
            depart = j;
            while (plus_a_gauche >=0 
                    && x[plus_a_gauche] == x[n-1-j+plus_a_gauche])
                plus_a_gauche--;
            suffixe[j] = depart - plus_a_gauche;
        }
    }

    return suffixe;
}
// END SUFFIXE

// BEGIN BONSUFFIXE
/* Calcule le tableau suffixe associé au mot x
 * le tableau renvoyé a été alloué, il devra être libéré après utilisation */
int *calcule_bonsuffixe(char *x)
{
    int n = strlen(x);
    int *suffixe = calcule_suffixe(x);
    int *bonsuffixe = malloc(sizeof(int) * n);
    int suivant = 0;

    memset(bonsuffixe, n, sizeof(int) * n);

    for (int k = n-2; k >= 0; k--)
    {
        if (suffixe[k] == k+1) // bord
        {
            for (int i = suivant; i < n-1-k; i++)
                bonsuffixe[i] = n-1-k;
            suivant = n-1-k;
        }
    }

    for (int k = 0; k < n-1; k++)
        bonsuffixe[n-1-suffixe[k]] = n-1-k;

    free(suffixe);

    return bonsuffixe;
}
// END BONSUFFIXE

// BEGIN BOYERMOORE
/* cherche motif dans chaine en utilisant les tables de saut précalculées
 * droite et bonsuffixe. Renvoie l'indice de la première occurrence ou -1 s'il n'est pas
 * présent */
int recherche_BM(char *motif, int *droite, int *bonsuffixe, char *chaine)
{
    int n = strlen(chaine);
    int p = strlen(motif);

    for(int i = 0; i <= n-p; )
    {
        bool present = true;
        for (int j = p-1; j >= 0; j--)
        {
            if (chaine[i+j] != motif[j])
            {
                int k = droite[chaine[i+j]];
                int dec = 1;
                present = false;
                if (k < 0)
                    dec = j + 1;
                else if (k < j)
                    dec = j - k;
                i = i + MAX(dec, bonsuffixe[j]);
                break;
            }
        }
        if (present)
            return i;
    }

    return -1;
}
// END BOYERMOORE

int main(int argc, char *argv[])
{
    char *motif = "abaa";
    int *droite = calcule_droite(motif);
    assert(recherche_BMH(motif, droite, "aaabababbaa") == -1);
    assert(recherche_BMH(motif, droite, "aaababaabaa") == 4);

    char *motif1 = "bcabc";
    int *suffixe = calcule_suffixe_rep(motif1);
    int suffixe_test1[] = { 0, 2, 0, 0, 5 };
    assert(memcmp(suffixe, suffixe_test1, strlen(motif1) * sizeof(int)) == 0);
    free(suffixe);
    suffixe = calcule_suffixe(motif1);
    assert(memcmp(suffixe, suffixe_test1, strlen(motif1) * sizeof(int)) == 0);
    free(suffixe);

    char *motif2 = "abbabba";
    suffixe = calcule_suffixe_rep(motif2);
    int suffixe_test2[] = { 1, 0, 0, 4, 0, 0, 7 };
    assert(memcmp(suffixe, suffixe_test2, strlen(motif2) * sizeof(int)) == 0);
    free(suffixe);
    suffixe = calcule_suffixe(motif2);
    assert(memcmp(suffixe, suffixe_test2, strlen(motif2) * sizeof(int)) == 0);
    free(suffixe);

    int *bonsuffixe;
    char *motif3 = "baacababa";
    int bonsuffixe_test3[] = { 7, 7, 7, 7, 7, 2, 7, 4, 1 };
    bonsuffixe = calcule_bonsuffixe(motif3);
    assert(memcmp(bonsuffixe, bonsuffixe_test3, strlen(motif3) * sizeof(int)) == 0);
    free(bonsuffixe);
    
    char *motif4 = "abaababaaba";
    int bonsuffixe_test4[] = { 5, 5, 5, 5, 5, 8, 8, 3, 10, 2, 1 };
    bonsuffixe = calcule_bonsuffixe(motif4);
    assert(memcmp(bonsuffixe, bonsuffixe_test4, strlen(motif4) * sizeof(int)) == 0);
    free(bonsuffixe);

    bonsuffixe = calcule_bonsuffixe(motif);
    assert(recherche_BM(motif, droite, bonsuffixe, "aaabababbaa") == -1);
    assert(recherche_BM(motif, droite, bonsuffixe, "aaababaabaa") == 4);
    free(droite);
    free(bonsuffixe);

    char *chaine5 = "cbacbbcabc";
    char *chaine6 = "cbaabbcabc";
    char *motif5 = "abbcabc";

    droite = calcule_droite(motif5);
    bonsuffixe = calcule_bonsuffixe(motif5);
    assert(recherche_BMH(motif5, droite, chaine5)
           == recherche_BM(motif5, droite, bonsuffixe, chaine5));
    assert(recherche_BMH(motif5, droite, chaine6)
           == recherche_BM(motif5, droite, bonsuffixe, chaine6));
    free(droite);
    free(bonsuffixe);

    return 0;
}
