import sys
import re

link_re = re.compile(r'<a href="#(?P<target>[^"]*)"[^>]*><span class="toc-section-number">(?P<number>[^<]*)</span>(?P<title>.*?)</a>')

def read_link(toc):
    m = link_re.search(toc)
    if m is None:
        print(toc)
    target = m.group('target')
    number = m.group('number')
    title = m.group('title')
    s, e = m.span()
    return (target, number, title), toc[e:]

def read_item(toc):
    assert(toc[:4] == '<li>')
    toc = toc[4:]
    link, toc = read_link(toc)
    sublist, toc = read_list(toc)
    assert(toc[:5] == '</li>')
    return link+(sublist,), toc[5:]

def read_list(toc):
    if toc[:4] != '<ul>':
        return [], toc
    toc = toc[4:]
    items = []
    while toc[:5] != '</ul>':
        item, toc = read_item(toc)
        items.append(item)
    return items, toc[5:]

def toc_to_tree(toc):
    tree, toc = read_list(toc)
    return tree

def tree_to_dynamic_toc(tree):
    s = ''
    for level1 in tree:
        s += f'<div class="item header"> <a href="#{level1[0]}"><span>{level1[1]}</span> {level1[2]}</a>'
        s += '<div class="menu">'
        for level2 in level1[3]:
            s += f'<a class="item" href="#{level2[0]}"><div class="ui label">{level2[1]}</div> {level2[2]}</a>'
            if level2[3] != []:
                s += '<div class="secondary menu">'
                for level3 in level2[3]:
                    s += f'<a style="font-size:0.6em" class="item" href="#{level3[0]}"><i class="circle icon"></i> {level3[2]}</a>'
                s+= '</div>'

        s += '</div></div>'
    return s

def fixtoc(toc):
    flat_toc = ' '.join(toc).replace('\n','').replace('> ','>')
    open('test.out','w').write(flat_toc)
    tree = toc_to_tree(flat_toc)
    return tree_to_dynamic_toc(tree)

if __name__ == "__main__":
    fn = sys.argv[1]
    start_toc, end_toc = None, None
    lines = open(fn).readlines()
    for i in range(len(lines)):
        if lines[i] == '<!-- FIX TOC -->\n':
            if start_toc is None:
                start_toc = i+1
            else:
                end_toc = i

    toc = lines[start_toc:end_toc]

    open(fn,'w').write(''.join(lines[:start_toc])+fixtoc(toc)+''.join(lines[end_toc:]))
