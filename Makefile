TEMPLATES=tex/main.tex tex/structure.tex
CHAP_SOURCES=$(wildcard src/chap*md)
CHAP_PDF=$(subst .md,.pdf,$(subst src,tex,$(CHAP_SOURCES)))
SOURCES=src/index.md $(CHAP_SOURCES)
SLIDES=$(wildcard src/slides/*md)
SLIDES_HOUT=$(subst .md,.hout,$(SLIDES))
SLIDES_HTML=$(subst .md,.html,$(subst src,html,$(SLIDES)))

HTML=$(subst .md,.html,$(subst src,html,$(SOURCES)))
HOUT=$(subst .md,.hout,$(SOURCES))
TOUT=$(subst .md,.tout,$(SOURCES))

TPS=html/algorithmique/graphes_avances_tp_couplage.ipynb  html/algorithmique/graphes_avances_tp_kosaraju_2sat.ipynb 
ALLSOURCES=$(shell find src/ -name "*.md")

all: pdf html

tps: $(TPS)
pdf: $(CHAP_PDF)

html: $(HTML) $(HOUT)
	cp -r html/* MarcdeFalco.github.io/

copypdf:
	cp -r tex/*pdf html/

sync:
	rsync -avz html/* marc@de-falco.fr:~/sites/pico/poly/
	rsync -avz content/* marc@de-falco.fr:~/sites/pico/content/

src/%.tout: src/%.md preprocessor.py $(ALLSOURCES)
	python preprocessor.py $< $@ --format tex --lang ocaml

tex/%.pdf: src/%.tout $(ALLSOURCES)
	cd tex ; pandoc -F pandoc-crossref -f markdown+raw_attribute+raw_tex -o ../$@ --pdf-engine=lualatex --toc-depth 3 --template=chapitre.tex ../$<
	cp $@ html/

tex/%.tex: src/%.tout $(ALLSOURCES)
	cd tex ; pandoc -F pandoc-crossref -f markdown+raw_attribute+raw_tex -o ../$@ --shift-heading-level-by=-2 --toc-depth 3 --template=chapitre.tex ../$<

src/slides/%.hout: src/slides/%.md preprocessor.py $(SLIDES)
	python preprocessor.py $< $@ --format html --slides

html/%.ipynb: src/%.md 
	python preprocessor.py $< $<.out --macros src/macros_ipynb.md
	pandoc -f markdown --shift-heading-level-by=-3 -o $@ $<.out

src/%.hout: src/%.md preprocessor.py $(ALLSOURCES)
	python preprocessor.py $< $@ --format html

html/slides/%.html: src/slides/%.hout template.html $(SLIDES)
	cd html/slides ; pandoc -F pandoc-crossref -s -t revealjs -V revealjs-url=../reveal.js -V theme=night --mathjax -H slides.css -f markdown+raw_attribute+raw_html+fenced_code_attributes -o ../../$@ ../../$< 

html/%.html: src/%.hout template.html fixtoc.py $(ALLSOURCES)
	cd html ; pandoc -F pandoc-crossref -N --mathjax -f markdown+raw_attribute+raw_html+fenced_code_attributes --template=../template.html --shift-heading-level-by=-2 --toc-depth 3 --toc -o ../$@ ../$< ; python ../fixtoc.py ../$@

src/complet.ref: preprocessor.py $(ALLSOURCES)
	python preprocessor.py src/complet.md src/complet.ref --format html --exportref

src/complet_p_ocaml.out: preprocessor.py $(ALLSOURCES)
	python preprocessor.py src/complet.md src/complet_p_ocaml.out --format tex --lang ocaml
tex/complet_ocaml.pdf: $(TEMPLATES) src/complet_p_ocaml.out
	cd tex ; pandoc -F pandoc-crossref -f markdown+raw_attribute+raw_tex -o complet_ocaml.pdf --pdf-engine=lualatex --template=main.tex ../src/complet_p_ocaml.out
tex/complet_ocaml.tex: $(TEMPLATES) src/complet_p_ocaml.out
	cd tex ; pandoc -F pandoc-crossref -f markdown+raw_attribute+raw_tex+fenced_code_attributes -o complet_ocaml.tex --template=main.tex ../src/complet_p_ocaml.out
html/pdf/complet_ocaml.pdf: tex/complet_ocaml.pdf
	cp tex/complet_ocaml.pdf html/pdf/complet_ocaml.pdf

src/complet_p_python.out: preprocessor.py $(ALLSOURCES)
	python preprocessor.py src/complet.md src/complet_p_python.out --format tex --lang python
tex/complet_python.pdf: $(TEMPLATES) src/complet_p_python.out
	cd tex ; pandoc -F pandoc-crossref -f markdown+raw_attribute+raw_tex -o complet_python.pdf --pdf-engine=lualatex --template=main.tex ../src/complet_p_python.out
tex/complet_python.tex: $(TEMPLATES) src/complet_p_python.out
	cd tex ; pandoc -F pandoc-crossref -f markdown+raw_attribute+raw_tex+fenced_code_attributes -o complet_python.tex --template=main.tex ../src/complet_p_python.out
html/pdf/complet_python.pdf: tex/complet_python.pdf
	cp tex/complet_python.pdf html/pdf/complet_python.pdf

src/complet_p_c.out: preprocessor.py $(ALLSOURCES)
	python preprocessor.py src/complet.md src/complet_p_c.out --format tex --lang c
tex/complet_c.pdf: $(TEMPLATES) src/complet_p_c.out
	cd tex ; pandoc -F pandoc-crossref -f markdown+raw_attribute+raw_tex -o complet_c.pdf --pdf-engine=lualatex --template=main.tex ../src/complet_p_c.out
tex/complet_c.tex: $(TEMPLATES) src/complet_p_c.out
	cd tex ; pandoc -F pandoc-crossref -f markdown+raw_attribute+raw_tex+fenced_code_attributes -o complet_c.tex --template=main.tex ../src/complet_p_c.out
html/pdf/complet_c.pdf: tex/complet_c.pdf
	cp tex/complet_c.pdf html/pdf/complet_c.pdf


